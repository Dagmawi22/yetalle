<?php
require("../db/connection.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=
    , initial-scale=1.0">
    <title>Categories | Yetalle </title>
    <link rel="icon" type="image/png" href="../icons/yet.png" hreflang="en-us">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/mystyle.css?version=51">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <style>
        *{
            font-family: 'Source Sans Pro', sans-serif;
            font-style: normal;
            font-weight:400;
        }
        #load{
             margin:auto;
            animation: eyoha 6s infinite;
            -webkit-animation: eyoha 6s infinite;
        } 
        @keyframes eyoha{
           from{
                transform:rotate(360deg);
            }
            to{
                transform:rotate(-360deg);
            }
        }
        @-webkit-keyframes eyoha{
           from{
                transform:rotate(360deg);
            }
            to{
                transform:rotate(-360deg);
            }
        }
        .animate-bottom{
            position:relative;
            -webkit-animation-name:animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s;
        }
        
        @keyframes animatebottom{
             from {
                bottom:-100px; opacity:0;
            }
            to {
                bottom:0px; opacity:1;
            }
        }
        @-webkit-keyframes animatebottom{
             from {
                bottom:-100px; opacity:0;
            }
            to {
                bottom:0px; opacity:1;
            }
        }
        #loaded{
            display:none;
            
        }
    </style>
     <script>
         var myVar;
        function Load(){
            myVar = setTimeout(showPage,4000);
        }
        function showPage(){
            document.getElementById("loader").style.display="none";
             document.getElementById("loaded").style.display = "block";

        }
    </script>
</head>
<body onload="Load()">
  
<div id="loader" style="position:absolute; top:50%; left:50%; transform:translate(-50%,-50%);">

<span style="color:black; font-size:13px;"></span><span style="color:#00bfff; font-size:50px;" id="auto-write"></span>
<script>
    const text = document.getElementById("auto-write");
const prog = '...';

let idx=1;
setInterval(Write,250);

function Write(){
text.innerText = prog.slice(0,idx);
idx++;

if(idx>prog.length){
    idx=1;
}
}
</script>


</div>
<div id="loaded" class="">
        <a style="width:50px; height:50px;" class="btn" onclick="window.history.back()"><img src="../icons/Tg/PicsArt_02-18-11.36.20.png" width="40" height="40" style="cursor:pointer;" class=""></a><br>
    <?php $bussiness = strval($_GET['cat']);
          $bussiness1 = str_replace('-',' ',$bussiness);
          
    ?>
    <p class="text-center"><img src="../icons/covv.png" width="100%" height="300" style="max-width:500px;"><br>
      <span style="font-size:20px;"><?php echo $bussiness1;?></span>
   </p>
   
    <div class="mt-5">
       
         <?php   
                $query = "SELECT count(id) AS num FROM bussiness WHERE category='$bussiness1'";
                $sql = mysqli_query($conn,$query);
                $fetch = mysqli_fetch_array($sql);
                $num = $fetch['num'];
                if($num==0){
                    ?>
                    <script>document.getElementById("choose").innerHTML="";</script>
                    <?php
                }
                ?>
		<?php
			if($num>0){
				?>
				<p class="text-center recentlyopened" style="font-size:17px;"><?php echo $num?> results found.</p>

				<?php
					}

			if($num==0){
				?>
				<p class="text-center recentlyopened" style="font-size:17px;">Sorry, no result found.</p>

				<?php
					}
				?>
                </div>
   

    
    
    <div class="container-fluid">
        

        <p class="text-center">

       
        <?php 
        $qc = "SELECT count(city) AS num FROM bussiness";
        $sc = mysqli_query($conn,$qc);
        $fc = mysqli_fetch_array($sc);
        $cities = $fc['num'];

        
           
           
                $biz = strval($_GET['cat']);
                
            
               
                $query = "SELECT count(id) AS num FROM bussiness WHERE category='$bussiness1'";
                $sql = mysqli_query($conn,$query);

			if($num>0){
				?>
				<p class="text-center recentlyopened" style="font-size:17px;">Choose city to filter results.</p>

				<?php
					}
                
                    ?><p class="text-center"><span class="recentlyopened" style="font-size:17px;" id="choose"></span><?php
                    //$biz = str_replace('-',' ',$biz);
               $query = "SELECT city FROM bussiness WHERE category='$bussiness1' GROUP BY city";
               $sql = mysqli_query($conn,$query);
                while($fetch = mysqli_fetch_array($sql)){
                   
                   $biz1 = str_replace(' ','',$biz);
                   $city = $fetch['city'];
                   $city1 = str_replace(' ','-',$city);
                   ?>
                    <a href="search-results?bussiness=<?php echo $bussiness?>&city=<?php echo $city1?>"><?php echo $fetch['city'];
                   $ct = $fetch['city'];
                   $q = "SELECT count(id) AS num FROM bussiness WHERE category='$bussiness1' AND city='$city'";
                   $s = mysqli_query($conn,$q);
                   $f = mysqli_fetch_array($s);
                   $num = $f['num'];
                   ?> &nbsp; (<?php echo $num?>)
                
                </a><br> <?php
                     
                }
        ?></p>
         </p>
         
       
      
  
  </div>
        
        
       

    </p>
        
   
          <hr>
    <hr>
<footer class="mt-2">
    <p class="text-center" style="font-size:14px;">
    <span style="color:white; background-color:#00bfff; padding:5px; border-radius:10px;"><strong>Yetalle</strong></span> is a platform to help customers easily explore the goods and 
    services in their surrounding and to help bussinesses to easily be explored. <br>
   
</p>

<?php include('../includes/contacts.htm');?>
<?php include('../includes/social.htm');?>
</footer>
        </div>
         </div>
      

</body>
</html>