

<?php require("../db/connection.php");?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>  Sign Up | Yetalle</title>
    <link rel="icon" type="image/png" href="../icons/yet.png" hreflang="en-us">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/mystyle.css?version=51">
    <style>
        *{
            font-family: 'Source Sans Pro', sans-serif;
            font-style: normal;
            font-weight:400;
        }
       .container-fluid{
           width: 95%;
  padding-right: 60px;
  padding-left: 60px;
  padding-bottom:30px;
  padding-top:30px;
  margin-right: auto;
  margin-left: auto;
 
    
   border: 1px solid #e6e6ff;
       }
       option{
           color:black;
       }
      
    </style>
</head>
<body>
   <?php
    if(!isset($_GET['type'])){
        ?>
         
                 <div id="select">
                
    <a href="../HOME"><img src="../icons/yet.png" width="70" height="70" class="ml-1 mt-1" style=""></a><br><br>
         
    <div class="container-fluid mt-5">
        <p class="text-center"> <img src="../icons/signup.jpg" width="120" height="120"></p>
         <p class="text-center recentlyopened">Select Account Type</p>
         <form method="post">
             
             <p class="text-center" style="4px; border:1px solid #e6e6ff; border-radius:5px;">
                
                    <a href="register?type=biz" style="font-size:17px;">
                    <img src="../icons/biz.jpg" width="120" height="120"> <br> 
                    <u class="ml-5" style="font-size:17px;">bussiness account</u> <img src="../icons/chevron-right-512.png"  width="50" height="50" style="margin-top:-70px; float:right;"></a><br>
                                        </p>
                                         
                     <p class="text-center" style="padding:4px; border:1px solid #e6e6ff; border-radius:5px;">
                    <a href="register?type=user" style="">
                    <img src="../icons/user.jpg" width="120" height="120"> <br> 
                    <u class="ml-5" style="font-size:17px;">user account</u><img src="../icons/chevron-right-512.png"  width="50" height="50" style="margin-top:-70px; float:right;"></a><br>
                    
             </p>
             <p class="text-center">
              <span style="font-size:13px; color:black;">already registered? <a href="login" style="font-size:13px;">Login</a></span>
        </p>
            
    
        <?php
    }

        if(isset($_GET['type'])){
            
            $type = $_GET['type'];
            
            if($type=='biz'){
                     ?>
                     <a href="../HOME"><img src="../icons/yet.png" width="70" height="70" class="mt-1 ml-1"></a><br><br>
                    <div class="container-fluid">
                        <p class="text-center recentlyopened mt-3" style="border-bottom:1px solid black">
                         <img src="../icons/biz.jpg" width="200" height="200"></p>   
                        <p class="text-center" style="font-size:22px;">Bussiness Account</p>
                        <div style="font-size:14px; margin-top:-15px;">Advertising on Yetalle is a powerful way of connecting with customers and growing your bussiness.
                    Let's begin with your basic info and you will complete your bussiness profile after you signed up.
                    </div><hr>
                    <div class="alert alert-info">
                    <span style="font-size:13px; color:black;"><img src="../icons/lst.png" width="20" height="20"> If you can't find a matching category for your bussiness,
                     <a href="tel:0918888225" style="text-decoration:underline; font-size:13px;">call us.</a></span><br>
                     <span style="font-size:13px; color:black;"><img src="../icons/lst.png" width="20" height="20"> If your city isn't in the list,
                     select your nearest city.</span>
                    </div>
                        <form method="post">
                        
                        <span>Bussiness Name</span><br>
                        <input type="text" name="name-biz" id="name-biz" maxlength="20" placeholder="eg: X Restaurant" class="form-control" autofocus required>
                        <span id="error-name-biz" style="font-size:13px; color:red;"></span><hr>

                        <span>Primary Bussiness Category</span><br>
                        <select name="cat_biz" class="form-control" style="background-color:white; border:none; border-bottom:2px solid #00bfff; font-size:13px;" required>
                            
                        <?php include('../bussiness/cat.htm');?>
                        </select>
                         
                       <hr>
                         <span>City</span><br>
                        <select name="city_biz" class="form-control" 
                        style="background-color:white; border:none; 
                        border-bottom:2px solid #00bfff; font-size:13px;" required>
                            
                        <?php include('../bussiness/cities.htm');?>
                        </select>
                        
                        <hr>                       
       
                        <span>Email</span><br>
                        <input type="email" name="email-biz" id="email-biz" class="form-control" placeholder="eg: yourname@domain.com" autofocus required>
                        <span id="error-email-biz" style="font-size:13px; color:red;"></span>
                        <hr>
			
			<span>Mobile</span><br>
                        <input type="text" pattern="[0][9][0-9]{8}" name="mobile-biz" id="mobile-biz" class="form-control" placeholder="eg: 091134--54" autofocus required>
                        <span id="error-mobile-biz" style="font-size:13px; color:red;"></span>
                        <hr>
                      

                        <div>Password</div>
                        <input type="password" name="pass-biz" id="pass-biz" placeholder="******" class="form-control" required>
                        <span id="error-pass-biz" style="font-size:13px; color:red;"></span>
                        <hr>
                        
                        <div>Verify Password</div>
                        <input type="password" name="pass1-biz" id="pass1-biz" placeholder="******" class="form-control" required>
                        <span id="error-pass-biz1" style="font-size:13px; color:red;"></span>
                        <hr>
                        
                        

                         <p class="text-center">
        <button type="submit" name="add-biz" class="btn mt-3 btn-md"
         style="width:150px; height:35px; font-size:14px;">GO <img src="../icons/Tg/PicsArt_02-18-11.38.55.png" width="16" height="16"></button><br>
            <span style="font-size:13px;">not a member yet? <a href="register" style="font-size:13px;">Sign Up</a></span><br>
    <span style="font-size:13px;">forgot your password? <a href="forgot-password" style="font-size:13px;">Click here</a></span>
</p>
                        </form>
                        
                        <?php
                            if(isset($_POST['add-biz'])){

                                $name= $_POST['name-biz'];
                                 $new_name = str_replace(' ','_',$name);  
                                 $query_num = "SELECT count(email) AS num FROM bussiness WHERE name='$name'";
                                 $sql_num = mysqli_query($conn,$query_num);
                                 $fetch_num = mysqli_fetch_array($sql_num);
                                 $num = $fetch_num['num']+1;
                                 $id = $new_name.'_'.$num; 

                                $pass= $_POST['pass-biz'];
                                $pass1= $_POST['pass1-biz'];
                                $pass_len = strlen($pass);
                                $email = $_POST['email-biz'];
                                $cat = $_POST['cat_biz'];
                                $city = $_POST['city_biz'];
				$mobile = $_POST['mobile-biz'];

                                
                                 // delete if there is incomplete sign-up attempt before 
                                 $query_jmr = "SELECT count(*) AS num FROM user WHERE email='$email' AND verified!='yes'";
                                $sql_jmr = mysqli_query($conn,$query_jmr);
                                $fetch_jmr = mysqli_fetch_array($sql_jmr);
                                $jmr = $fetch_jmr['num'];

				 $query_mob = "SELECT count(*) AS num FROM bussiness WHERE phone='$mobile'";
                                $sql_mob = mysqli_query($conn,$query_mob);
                                $fetch_mob = mysqli_fetch_array($sql_mob);
                                $mob = $fetch_mob['num'];

                                if($jmr==1){
                                    $q1 = "DELETE FROM user WHERE email='$email'";
                                    $q2 = "DELETE FROM bussiness WHERE email='$email'";
                                    $s1 = mysqli_query($conn,$q1);
                                    $s1 = mysqli_query($conn,$q2);
                                }
                                 // end of prev delete


                                
                                $query_email = "SELECT count(*) AS num FROM user WHERE email='$email'";
                                $sql_email = mysqli_query($conn,$query_email);
                                $fetch_email = mysqli_fetch_array($sql_email);
                                $emails = $fetch_email['num'];

                                if($cat=='select category'){
                                    echo "<script>alert('Please, select a category for your bussiness.');</script>";
                                }
                                if($city=='select a city'){
                                    echo "<script>alert('Please, select a city for your bussiness.');</script>";
                                }
                                
                                if($emails>0){
                                     ?>
                                    <script>
                document.getElementById("error-email-biz").innerHTML = "Email already in use. Try another.";
                var ez = document.getElementById("email-biz");
                ez.style.border = "1px solid red";
                ez.focus();
                </script>
                                    <?php
                                }
				if($mob>0){
                                     ?>
                                    <script>
                document.getElementById("error-mobile-biz").innerHTML = "Phone number already in use. Try another.";
                var ez = document.getElementById("mobile-biz");
                ez.style.border = "1px solid red";
                ez.focus();
                </script>
                                    <?php
                                }
                                if($pass_len<6){
                                    ?>
                                    <script>
                document.getElementById("error-pass-biz").innerHTML = "Password must be atleast 6 characters.";
                var ps = document.getElementById("pass-biz");
                ps.style.border = "1px solid red";
                ps.focus();
                </script>
                                    <?php
                                }

                                 if($pass!=$pass1){
                                    ?>
                                    <script>
                document.getElementById("error-pass-biz").innerHTML = "Passwords didn't match.";
                document.getElementById("error-pass-biz1").innerHTML = "Passwords didn't match.";
                var ps = document.getElementById("pass-biz");
                ps.style.border = "1px solid red";
                var ps1 = document.getElementById("pass1-biz");
                ps1.style.border = "1px solid red";
                         
                </script>
                                    <?php
                                }

                                if($pass_len>=6 && $pass==$pass1 && $emails<1 && $cat!='select category' && $city!='select a city' && $mob<1){
                                     $confirm = mt_rand(1000,9999);
                                     $today = date('20y-m-d');
                                     $pass = md5($pass);
                                     $cat = $_POST['cat_biz'];
                                     $invitor = '';
                                        if(isset($_GET['i'])){
                                             $invitor = strval($_GET['i']);
                                        }
                                    
                                    
                                    $query = "INSERT INTO user VALUES ('','$email','$pass','bussiness',$confirm)";
                                    $sql = mysqli_query($conn,$query);
                                     $query = "INSERT INTO bussiness (name,id,acc_type,password,email,date_joined,invitor,category,category_type,city,phone)
                                      VALUES ('$name','$id','none','$pass','$email','$today','$invitor','$cat','primary','$city','$mobile')";
                                    $sql = mysqli_query($conn,$query);
                                    $header = "From yetale.com";
                                    $subject = "Sign Up Verification";
                                    $message = "Your confirmation code is $confirm.\n\n This email was sent to you because of your sign up request. \n\n
                                    If you didn't request to sign up, simply ignore it.\n\n Yours\n The YETALE team\n https://www.yetale.et";
                                    $mail = mail($email,$subject,$message,$header);
                                   
                        
                                    
                                   
                                    ?>
                                    <!-- js for detecting the location and saving in cookie-->
                                    <?php
                                    ?>
                                    <script>
                                       
                                        location.href="verify?em=<?php echo $email?>"
                                    </script><?php
                                   
                                }
                            }
                        ?>


                    </div>
               <?php
                      }

        if($type=='user'){
               ?>
               <a href="../HOME"><img src="../icons/yet.png" width="70" height="70" class="mt-1 ml-1"></a><br><br>
                    <div class="container-fluid">
                        <p class="text-center recentlyopened mt-3" style="border-bottom:1px solid black; font-size:22px;">
                        <img src="../icons/user.jpg" width="200" height="200"> <br>     
                        User Account<br><span style="font-size:16px;">It only takes seconds.</span></p>
                        <form method="post" enctype="multipart/form-data">
                        <span>Full Name</span><br>
                        <input type="text" maxlength="25" id="name" name="name" placeholder="eg: X Y" class="form-control" autofocus required>
                     
                        <br><label for="photo-user" style="border-bottom:2px solid #00bfff; background-color:white;" class="form-control"><img src="../icons/camera.png" width="22" height="22"> Your Photo</label> 
                        
                        <input type="file" name="photo-user" id="photo-user" style="background-color:white; border-bottom:2px solid #00bfff;" class="form-control" accept="image/*" hidden> 
                        <span id="error-photo" style="font-size:13px; color:red;"></span>
                       
                        <hr>
                        <div>Email</div>
                        <input type="email" name="email-user" id="e-user" class="form-control" placeholder="eg: yourname@domain.com" required>
                        <span id="err-em" style="color:red; font-size:13px;"></span>
                        <hr>
                        <div>Password</div>
                        <input type="password" name="pass" id="pass-u" placeholder="******" class="form-control" required>
                        <span id="error-pass-u" style="color:red; font-size:13px;"></span>
                        <hr>
                        <div>Verify Password</div>
                        <input type="password" name="pass1" id="pass1-u" placeholder="******" class="form-control" required>
                        <span id="error-pass1-u" style="color:red; font-size:13px;"></span><br>

                         <p class="text-center">
        <button type="submit" name="add-user" class="btn mt-3 btn-md"
         style="width:150px; height:35px; font-size:14px;">GO <img src="../icons/Tg/PicsArt_02-18-11.38.55.png" width="16" height="16"></button><br>
            
</p>
                        </form>
                        <?php
                            if(isset($_POST['add-user'])){
                               
                                $pass= $_POST['pass'];
                                $pass1= $_POST['pass1'];
                                $pass_len = strlen($pass);
                                 $email = $_POST['email-user'];

                                 // delete if there is incomplete sign-up attempt before 
                                 $query_jmr = "SELECT count(*) AS num FROM user WHERE email='$email' AND verified!='yes'";
                                $sql_jmr = mysqli_query($conn,$query_jmr);
                                $fetch_jmr = mysqli_fetch_array($sql_jmr);
                                $jmr = $fetch_jmr['num'];

                                if($jmr==1){
                                    $q1 = "DELETE FROM user WHERE email='$email'";
                                    $q2 = "DELETE FROM normal WHERE email='$email'";
                                    $s1 = mysqli_query($conn,$q1);
                                    $s1 = mysqli_query($conn,$q2);
                                }
                                 // end of prev delete
                                

                                $query_email = "SELECT count(email) AS num FROM user WHERE email='$email'";
                                $sql_email = mysqli_query($conn,$query_email);
                                $fetch_email = mysqli_fetch_array($sql_email);
                                $emails = $fetch_email['num'];

                               

                                 $size = $_FILES['photo-user']['size']/1024;

                             
                                 
                                if($size>1000){
                                    ?>
                                    <script>
                                    document.getElementById("error-photo").innerHTML = "Choose a photo not exceeding 1MB.";
                                    var phuser = document.getElementById("photo-user");
                                    phuser.style.border = "1px solid red";
                                    
                                    </script>
                                    <?php
                                }
                                 if($emails>0){
                                     ?>
                                    <script>
                document.getElementById("err-em").innerHTML = "Email already in use. Try another.";
                var eer = document.getElementById("e-user");
                eer.style.border = "1px solid red";
                eer.focus();
                </script>
                                    <?php
                                }

                                if($pass_len<6){
                                    ?>
                                    <script>
                document.getElementById("error-pass-u").innerHTML = "Password must be atleast 6 characters.";
                var passu = document.getElementById("pass-u");
                passu.style.border = "1px solid red";
                passu.focus();
                </script>
                                    <?php
                                }

                                 if($pass!=$pass1){
                                    ?>
                                    <script>
                document.getElementById("error-pass-u").innerHTML = "Passwords didn't match.";
                document.getElementById("error-pass1-u").innerHTML = "Passwords didn't match.";
                var pass = document.getElementById("pass-u");
                pass.style.border = "1px solid red";
                var pass1 = document.getElementById("pass1-u");
                pass1.style.border = "1px solid red";
                pass.focus();
                
                </script>
                                    <?php
                                }

                                if($pass_len>=6 && $pass==$pass1 && $emails<1 && $size<=1000){
                                    $name= $_POST['name'];
                                    $name1 = str_replace(' ','_',$name);
                                    // submit
                                    $query_id = "SELECT count(id) AS num FROM normal WHERE full_name='$name'";
                                    $sql_id = mysqli_query($conn,$query_id);
                                    $fetch_id = mysqli_fetch_array($sql_id);
                                    $id = $fetch_id['num']+1;
                                    $user_id = $name1.'_'.$id;
                                    $today = date('20y-m-d');

                                    
                                    $email = $_POST['email-user'];
                                    $pass = $_POST['pass'];

                                    if($_FILES['photo-user']['name']==''){
                                        $photo = '';
                                    }

                                    if($_FILES['photo-user']['name']!=''){
                                        
                                         $photo = str_shuffle('abvuggyuih').$_FILES['photo-user']['name'];
                                   
                                   
                                    $Temp =  $_FILES['photo-user']['tmp_name'];
                                    $MyLocation= "../images/".$photo ;
                                    move_uploaded_file($Temp,$MyLocation);
                                        
                                    }
                                    
                                    $confirm = mt_rand(1000,9999);
                                    $pass = md5($pass);

                                     $query = "INSERT INTO normal VALUES ('$user_id','$photo','$email','$pass','$name','$today')";
                                     $sql = mysqli_query($conn,$query);
                                    
                                     $query = "INSERT INTO user VALUES ('$user_id','$email','$pass','user',$confirm)";
                                     $sql = mysqli_query($conn,$query);
                                      $header = "From yetale.com";
                                    $subject = "Sign Up Verification";
                                    $message = "Your confirmation code is $confirm.\n\n This email was sent to you because of your sign up request. \n\n
                                    If you didn't request to sign up, simply ignore it.\n\n Yours\n The YETALE team\n https://www.yetale.com";
                                    mail($email,$subject,$message,$header);

                                   ?>
                                    <script>location.href="verify?em=<?php echo $email?>"</script>
                                   <?php
                                    
                                }
                            }
                        ?>


                    </div>
             


                    </div>
               <?php
                }
        
        }

        ?>

        
       
</body>
</html>
