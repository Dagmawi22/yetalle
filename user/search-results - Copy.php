
<?php
require("../db/connection.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>  Search Results | Yetalle</title>
    <link rel="icon" type="image/png" href="../icons/yet.png" hreflang="en-us">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/mystyle.css">
    <link rel="stylesheet" href="../css/rev.css">
    <style>
        a{
            color:#00bfff;
        }
         #load{
             margin:auto;
            animation: eyoha 6s infinite;
            -webkit-animation: eyoha 6s infinite;
        } 
        @keyframes eyoha{
           from{
                transform:rotate(360deg);
            }
            to{
                transform:rotate(-360deg);
            }
        }
        @-webkit-keyframes eyoha{
           from{
                transform:rotate(360deg);
            }
            to{
                transform:rotate(-360deg);
            }
        }
        .animate-bottom{
            position:relative;
            -webkit-animation-name:animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s;
        }
        
        @keyframes animatebottom{
             from {
                bottom:-100px; opacity:0;
            }
            to {
                bottom:0px; opacity:1;
            }
        }
        @-webkit-keyframes animatebottom{
             from {
                bottom:-100px; opacity:0;
            }
            to {
                bottom:0px; opacity:1;
            }
        }
        #loaded{
            display:none;
            
        }
 #spin{
            position:absolute;
            left:45%;
            top:45%; 
            transform:translate(-50%,-50%);
            z-index:1;
             margin:auto;
            width:50px;
            height:50px;
            border:5px solid #f3f3f3;
            border-radius: 50%;
            border-top:5px solid #00bfff;
            -webkit-animation: spin 2s linear infinite; 
            animation: spin 2s linear infinite;
        }
      
    </style>
     <script>
         var myVar;
        function Load(){
            myVar = setTimeout(showPage,4000);
        }
        function showPage(){
            document.getElementById("loader").style.display="none";
             document.getElementById("loaded").style.display = "block";

        }
    </script>

</head>
<body onload="Load()" style="">

<?php 
    $biz = $_GET['bussiness'];
    $ct = $_GET['city'];

    // $qc = "SELECT lat,lng,city FROM cities WHERE city='$ct'";
    // $sc = mysqli_query($conn,$qc);
    // $fc = mysqli_fetch_array($sc);
    // $city = $fc['city'];
    // $lat = $fc['lat'];
    // $lng = $fc['lng'];

    // $query = "SELECT lat,lng,name FROM bussiness WHERE category='$biz' AND city='$ct'";
    // $sql = mysqli_query($conn,$query);
    //   $arr_lat = array();
    //   $arr_lng = array();
    //   $arr_name = array();
    
    // if(mysqli_num_rows($sql)>0){
    //     while($fetch= mysqli_fetch_array($sql)){
    //         array_push($arr_lat,$fetch['lat']);
    //         array_push($arr_lng,$fetch['lng']);
    //         array_push($arr_name,$fetch['name']);
    //     }
    // }
    
?>

<div id="loader" style="position:absolute; top:50%; left:50%; transform:translate(-50%,-50%); display:flex; flex-direction:row;">

<span style="color:black; font-size:13px;"></span><span style="color:#00bfff; font-size:50px;" id="auto-write"></span>
<script>
    const text = document.getElementById("auto-write");
const prog = '...';

let idx=1;
setInterval(Write,250);

function Write(){
text.innerText = prog.slice(0,idx);
idx++;

if(idx>prog.length){
    idx=1;
}
}
</script>

</div>

<div id="loaded" class="">
   <a style="width:50px; height:50px;" class="btn" onclick="window.history.back()"><img src="../icons/Tg/PicsArt_02-18-11.36.20.png" width="40" height="40" style="cursor:pointer;" class=""></a>
    <p class="text-center reveal"><img src="../icons/covv.png" width="100%" height="300" style="max-width:500px;"></p>
    
    <div style="width:100%; height:300;" id="map">
        
    </div>

    <div class="mt-5">
        <?php
        // $city = strval($_GET['city']);
        // $city = str_replace('-',' ',$city);
        // $bussiness = strval($_GET['bussiness']);
        // $bussiness = str_replace('-',' ',$bussiness);
        ?>
        <p class="text-center">
            <?php 
            $bussiness = strval($_GET['bussiness']);
            $bussiness1 = str_replace('-',' ',$bussiness);
            $city = strval($_GET['city']);
             $city1 = str_replace('-',' ',$city);
            ?>
            <span class="results" style="font-size:15px;">Search results for <span style="color:#00bfff; font-size:15px;"><?php echo $bussiness1?></span> near <span style="color:#00bfff; font-size:15px;"><?php echo $city1?></span></span>
        </p>
    </div>
    <?php
     $query = "SELECT * FROM bussiness WHERE city='$city1' AND category='$bussiness1' AND acc_type !='none' ORDER BY acc_type DESC";
    $sql = mysqli_query($conn,$query);
    $suggested = mysqli_num_rows($sql); 

     $query1 = "SELECT * FROM bussiness WHERE city='$city1' AND category='$bussiness1'";
    $sql1 = mysqli_query($conn,$query1);
    $normal = mysqli_num_rows($sql1); 
    ?>

    <span class="recentlyopened" style="font-size:14px;">Suggested Results (<?php echo $suggested?>) </span>
<div class="ml-1 recent" style="">
         <div class="car ml-1" style="border:1px solid #e6e6ff;">
        <span class="bussiness-name"><a href="">Name
    </a><img src='../icons/green.png' width='15' height='15'>
  </span><br>
 <span style="font-size:11px;">7 reviews</span><br>
        <img src="../icons/full-star.png" width="20" height="20"> <span style="font-size:13px;">5</span>
        <br>
        <img src="../images/gugy.jpg" width="150" height="150" style="border-radius:10px;" class="mt-1"><br>
        <span><img src="../icons/destination.png" width="20" height="20"> <span style="font-size:12px;">Wonji</span></span>
        </div>
            
<span><p class="text-center" style="font-size:13px;"> No suggestion found.</p></span>
  
    
<hr>

      <div>
      
        <?php
         if(isset($_GET['page']) && $_GET['page']!=1){
                 $start = $_GET['page']+2;
                 $page = $_GET['page'];
            }
            if(isset($_GET['page'])){
                 include('res-paging.php');
            }

           if(!isset($_GET['page'])){
               include('res-paging.php');
           }
           
            $limit = 6;

             $query_last = "SELECT count(id) as num FROM bussiness WHERE category='$bussiness1' AND city='$city1'";
            $sql_last = mysqli_query($conn,$query_last);
            $fetch_last = mysqli_fetch_array($sql_last);
            $last = $fetch_last['num'];
            $maxpg = ceil($last/6);
           
            ?>
              <span class=""><strong class="recentlyopened" style="font-size:14px;">All results (<?php echo $last?>)</strong></span><br>
             <?php if($last >0){
                 ?>
                 <p class="text-center mt-1"><span style="font-size:13px;">Showing page 
                 <span style="color:#00bfff; font-size:14px;"><?php echo $page?></span> of <span style="color:#00bfff; font-size:14px;"><?php echo $maxpg?></span></span></p>
                 <?php
             }

             if($last <1){
                 ?>
                 <p class="text-center mt-1" style="font-size:13px;">Sorry, no result found.</p>
                 <?php
             }
             
             ?>
       
        <div class="container-fluid">
            <div class="row">
        <?php 
            $bussiness = strval($_GET['bussiness']);
            $city = strval($_GET['city']);

            $query = "SELECT * FROM bussiness WHERE category='$bussiness1' AND city='$city1' ORDER BY avg_rating DESC,views DESC limit $start,$limit";
            $sql = mysqli_query($conn,$query);
            

            while($fetch = mysqli_fetch_array($sql)){
               
                ?>
                          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-2 reveal" style="border-radius:10px; border:1px solid #e6e6ff; padding:5px;">
                          <span><a href="profile?id=<?php echo $fetch['id']?>"><?php echo $fetch['name']?></a> <img src="../icons/<?php echo $fetch['acc_type']?>.png" width="17" height="17"></span><br>
                          <?php 
                          $rate = $fetch['avg_rating'];
                          $whole = floor($rate);
                          $float = $rate-$whole;
                          $empty = floor(5-$rate);

                          if($rate>0){
                          
                            ?><img src="../icons/full-star.png" width="17" height="17"> <span style="font-size:13px;"><?php echo $rate?></span><?php
                          $id = $fetch['id'];
                          $numofrt = "SELECT count(id) AS num FROM rating WHERE bussiness='$id'";
                          $sqlrt = mysqli_query($conn,$numofrt);
                          $fetchrt = mysqli_fetch_array($sqlrt);
                          $num = $fetchrt['num'];
                          ?><br><span style="font-size:11px;"><?php echo $num?> reviews.</span><?php
                          
                          }
                          if($rate==0){
                              ?>
                              <span style="font-size:11px;">Not rated yet.<br>0 reviews.</span> 
                              <?php
                          }
                          ?>
                          
                          
                          <img src="../images/<?php echo $fetch['photo']?>" class="mt-1" width="100%" height="250"><br>
                          <?php 
                          if($fetch['address']!=''){
                              ?>
                              <img src="../icons/destination.png" width="16" height="16">&nbsp;
                              <span style="font-size:12px;"><?php echo $fetch['address'];?></span>
                              <?php
                          }
                          if($fetch['address']==''){
                              ?>
                              <img src="../icons/destination.png" width="16" height="16">&nbsp;
                              <span style="font-size:12px;">Unknown address.</span>
                              <?php
                          }
                          ?>
            </div>
               
    
    <?php  }
        ?>

            
        </div>
       <div class="mt-1">
        <?php
                    if(isset($_GET['page'])){
                         $biz = $_GET['bussiness'];
                         $ct = $_GET['city'];
      
                        $page = strval($_GET['page']);
                        

                        if($page==1 && $page<$maxpg){
                            ?>
                            <div style="float:right;" class="mr-2"><a href="search-results.php?bussiness=<?php echo $biz?> & city=<?php echo $ct?> & page=<?php echo $page + 1?>" class="btn text-white" style="font-size:25px;">></a></div>
                            <?php
                        }

                        
                        if($page>=2){
                            ?>
                            <div style="float:left;" class="ml-2"><a href="search-results.php?bussiness=<?php echo $biz?> & city=<?php echo $ct?> & page=<?php echo $page - 1?>" class="btn text-white" style="font-size:25px;"><</a></div>
                            <?php
                        }
                    }
                    if(!isset($_GET['page'])){
                          if($maxpg>1){
                            ?>
                            <div style="float:right;" class="mr-2"><a href="search-results.php?bussiness=<?php echo $biz?> & city=<?php echo $ct?> & page=2" class="btn text-white" style="font-size:25px;">></a></div>
                            <?php
                        }
                    }
                ?>
                </div>
                </div>
                </div>
   
    <hr><hr>
     
<div style="margin-top:70px;">
<footer class="mt-2">
    <p class="text-center" style="font-size:12px;">
    <span style="color:white; background-color:#00bfff; padding:5px; border-radius:10px;"><strong>Yetalle</strong></span> is a platform to help customers easily explore the goods and 
    services in their surrounding and to help bussinesses to easily be explored. <br>
   
</p>
<p class="text-center" style="font-size:12px;">
Want to get in touch?<br>
<a href="tel:+251 91 888 8225" style="text-decoration:none; font-size:12px; padding:5px; border-right:2px solid #00bfff; color:black;"><img class="myname" src="../icons/phone.png" width="20" height="20"> +251 91 888 8225</a>
<a href="tel:+251 91 888 8233" style="font-size:12px; padding:5px; border-right:2px solid #00bfff; color:black;"><img class="myname" src="../icons/phone.png" width="20" height="20"> +251 91 888 8233</a>

<a href="mailto:contact@yetale.com" style="font-size:12px; padding:3px; 
color:black;"><img class="myname" src="../icons/envelope-square-256.png" width="20" height="20"> contact@yetale.com</a>
<br><br>

Want to add your bussiness? <a href="register?" class="btn-sm text-white" style="">Sign Up</a> <br>
Want to explore bussinesses, share reviews, invite to friends, update your bussiness profile? <a href="login" class="btn-sm text-white">Login</a> <br>

 <span style="color:#00bfff; font-size:12px;">&copy; 20<?php echo date('y');?> YETALLE</span><br> <br>
</p>
</footer>
                </div>
    </div>
     </div>
      
<script src="../js/rev.js"></script>
    
    
</body>
</html>