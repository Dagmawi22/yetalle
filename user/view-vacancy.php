
<?php
session_start();
require("../db/connection.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>  Vacancy | Yetalle</title>
    <link rel="icon" type="image/png" href="../icons/yet.png" hreflang="en-us">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/mystyle.css?version=51">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <style>
        *{
            font-family: 'Source Sans Pro', sans-serif;
            font-style: normal;
            font-weight:400;
        }
        
        .m{
            padding:20px;
            background-color:white;
            border-radius:5px;
            min-width:200px;
            position:absolute;
            top:50%;
            left:50%;
            transform:translate(-50%,-50%);
            display:none;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.5);
        }
        #mm{
            opacity:1;
        }

#close{
    color:red;
    float:right;
    font-size: 20px;
    margin-top:-20px;
    margin-right:-10px;

}
#close:hover ,
#close:focus{
    color:red;
    text-decoration: none;
    cursor: pointer;
}



        video{
            width:100%;
            height:400px;
            object-fit:cover;
            
        }
        .logo-anim{
            animation:1s oo infinite;
        }
        @keyframes oo {
            0%,50%{
                opacity:0.5;
                width:100;
                height:100;
            }
            100%{
                opacity:0.8;
                width:200;
                height:200;
            }
        }
        .option{
            
            color:black;
            
            

        }
        a{
            text-decoration:none;
        }
         .animate-bottom{
            position:relative;
           
            animation-name: animatebottom;
            animation-duration: 2s;
        }
        
        @keyframes animatebottom{
             from {
                bottom:-100px; opacity:0;
            }
            to {
                bottom:0px; opacity:1;
            }
        }
    </style>
  
</head>
<body>
    <a style="width:50px; height:50px;" class="btn" onclick="window.history.back()"><img src="../icons/Tg/PicsArt_02-18-11.36.20.png" width="40" height="40" style="cursor:pointer;" class=""></a>
    
    <div class="mt-2">
    <?php
    $id = strval($_GET['id']);

    $query = "SELECT * FROM vacancy WHERE id='$id'";
    $sql = mysqli_query($conn,$query);
    ?>
    <div class="container">
     <div class="row">
    <?php

    while($fetch = mysqli_fetch_array($sql)){
         $biz = $fetch['bussiness'];
         $query_org = "SELECT name,logo FROM bussiness WHERE id='$biz'";
         $sql_org = mysqli_query($conn,$query_org);
         $fetch_org = mysqli_fetch_array($sql_org);
         $logo = $fetch_org['logo'];
         $name = $fetch_org['name'];
         ?>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" style="padding:15px;">
             <img src="../images/<?php echo $logo?>" width="100" height="100" style="border-radius:10px;"><br>
         <span style="font-size:2em;"><?php echo $name?></span><br>
        
        </div>
        
         <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" style="padding:15px;">
             <span style="font-size:18px;"><img src="../icons/usermale.png" width="25" height="25"> Position</span><br>
             <span style="font-size:13px;"><?php echo $fetch['position']?></span><br>
        
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" style="padding:15px;">
              <span style="font-size:18px;"><img src="../icons/lst.png" width="27" height="27"> Quantity Required</span><br>
             <span style="font-size:13px;"><?php echo $fetch['quan']?></span><br>
        
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" style="padding:15px;">
              <span style="font-size:18px;"><img src="../icons/232423534.png" width="27" height="27"> Tasks</span><br>
             <span style="font-size:13px;"><?php echo $fetch['tasks']?></span><br>
        
        </div>

         <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" style="padding:15px;">
              <span style="font-size:18px;"><img src="../icons/232423534.png" width="27" height="27"> Qualifications</span><br>
             <span style="font-size:13px;"><?php echo $fetch['req']?></span><br>
        
        </div>

         <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" style="padding:15px;">
              <span style="font-size:18px;"><img src="../icons/verified.png" width="22" height="22"> Experience</span><br>
             <span style="font-size:13px;"><?php echo $fetch['minex']?> - <?php echo $fetch['maxex']?> years.</span><br> 
        
        </div>

          <?php if($fetch['app_link']!=''){
                  ?>
                   <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" style="padding:15px;">
                  <span style="font-size:18px;"><img src="../icons/globe-480.png" width="22" height="22"> Application Link</span><br>
             <span style="font-size:13px;"><a href="<?php echo $fetch['app_link']?>"><?php echo $fetch['app_link']?></a></span><br> 
          </div>

                  <?php

              } ?>
            
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" style="padding:15px;">
                  <span style="font-size:18px;"><img src="../icons/calendar-512.png" width="22" height="22"> Deadline</span><br>
             <span style="font-size:13px;"><?php echo $fetch['dadeline']?></span><br> 
          </div>
              
            
            </div>
            </div>
           
        <?php
        $id = strval($_GET['id']);
        if(isset($_SESSION['email'])){
            $email = $_SESSION['email'];

             $query_applied_or_not = "SELECT * FROM job_application WHERE job_id=$id AND email='$email'";
        $sql_applied_or_not = mysqli_query($conn,$query_applied_or_not);
        $fetch_a_o_n = mysqli_fetch_array($sql_applied_or_not);
        $aon = mysqli_num_rows($sql_applied_or_not);
        if($aon >0){
            $app_time=$fetch_a_o_n['application_time'];
        }
        }
        

       
    }
        
    if($fetch['app_email']!=' ' && isset($_SESSION['email']) && $_SESSION['type']=='user' && $aon<1){
        ?>
             <p class="text-center container">
        <button class="btn" id="tomodal" onclick="openmodal()" style="width:200px; font-size:17px;">Apply</button>
       
        <script>
            function openmodal(){
                document.getElementById("modal").style.display="block";
                document.getElementById("modal").scrollIntoView();
            }
            
        </script>
    </p>
        <?php
    }
     if(!isset($_SESSION['email']) || $_SESSION['type']=='bussiness'){
        ?>
             <p class="text-center container">
        <button class="btn" id="tomodal" onclick="openmodal()" style="width:200px; font-size:17px;" disabled>Apply</button><br>
        <span style="font-size:14px;">Please  <span><a href="login" style="font-size:14px;"><u>login</u></a></span> first to apply.</span>
        <script>
            function openmodal(){
                document.getElementById("modal").style.display="block";
                document.getElementById("modal").scrollIntoView();
            }
            
        </script>
    </p>
        <?php
    }

    if(isset($_SESSION['email']) && $aon==1){
        ?>
             <p class="text-center container">
        <button class="btn" id="tomodal" onclick="openmodal()" style="width:200px; font-size:17px;" disabled>Apply</button><br>
        <span style="font-size:14px;">You already applied to this job on  <span>
            <a style="font-size:14px; "><a style="color:#00bfff;"><?php echo $app_time?></a></a></span>.</span>
        <script>
            function openmodal(){
                document.getElementById("modal").style.display="block";
                document.getElementById("modal").scrollIntoView();
            }
            
        </script>
    </p>
        <?php
    }
    
    ?>
   
    <hr>
    <hr>

    <div id="modal" class="animate-bottom" style="z-index:100; transition:2s; display:none; position:absolute; top:5%; left:5px; width:98%; height:95vh; background-color:white; box-shadow: 0px 8px 16px 0px rgba(1,1,1,1.5);">
            <span style="float:right; color:red; font-size:30px; cursor:pointer;" class="mr-3 mt-2" id="close" onclick="closemodal()">&times;</span><br><br>
            <script>
            function closemodal(){
                document.getElementById("modal").style.display="none";
            }
        </script>
            <form method="post" enctype="multipart/form-data">
                <span class="ml-2">CV <span style="color:red;">*</span></span><br>
                <span style="font-size:14px;" class="ml-2">pdf/doc/docx file with maximum size of 3MB.</span>
                <div><input type="file" name="cv" class="form-control ml-2" accept=".pdf,.doc,.docx" style="width:95%;" required></div><hr>

                <span class="ml-2">Cover Letter</span><br>
                
                <div><textarea type="file" accept="pdf" name="cover" maxlength="500" class="form-control ml-2" style="width:95%;"></textarea></div><hr>
                <input type="submit" name="apply" value="Apply" class="btn ml-2" style="width:95%; font-size:17px;">
            </form>
            <?php
            if(isset($_POST['apply'])){
                $cv = $_FILES['cv']['size']/1024/1024;
                $cover = mysql_real_escape_string($_POST['cover']);
                $cover = str_replace('\n','<br>',$cover);
                if($cv>3){
                    echo "<script>alert('Yes');</script>";
                }
                
                if($cv<=3){
                    $id = strval($_GET['id']);
                   $query = "SELECT people_applied,app_email,position FROM vacancy WHERE id=$id";
                   $sql = mysqli_query($conn,$query);
                   $fetch = mysqli_fetch_array($sql);
                   $people = $fetch['people_applied']+1;
                   $em = $fetch['app_email'];
                   $pos = $fetch['position'];
                   $query = "UPDATE vacancy SET people_applied=$people WHERE id=$id";

                $cv =  str_shuffle("ygfudfcywfucfyufcyy").$_FILES['cv']['name'];
                   $Temp =  $_FILES['cv']['tmp_name'];
             $MyLocation= "../files/".$cv ;
             move_uploaded_file($Temp,$MyLocation);

             $sender = $_SESSION['email'];
             $recepient = $em;
             $subject = "Application to '$pos' via https://www.yetale.com";
             $body = $cover;
             $headers  = "From :".$sender."/r/n Reply to:".$recepient."/r/n";
             
             $boundary = md5("random");

            

                // (A) EMAIL SETTINGS
$mailTo = $em;
$mailSubject = "Application to <strong>$pos</strong> from <a href='mailto:$sender'>$sender</a> via https://www.yetale.com";
$mailMessage = "<strong>$pos</strong>";
$mailAttach = "../files/$cv";

// (B) GENERATE RANDOM BOUNDARY TO SEPARATE MESSAGE & ATTACHMENTS
// https://www.w3.org/Protocols/rfc1341/7_2_Multipart.html
$mailBoundary = md5(time());
$mailHead = implode("\r\n", [
"MIME-Version: 1.0",
"Content-Type: multipart/mixed; boundary=\"$mailBoundary\""
]);

// (C) DEFINE THE EMAIL MESSAGE
$mailBody = implode("\r\n", [
"--$mailBoundary",
"Content-type: text/html; charset=utf-8",
"",
$mailMessage
]);

// (D) MANUALLY ENCODE & ATTACH THE FILE
$mailBody .= implode("\r\n", [
"$cover",
"--$mailBoundary",
"Content-Type: application/octet-stream; name=\"". basename($mailAttach) . "\"",
"Content-Transfer-Encoding: base64",
"Content-Disposition: attachment",
"",
chunk_split(base64_encode(file_get_contents($mailAttach))),
"--$mailBoundary--"
]);

$mail = mail($mailTo, $mailSubject, $mailBody, $mailHead);

// (E) SEND
// mail($mailTo, $mailSubject, $mailBody, $mailHead)
//? "OK" : "ERROR" ;
// remove the cv
 rename('../files/'.$cv,'../del/'.$cv);
 $id = strval($_GET['id']);

    $q = "INSERT INTO job_application VALUES('$sender',$id,NOW())";
    $s = mysqli_query($conn,$q);
     ?>

     <script>alert('Successfully applied!'); location.href='view-vacancy?id=<?php echo $id?>';</script>
     <?php
 
 
 
 
 
}}

            
            ?>
    </div>
<footer class="mt-2">
    <p class="text-center" style="font-size:14px;">
    <span style="color:white; background-color:#00bfff; padding:5px; border-radius:10px;"><strong>Yetalle</strong></span> is a platform to help customers easily explore the goods and 
    services in their surrounding and to help bussinesses to easily be explored. <br>
   
</p>

<?php include('../includes/contacts.htm');?>
<?php include('../includes/social.htm');?>
</footer>

    
    
</body>
</html>