
<?php
session_start();
require("../db/connection.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Events | Yetalle</title>
    <link rel="icon" type="image/png" href="../icons/yet.png" hreflang="en-us">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/mystyle.css?version=51">
    <style>
        .m{
            padding:20px;
            background-color:white;
            border-radius:5px;
            min-width:200px;
            position:absolute;
            top:50%;
            left:50%;
            transform:translate(-50%,-50%);
            display:none;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.5);
        }
        #mm{
            opacity:1;
        }

#close{
    color:red;
    float:right;
    font-size: 20px;
    margin-top:-20px;
    margin-right:-10px;

}
#close:hover ,
#close:focus{
    color:red;
    text-decoration: none;
    cursor: pointer;
}



        video{
            width:100%;
            height:400px;
            object-fit:cover;
            
        }
        .logo-anim{
            animation:1s oo infinite;
        }
        @keyframes oo {
            0%,50%{
                opacity:0.5;
                width:100;
                height:100;
            }
            100%{
                opacity:0.8;
                width:200;
                height:200;
            }
        }
        .option{
            
            color:black;
            
            

        }
        a{
            text-decoration:none;
        }
    </style>
  
</head>
<body>
    <a href="../HOME" style="position:absolute; top:0; left:0;"><img src="../icons/yet.png" width="70" height="70"></a>
    <div id="none-modal">
    <?php
    $id = strval($_GET['id']);

    $query = "SELECT * FROM bussiness WHERE id='$id'";
    $sql = mysqli_query($conn,$query);
    $fetch = mysqli_fetch_array($sql);
    $photo = $fetch['photo'];
    $category = $fetch['category'];
     $city = $fetch['city'];
     $logo = $fetch['logo'];
     $lvl = $fetch['acc_type'];
     $vid = $fetch['video'];
     $name = $fetch['name'];

    ?>
   
         <div class="image mb-2">
        <img src="../images/<?php echo $photo?>" width="100%" height="300">
    </div>
     <div class="nav" style="z-index:100;">
<a href="profile?id=<?php echo $id?>" style="font-size:13px;">Bio & Review</a>
<a href="images?id=<?php echo $id?>" style="font-size:13px;">Images</a>
<a href="" style="color:white; border-bottom:4px solid white; font-size:13px;">Events</a>
<a href="vacancy?id=<?php echo $id?>" style="font-size:13px;">Vacancy</a>
</div>
    <div class="info">
        <p class="text-center" id="info">
            <img src="../images/<?php echo $logo?>" width="100" height="100" style="border-radius:100%;"><br> 
            <strong style="font-size:2em;"><?php echo $fetch['name']?></strong><br>
            <span style="font-size:14px;">Since <?php echo $fetch['date_founded']?></span><br>
            <span style="color:#00bfff; font-weight:700;">@<?php echo $fetch['id']?></span><br>
           
           
        </div>

       <span class="recentlyopened">Upcoming Events</span>
<div class="ml-1 recent" style="">
    <?php
    $today = date('20y-m-d');
    $query = "SELECT * FROM events WHERE starts_on >='$today' AND bussiness='$id' ORDER BY starts_on ASC";
    $sql = mysqli_query($conn,$query);
    
    if(mysqli_num_rows($sql)<1){
        ?>
        <p class="text-center">No upcoming event found from <?php echo $name?>.</p>
        <?php
    }

    if(mysqli_num_rows($sql)>0){
        ?>
         <div class="container-fluid">
        <div class="row" style="">
        <?php
       while($fetch = mysqli_fetch_array($sql)){
           $photo = $fetch['photo'];
            ?>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" style="box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2); border-bottom:1px solid #00bfff; padding:15px;">
                <?php
                    $biz= $fetch['bussiness'];
                    $query_biz = "SELECT logo,name FROM bussiness WHERE id='$id'";
                    $sql_biz = mysqli_query($conn,$query_biz);
                    $fetch_biz = mysqli_fetch_array($sql_biz);
                    $logo = $fetch_biz['logo'];
                    $name = $fetch_biz['name']; 
                ?>
                <div><a href=""><img src="../images/<?php echo $logo?>" width="30" height="30" style="border-radius:100%;" alt="user"> 
                <span><?php echo $name?></span></a>
       </div><br>
              <?php $date = strtotime($fetch['starts_on']);
                   $today = strtotime(date('20y-m-d'));

                   $interval = ($today-$date)/60/60/24;
                  if($interval >= 0 && $interval <= $fetch['days']){
                      ?>
                      <span><span class="mb-1"><img src="icons/174-01-512.png" width="18" height="18"> <span style="border-bottom:2px solid #00bfff;">happening now</span></span></span><br>
                      <?php
                  }
                  if($interval <0 && $interval>$fetch['days']){
                      $id = $fetch['id'];
                      $query_del = "DELETE FROM events WHERE id=$id";
                      $sql_del = mysqli_query($conn,$query_del);
                  }
                   ?>
                <span> <img src="../images/<?php echo $fetch['photo']?>" width="100%" height="200" style="border-radius:10px;" class="mt-1"></span><br>
                <span style="font-size:16px; text-transform:uppercase; color:#00bfff;"><?php echo $fetch['caption']?></span><br>
                 <span> <img src="../icons/destination.png" width="18" height="18"> <?php echo $fetch['place']?></span><br>
                <?php $date = $fetch['starts_on'];
                   $day = date('D',strtotime($date));
                   
                   
                   ?>

                <span><img src="../icons/calendar-512.png" width="18" height="18"> <?php echo $day?>, <?php echo $fetch['starts_on']?><br>
                <span><img src="../icons/business_12-20-512.png" width="18" height="18"> <?php echo $fetch['days']?> days<br>
                
               
            </div>
            <?php

      }  }
?>
    
       
     
</div>
<hr>

     
     </div>
     
    
</body>
</html>