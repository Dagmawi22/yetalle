<?php
session_start();

if(!isset($_SESSION['email'])  || $_SESSION['type']!='bussiness'){
    ?>
    <script>alert('Please login first.'); location.href="../user/login";</script>
    <?php
}
require("../db/connection.php");

if(isset($_COOKIE['email']) && isset($_COOKIE['lat']) && isset($_COOKIE['lng'])){
$email = $_COOKIE['email'];
$lat = $_COOKIE['lat'];
$lng = $_COOKIE['lng'];

	$query = "UPDATE bussiness SET lat='$lat',lng='$lng' WHERE email='$email'";
	$sql = mysqli_query($conn,$query);
} 

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Accounts | Yetale</title>
     <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/mystyle.css?version=50">
     <style>
        *{
            font-size:12px;
        }
    </style>
</head>
<body>
    <?php
    $email = $_SESSION['email'];
    $type = $_SESSION['type'];

    if($type=='bussiness'){
         $query = "SELECT * FROM bussiness WHERE email='$email'";
    $sql = mysqli_query($conn,$query);
    $fetch = mysqli_fetch_array($sql);
    $name = $fetch['name'];
     $photo = $fetch['photo'];
    $lvl = $fetch['acc_type'];
    $view = $fetch['views'];
    $id = $fetch['id'];
    $logo = $fetch['logo'];
    $desc= $fetch['description'];

    }?>
    <div class="topnav" id="myTopnav" style="position:fixed; z-index:100; background-color:#00bfff; width:100vw;">
        <a href="../HOME" class="mr-5"><img src="../../icons/yet.png" width="60" height="60"></a>
         <?php
        if(isset($_SESSION['email']) && $_SESSION['type']=='bussiness'){
            ?>
                <a href="" class="text-center" style="color:black; font-weight:600; text-decoration:none; margin:0 auto;">
         <img src="../../images/<?php echo $photo?>" width="100" height="100" style="border-radius:100%;"><br>  
       <span style="text-transform:uppercase; font-size:27px; color:white;"><?php echo $name?></span>
       
   
     <a href="log-out.php" class="text-center" style="color:white; font-weight:600; text-decoration:none; margin-top:-35px;">
           
        Log Out</a>
    </a>
            <?php
        }
        ?>
        <a href="my-profile" class="" style="font-weight:600; color:black; text-decoration:none; margin-top:-20px;">
        <!-- <img src="icons/destination.png" width="18" height="18">  -->
        My Profile</a>
        <a href="" class="" style="font-weight:600; color:white; border-bottom:5px solid white; text-decoration:none;">
        <!-- <img src="icons/blog 1.png" width="18" height="18">  -->
        Get Badge</a>
        <a href="my-reviews" class="" style="color:black; font-weight:600; text-decoration:none;">
         <!-- <img src="icons/food.png" width="18" height="18">  -->
        My Reviews</a>
        <a href="../user/profile?id=<?php echo $id?>" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        View-As</a>
        <a href="my-events" class="" style="color:black; font-weight:600; text-decoration:none;">
         <!-- <img src="icons/food.png" width="18" height="18">  -->
        Post an Event</a>
        <a href="my-vacancy" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        Post a Job</a>
        
        
       <a href="javascript:void(0);" class="icon mr-3 mt-3" style="float:right; margin-right:50px;" onclick="myFunction()"> 
           <img src="../../icons/2747254.png" width="40" height="40" style="filter:invert(1);" class=""> 
            </a>
            <br>
             </div>
        
    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
   
    <!--end of nav-->
    <br><br><br><br><br>
    
    
    <?php
        
    ?>
    <div class="alert alert-info">
        The places in the images below aren't real places. They are only for illustration.
    </div>
    
<div class="level container-fluid ml-5 mt-2" style="color:black">
        <span style="font-size:17px;">ብርማ ባጅ</span> <img src="../../icons/blue.png" width="20" height="20"> </div><br>
  
    <div class="container-fluid leveldesc">
        የሚያስገኛቸው ጥቅሞች:<br>
        <div class="row">

            
            
           <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
           1/ ምርት እና አገልግሎትዎን የሚገልፁ እስከ 10 ፎቶዎች መለጠፍ ያስችልዎታል::<br>    
           <img src="../../demo/post_photo.png" width="100%" height="300" style="margin:auto;"></div>
            
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
            2/ በተመሳሳይ ንግዶች ገፅ ላይ የእርስዎን ንግድ ይጠቁማል:: <br>    
            <img src="../../demo/suggestion.png" width="100%" height="300" style="margin:auto;"></div>
           
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
             3/ ደንበኞች ፍለጋ ሲያደርጉ የእርስዎን ንግድ አስቀድሞ ይጠቁማል::<br>    
            <img src="../../demo/suggestion.png" width="100%" height="300" style="margin:auto;"></div>
            
             <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
              4/ ደንበኞች በቀላሉ እንዲደውሉልዎት እና ኢሜይል እንዲልኩልዎት ያስችላል::<br>    
             <img src="../../demo/call-em.png" width="100%" height="300" style="margin:auto;"></div>
             </div>
            <p class="text-center">
                
                <div class="mt-2">
                <img src="../../icons/1614832.png" width="20" height="20"> <span style="font-size:14px;">1800ብር/አመት</span></div>
               
               
            </p>
        </div>
    </div>
    <div>
        <img src="../../icons/pay.gif" width="100%" height="200">
    </div>
    <?php
        $query = "SELECT phone FROM bussiness WHERE email='$email'";
        $sql = mysqli_query($conn,$query);
        $fetch = mysqli_fetch_array($sql);
        $phone = $fetch['phone'];
         $p1 = substr($phone,0,2);
        $p2 = substr($phone,2,2);
        $p3 = substr($phone,4,2);
        $p4 = substr($phone,6,2);
        $p5 = substr($phone,8,2);
        $phone = $p1.' '.$p2.' '.$p3.' '.$p4.' '.$p5;
    ?>
    <p class="text-center">ብርማ ባጅ ለማግኘት <br>ከ <span style="font-size:17px; padding:3px; border-radius:5px; background-color:#00bfff; color:white;"><?php if($phone !='') echo $phone; if($phone==' ' || $phone==0) echo "<a href='profile.php' class='text-white' style='font-size:11px; text-decoration:underline;'>Fill your mobile number first.</a>"?></span><br><br> ወደ&nbsp;&nbsp; <span class="" style="font-size:17px; padding:3px; border-radius:5px; background-color:#00bfff; color:white;">09 18 88 82 25</span><br>
        <br>1800ብር በነዚህ ይክፈሉ<br>
        <img src="../../icons/amole.png" width="50" height="50" style="border-radius:100%;">&nbsp;
        <img src="../../icons/hellocash.jpg" width="50" height="50" style="border-radius:100%;">&nbsp;
        <img src="../../icons/telebirr.png" width="50" height="50" style="border-radius:100%;">
         <img src="../../icons/cbe.png" width="50" height="50" style="border-radius:100%;">&nbsp;
        <img src="../../icons/ebirr.png" width="50" height="50" style="border-radius:100%;">&nbsp;
        <img src="../../icons/awash.png" width="50" height="50" style="border-radius:100%;">
        <br>
        <span>ለበለጠ መረጃ በ<a href="https://www.t.me/yetale01">ቴሌግራም</a> ያናግሩን</span>
    </p>
    <hr>

   <div class="level container-fluid ml-5 mt-5" style="color:black">
        <span style="font-size:17px;">ወርቃማ ባጅ</span> <img src="../../icons/green.png" width="20" height="20"> </div><br>
    <div class="container-fluid leveldesc">
        የሚያስገኛቸው ጥቅሞች:<br>
        <div class="row">

            
           
             <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
              1/ ምርት እና አገልግሎትዎን የሚገልፁ እስከ 10 ፎቶዎች መለጠፍ ያስችልዎታል::<br>    
             <img src="../../demo/post_photo.png" width="100%" height="300" style="margin:auto;"></div>
           
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4"> 
             2/ ከእርስዎ ገፅ ላይ የተፎካካሪ ንግዶችን ጥቆማ ያጠፋል::<br>
            3/ የተመሳሳይ ንግዶች ገፅ ላይ የእርስዎን ንግድ ይጠቁማል::<br>    
            <img src="../../demo/suggestion.png" width="100%" height="300" style="margin:auto;"></div>
            
            
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
            4/ ደንበኞች በቀላሉ እንዲደውሉልዎት እና ኢሜይል እንዲልኩልዎት ያስችላል::<br>    
            <img src="../../demo/call-email.png" width="100%" height="300" style="margin:auto;"></div>
           
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4"> 
             5/ ገፅዎን ወደ ማህበራዊ ሚዲያዎች እንዲያጋሩ ያስችላል::<br>
            6/ ሁነቶች እንዲለጥፉ ያስችላል::<br>    
            <img src="../../demo/event.png" width="100%" height="300" style="margin:auto;"></div>
            
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
            7/ የስራ ማስታወቂያ እንዲለጥፉ ያስችላል::<br>    
            <img src="../../demo/vacancy.png" width="100%" height="300" style="margin:auto;"></div>
        
        </div>
        </div>
         <p class="text-center">
               <div class="mt-2">
                <img src="../../icons/1614832.png" width="20" height="20"> <span style="font-size:14px;">3200ብር/አመት</span></div>
            </p>
           
    </div>
    <?php 
       
    ?>
     <div>
        <img src="../../icons/pay.gif" width="100%" height="200">
    </div>
    <?php
        $query = "SELECT phone FROM bussiness WHERE email='$email'";
        $sql = mysqli_query($conn,$query);
        $fetch = mysqli_fetch_array($sql);
        $phone = $fetch['phone'];
        $p1 = substr($phone,0,2);
        $p2 = substr($phone,2,2);
        $p3 = substr($phone,4,2);
        $p4 = substr($phone,6,2);
        $p5 = substr($phone,8,2);
        $phone = $p1.' '.$p2.' '.$p3.' '.$p4.' '.$p5;
    ?>
    <p class="text-center">ወርቃማ ባጅ ለማግኘት <br>ከ <span style="font-size:17px; padding:3px; border-radius:5px; background-color:#00bfff; color:white;"><?php if($phone !='') echo $phone; if($phone==' ' || $phone==0) echo "<a href='profile.php' class='text-white' style='font-size:11px; text-decoration:underline;'>Fill your mobile number first.</a>"?></span><br><br> ወደ&nbsp;&nbsp; <span class="" style="font-size:17px; padding:3px; border-radius:5px; background-color:#00bfff; color:white;">09 18 88 82 25</span><br>
        <br>3200ብር በነዚህ ይክፈሉ<br>
        <img src="../../icons/amole.png" width="50" height="50" style="border-radius:100%;">&nbsp;
        <img src="../../icons/hellocash.jpg" width="50" height="50" style="border-radius:100%;">&nbsp;
        <img src="../../icons/telebirr.png" width="50" height="50" style="border-radius:100%;">
         <img src="../../icons/cbe.png" width="50" height="50" style="border-radius:100%;">&nbsp;
        <img src="../../icons/ebirr.png" width="50" height="50" style="border-radius:100%;">&nbsp;
        <img src="../../icons/awash.png" width="50" height="50" style="border-radius:100%;">
        <br>
        <span>ለበለጠ መረጃ በ<a href="https://www.t.me/yetale01">ቴሌግራም</a> ያናግሩን</span>
    </p>
    <hr>
    <hr>

<footer class="mt-2">
    <p class="text-center" style="font-size:12px;">
    <span style="color:white; background-color:#00bfff; padding:5px; border-radius:10px;"><strong>Yetale.et</strong></span> 
    የንግድ ባለቤቶች በቀላሉ ምርትና አገልግሎታቸውን ከሚፈልጉ ደንበኞቻቸው ጋር በቀላሉ መገናኘት 
     እንዲችሉ እንዲሁም ደንበኞች የሚፈልጉትን በቀላሉ እንዲያገኙ የሚረዳ ድህረ-ገፅ ነዉ:: <br>
   
</p>
<p class="text-center" style="font-size:12px;">
ሊያገኙን ይፈልጋሉ?<br>
<a href="tel:+251 91 888 8225" style="text-decoration:none; font-size:12px; padding:5px; border-right:2px solid #00bfff; color:black;"><img class="myname" src="../../icons/phone.png" width="20" height="20"> +251 91 888 8225</a>
<a href="tel:+251 91 888 8233" style="font-size:12px; padding:5px; border-right:2px solid #00bfff; color:black;"><img class="myname" src="../../icons/phone.png" width="20" height="20"> +251 91 888 8233</a>

<a href="mailto:contact@yetale.com" style="font-size:12px; padding:3px; 
color:black;"><img class="myname" src="../../icons/envelope-square-256.png" width="20" height="20"> contact@yetale.com</a>
<br>

 <span style="color:#00bfff; font-size:12px;">&copy; 20<?php echo date('y');?> የታለ</span><br> <br>
</p>
</footer>
    
    
</body>
</html>