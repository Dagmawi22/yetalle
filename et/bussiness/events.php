<?php
session_start();
if(!isset($_SESSION['email']) || $_SESSION['type']=='user'){
    ?>
    <script>
        alert('You are not logged in.');
        location.href="../user/login";
    </script>
    <?php
}

require("../db/connection.php");
if(isset($_COOKIE['email']) && isset($_COOKIE['lat']) && isset($_COOKIE['lng'])){
$email = $_COOKIE['email'];
$lat = $_COOKIE['lat'];
$lng = $_COOKIE['lng'];

	$query = "UPDATE bussiness SET lat='$lat',lng='$lng' WHERE email='$email'";
	$sql = mysqli_query($conn,$query);
} 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Event | Yetale </title>
     <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/mystyle.css?version=50">
    <style>
         input{
            width:250px;
            height:35px;
            border:1px solid #00bfff;
            border-radius:5px;
            margin-bottom:10px;
        }
         
         @media screen and (min-width: 470px){
             .form{
                margin-left:30px;
             }
           
        }
         @media screen and (min-width: 700px){
             .form{
                margin-left:70px;
             }
           
        }
         @media screen and (min-width: 850px){
             .form{
                margin-left:150px;
             }
           
        }
        *{
            font-size:14px;
        }
    </style>
</head>
<body>
    <?php 
    
    $email = $_SESSION['email'];
     $query = "SELECT * FROM bussiness WHERE email='$email'";
    $sql = mysqli_query($conn,$query);
    $fetch = mysqli_fetch_array($sql);
    $name = $fetch['name'];
     $photo = $fetch['photo'];
    $lvl = $fetch['acc_type'];
    $view = $fetch['views'];
    $id = $fetch['id'];
    $logo = $fetch['logo'];
    $desc= $fetch['description'];

    ?>
    <div class="topnav" id="myTopnav" style="position:fixed; background-color:#00bfff; width:100%; z-index:100;">
        <a href="../HOME" class="mr-5"><img src="../../icons/yet.png" width="60" height="60"></a>
         <?php
        if(isset($_SESSION['email']) && $_SESSION['type']=='bussiness'){
            ?>
                <a href="" class="text-center" style="color:black; font-weight:600; text-decoration:none; margin:0 auto;">
         <img src="../../images/<?php echo $photo?>" width="100" height="100" style="border-radius:100%;"><br>  
       <span style="text-transform:uppercase; font-size:27px; color:white;"><?php echo $name?></span>
       
   
     <a href="log-out.php" class="text-center" style="color:white; font-weight:600; text-decoration:none; margin-top:-35px;">
           
        Log Out</a>
    </a>
            <?php
        }
        ?>
        <a href="my-profile" class="" style="font-weight:600; color:black; text-decoration:none; margin-top:-20px;">
        <!-- <img src="icons/destination.png" width="18" height="18">  -->
        My Profile</a>
       <a href="my-badge" class="" style="color:black; font-weight:600; text-decoration:none;">
         <!-- <img src="icons/food.png" width="18" height="18">  -->
        Get Badge</a>
        <a href="my-reviews" class="" style="color:black; font-weight:600; text-decoration:none;">
         <!-- <img src="icons/food.png" width="18" height="18">  -->
        My Reviews</a>
        <a href="../user/profile?id=<?php echo $id?>" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        View-As</a>
         <a href="" class="" style="font-weight:600; color:white; border-bottom:5px solid white; text-decoration:none;">
        <!-- <img src="icons/blog 1.png" width="18" height="18">  -->
        Post an Event</a>
        
        <a href="my-vacancy" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        Post a Job</a>
        
        
       <a href="javascript:void(0);" class="icon mr-3 mt-3" onclick="myFunction()"> 
           <img src="../../icons/2747254.png" width="40" height="40" style="filter:invert(1);" class="ham"> 
            </a>
            <br>
             </div>
        
    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
   
    <!--end of nav-->
    <br><br><br><br><br>
<span class="recentlyopened">የእርስዎ ሁነቶች</span>
    

<div class="ml-1 recent">
       
            <?php
                $query = "SELECT * FROM events WHERE bussiness='$id'";
                $sql = mysqli_query($conn,$query);
                if(mysqli_num_rows($sql)<1){
                    ?>
                    <p class="text-center">የቅርብ ሁነት የልዎትም::</p>
                    <?php
                }

                if(mysqli_num_rows($sql)>0){
                   while($fetch = mysqli_fetch_array($sql)){
                       ?>
                         <div class="car ml-1">
                             
                             <span><img src="../../images/<?php echo $fetch['photo']?>" width="150" height="150" style="border-radius:10px;"></span><br>
                             <a href="my-events?id=<?php echo $id?> && del=<?php echo $fetch['id']?>" class="btn mt-1 btn-sm text-white"><img src="../../icons/Tg/PicsArt_02-18-11.34.45.png" width="16" height="16"> Cancel</a><br>
                             <span style="font-weight:800; font-size:19px;"><?php echo $fetch['caption']?></span><br>
                             <span><?php echo $fetch['place']?></span><br>
                             <div style="font-size:13px;"><?php echo $fetch['starts_on']?>&nbsp;
                             <?php echo $fetch['tym']?></div><br>
                             
                         </div>
                       <?php
                       if(isset($_GET['del'])){
                           
                                
                                         $id = $_GET['del'];
                                         $q = "SELECT photo FROM events WHERE id=$id";
                                         $s = mysqli_query($conn,$q);
                                         $f = mysqli_fetch_array($s);
                                         $photo = $f['photo'];
                                         rename('../../images/'.$photo,'../../del/'.$photo);
                                            $query_del = "DELETE FROM events WHERE id=$id";
                                            $sql_del = mysqli_query($conn,$query_del);
                                            $query_del = "DELETE FROM interested WHERE id=$id";
                                            $sql_del = mysqli_query($conn,$query_del);
                                             $query_del = "DELETE FROM feed WHERE id=$id";
                                            $sql_del = mysqli_query($conn,$query_del);
                                            ?>
                                            <script>alert('ተሰርዝዋል::'); location.href="my-events";</script><?php
                                          

                               
                                                                 
                                           
                                    }
                               
                       }
                   }
               
            ?>
       
</div>
<br>
<span class="recentlyopened"><img src="../../icons/f196-128.png" width="25" height="30"> አዲስ ሁነት ለጥፍ</span>

<div class="container">
<br>    
<form method="post" enctype="multipart/form-data">

<span>ርዕስ</span><br>            
<input type="text" name="title" placeholder="ምሳሌ: የደብ ሙዚቃ ፌስቲቫል" class="form-control" maxlength="50" autofocus required>

<label for="image" class="form-control" style="background-color:#e6e6ff; border-bottom:2px solid #00bfff;"><img src="../../icons/camera.png" width="22" height="22"> ፎቶ አያይዝ</label><br>            
<input type="file" id="image" name="photo" accept="image/*" class="form-control" hidden>

 <span>ቦታ</span><br>            
<input type="text" name="place" placeholder="ምሳሌ: ሚሊኒየም አዳራሽ" class="form-control" required>

<span>የሚጀምርበት ቀን</span>   
<?php $today = date('20y-m-d');?>        
<input type="date" name="starts_on" min="<?php echo $today?>" class="form-control" style="font-size:12px; background-color:#e6e6ff; border-bottom:2px solid #00bfff;" required>

<span>ሰዐት</span>    
<input type="time" name="starts_at" class="form-control" style="background-color:#e6e6ff; border-bottom:2px solid #00bfff;" required>

<span>የሚቆይበት ቀን</span>    
<input type="number" name="days" class="form-control" placeholder="ምሳሌ: 2" min="1" style="background-color:#e6e6ff; border-bottom:2px solid #00bfff;" required>


<span>ቲኬት እንዴት ይገኛል</span><br>            
<input type="text" name="ticket" placeholder="ምሳሌ: በሁሉም የንግድ ባንክ ቅርንጫፎች ወይም በአሞሌ" class="form-control" maxlength="50">
<?php $email = $_SESSION['email'];
      $query = "SELECT acc_type,expiry FROM bussiness WHERE email='$email'";
      $sql = mysqli_query($conn,$query);
      $fetch = mysqli_fetch_array($sql);
      $badge = $fetch['acc_type'];
      $expiry = $fetch['expiry'];
      $today = date('20y-m-d');

      if($badge!='none' && $expiry>=$today){
          ?>
        <p class="text-center"><input type="submit" class="btn text-white" name="add" value="ለጥፍ"></p>
          <?php

      }
      else {
         ?>
         <p class="text-center" style="font-size:12px;">
         <input type="submit" value="Post" class="btn" style="cursor:pointer;" disabled><br>

         Sorry, you don't have a green badge or it's just expired.<br>
         want to get or renew your badge? <a href="my-accounts" style="text-decoration:underline;">click here</a>
         
         </p><?php
      }?>
    </form>
</div>
<?php
    if(isset($_POST['add'])){
      
       
        $title = mysql_real_escape_string($_POST['title']);
        $title = str_replace('\n','<br>',$title);
        $place = mysql_real_escape_string($_POST['place']);
        $ticket = mysql_real_escape_string($_POST['ticket']);
        $place = str_replace('\n','<br>',$place);
        $start = $_POST['starts_on'];
         $days = $_POST['days'];
       $end = date('Y-m-d', strtotime($start. ' + '.$days.' days'));
      
        $tym = $_POST['starts_at'];
       
        $datetime= date('Y-m-d').' '.date('h:i:sa');

         $photo =str_shuffle('fuyfyfufu').$_FILES['photo']['name'];
             //echo $FileName;
             $Temp =  $_FILES['photo']['tmp_name'];
             $MyLocation= "../../images/".$photo ;
             move_uploaded_file($Temp,$MyLocation);
        $qi = "SELECT COUNT(id) AS num FROM events";
        $si = mysqli_query($conn,$qi);
        $fi = mysqli_fetch_array($si);
        $event_id = $fi['num']+1;
        $query = "INSERT INTO events VALUES
                 ($event_id,'$id','$title','$photo','$place','$start','$end','$tym',$days,'$datetime','$ticket')";
        $sql = mysqli_query($conn,$query);
        if($sql){
            $email = $_SESSION['email'];
            $q = "SELECT name,id FROM bussiness WHERE email='$email'";
            $s = mysqli_query($conn,$q);
            $f = mysqli_fetch_array($s);
            $name = $f['name'];
            $username = $f['id'];
            $query = "INSERT INTO feed (date_updated,feed_type,name,username,image,id) VALUES (NOW(),'event','$name','$id','$photo',$event_id)";
            $sql = mysqli_query($conn,$query);
            echo "<script>alert('ተሳክቷል::');
            location.href='my-events';
            </script>";
        }
    }
?>
    <hr>
    <hr>

<footer class="mt-2">
    <p class="text-center" style="font-size:12px;">
    <span style="color:white; background-color:#00bfff; padding:5px; border-radius:10px;"><strong>Yetale.et</strong></span> 
    የንግድ ባለቤቶች በቀላሉ ምርትና አገልግሎታቸውን ከሚፈልጉ ደንበኞቻቸው ጋር በቀላሉ መገናኘት 
     እንዲችሉ እንዲሁም ደንበኞች የሚፈልጉትን በቀላሉ እንዲያገኙ የሚረዳ ድህረ-ገፅ ነዉ:: <br>
   
</p>
<p class="text-center" style="font-size:12px;">
ሊያገኙን ይፈልጋሉ?<br>
<a href="tel:+251 91 888 8225" style="text-decoration:none; font-size:12px; padding:5px; border-right:2px solid #00bfff; color:black;"><img class="myname" src="../../icons/phone.png" width="20" height="20"> +251 91 888 8225</a>
<a href="tel:+251 91 888 8233" style="font-size:12px; padding:5px; border-right:2px solid #00bfff; color:black;"><img class="myname" src="../../icons/phone.png" width="20" height="20"> +251 91 888 8233</a>

<a href="mailto:contact@yetale.com" style="font-size:12px; padding:3px; 
color:black;"><img class="myname" src="../../icons/envelope-square-256.png" width="20" height="20"> contact@yetale.com</a>
<br>

 <span style="color:#00bfff; font-size:12px;">&copy; 20<?php echo date('y');?> የታለ</span><br> <br>
</p>
</footer>


</body>
</html>

