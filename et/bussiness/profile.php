
<?php
    session_start();
   
  if(!isset($_SESSION['email']) || $_SESSION['type']=='user'){
      ?><script>
      alert('You must login first.');
      location.href="../user/login";
      </script><?php
      $email = $_SESSION['email'];
  }
   
    require("../db/connection.php");
    $email = $_SESSION['email'];
    $type = $_SESSION['type'];


    $query = "SELECT * FROM bussiness WHERE email='$email'";
    $sql = mysqli_query($conn,$query);
    $fetch = mysqli_fetch_array($sql);
    $name = $fetch['name'];
     $photo = $fetch['photo'];
    $lvl = $fetch['acc_type'];
    $view = $fetch['views'];
    $id = $fetch['id'];
    $logo = $fetch['logo'];
    $desc= $fetch['description'];
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $name?> | Yetale</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/mystyle.css">
    <style>
        .social{
             transition:2s;
        }
       .social:hover{
           opacity:0.5;
          
       }
       option{
           font-size:12px;
           color:black;
       }
    </style>
   
         
    
</head>

<body>
    <?php
    
    ?>
    <!-- Profile Image -->

     <div class="topnav" id="myTopnav" style="position:fixed; background-color:#00bfff; width:100%; z-index:100;">
        <a href="../HOME" class="mr-5"><img src="../../icons/yet.png" width="60" height="60"></a>
         <?php
        if(isset($_SESSION['email']) && $_SESSION['type']=='bussiness'){
            ?>
                <a href="#" class="text-center" style="color:black; font-weight:600; text-decoration:none; margin:0 auto;">
         <img src="../../images/<?php echo $photo?>" width="100" height="100" style="border-radius:100%;"><br>  
       <span style="text-transform:uppercase; font-size:27px; color:white;"><?php echo $name?></span>
       
   
     <a href="log-out.php" class="text-center" style="color:white; font-weight:600; text-decoration:none; margin-top:-35px;">
           
        Log Out</a>
    </a>
            <?php
        }
        ?>
        <a href="" class="" style="font-weight:600; color:white; border-bottom:5px solid white; text-decoration:none; margin-top:-20px;">
        <!-- <img src="icons/blog 1.png" width="18" height="18">  -->
        My Profile</a>
        <a href="my-badge" class="" style="font-weight:600; color:black; text-decoration:none; ">
        <!-- <img src="icons/destination.png" width="18" height="18">  -->
        Get Badge</a>
        <a href="my-reviews" class="" style="color:black; font-weight:600; text-decoration:none;">
         <!-- <img src="icons/food.png" width="18" height="18">  -->
        My Reviews</a>
        <a href="../user/profile?id=<?php echo $id?>" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        View-As</a>
         <a href="my-events" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        Post an Event</a>
         <a href="my-vacancy" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        Post a Job</a>
        <?php
        if(isset($_SESSION['email']) && $_SESSION['type']=='bussiness'){
            ?>
               
            <?php
        }
        ?>
        
        
       
      
        
    
       <a href="javascript:void(0);" class="icon mr-3 mt-3" style="position:fixed; right:0; top:0;" onclick="myFunction()"> 
           <img src="../../icons/2747254.png" width="40" height="40" style="filter:invert(1);" class="ham"> 
            </a>
            <br>
             </div>
                
   
    
   
    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
   
    <!--end of nav-->
    <br>
    <br>
    <br>
    

    <?php if($lvl=='none'){
        ?>
    <div class="alert alert-info mt-4" style="font-size:12px;">
    Try upgrading your account to blue or green badge. It enables customers searching for your products or services find you easily.<br>
    <a href="my-badge"><u style="font-size:12px;">learn more about blue & green badges here</u></a>

    </div>
    
        <?php
    }

    if($lvl=='green'){
        ?>
    <div class="float-right mr-3 mt-5" style="font-size:11px;">የእርስዎ ባጅ: <img src="../../icons/green.png" width="18" height="18">
        <br>የሚያበቃው: <?php echo $fetch['expiry']?>
</div>
        <?php
    }

    if($lvl=='blue'){
        ?>
    <div class="float-right mr-3" style="font-size:11px;">የእርስዎ ባጅ: <img src="../../icons/blue.png" width="18" height="18">
        <br>የሚያበቃው: <?php echo $fetch['expiry']?>
</div>
        <?php
    }?>
    
    
    

<p class="text-center mt-5">
   
    <span style="font-weight:700; font-size:2em;" class="margin-left:-70px;"></span><br><br>
    <span style="font-weight:700; font-size:1.5em;" class="margin-left:-70px;"><?php if($name!='') echo $name; if($name=='') echo 'Your bussiness name';?></span>
    <?php
    if($lvl=='blue'){
        echo "<img src='../../icons/blue.png' width='25' height='25' style='margin-top:-10px;'>";
    }
     if($lvl=='green'){
        echo "<img src='../../icons/green.png' width='25' height='25' style='margin-top:-10px;'>";
    }
    ?>
    <br>
    <span style="font-weight:700; font-size:1.2em; color:#00bfff;">
    @<?php if($fetch['id']!='') echo $fetch['id'];
    if($fetch['id']=='') echo 'your_username';
    ?>
    </span><br>
    <?php
         $query_avg = "SELECT avg(star) as average FROM rating WHERE bussiness='$id'";
                $sql_avg = mysqli_query($conn,$query_avg);
                $fetch_avg = mysqli_fetch_array($sql_avg);
                $avrg = $fetch_avg['average'];
                $rate = round($avrg,1);

                if($rate=='0'){
    ?><img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='1'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'1' && $rate<2){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='2'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'2' && $rate<3){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='3'){
    ?><img src="../../icons/full-star.png" width="18" height="18">
    <img src="../../icons/full-star.png" width="18" height="18">
    <img src="../../icons/full-star.png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'3' && $rate<4){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='4'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'4' & $rate<5){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <?php 
}
if($rate=='5'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <?php 
}



    ?><br>
    <img src="../../icons/eye-128.png"   width="14" height="14"> <span style="font-size:12px;"><?php echo $view?></span><br>

    <br><br>
     <img src="../../images/<?php echo $logo?>" width="150" height="150" style="border-radius:10px;" class="" alt="your logo"><br>
     <?php
     if($lvl=='green'){
         ?>
         <hr>
         <p class="text-center share" style="box-shadow:0px 4px 6px rgba(0,0,0,0.3); padding:10px;">
            <strong>ገፅዎን ያጋሩ</strong><br>
            <a href="https://www.facebook.com/sharer.php?u=https://www.yetale.et/user/profile?id=<?php echo $id?>" class="social">
            <img src="../../icons/socials/facebook.png" width="28" height="28"></a>&nbsp;&nbsp;
            <a href="https://twiter.com/share?url=https://www.yetale.et/user/profile?id=<?php echo $id?> & text=<?php echo $name?> on Yetale" class="social">
                <img src="../../icons/socials/twitter.png" width="28" height="28"></a>&nbsp;&nbsp;
            <a href="https://api.whatsapp.com/send?text=<?php echo $name?> on Yetale https://www.yetale.et/user/profile?id=<?php echo $id?>" class="social">
                <img src="../../icons/socials/whatsapp.png" width="30" height="30"></a>&nbsp;&nbsp;
            <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://www.yetale.et/user/profile?id=<?php echo $id?>" class="social">
                <img src="../../icons/socials/linkedin.png" width="28" height="28" style="border-radius:100%;"></a>&nbsp;&nbsp;
            <a href="https://www.pinterest.com/pin/create/button?url=https://www.yetale.et/user/profile?id=<?php echo $id?>&media=https://www.yetale.com/images/<?php echo $photo?>" class="social">
                <img src="../../icons/socials/pinterest.png" width="28" height="28"></a>
         </p>
         
         <?php
     }
     ?>
  <p class="text-center recentlyopened">ገፅዎን ያዘምኑ</p>
    <!-- update logo -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>አርማ</span>  
   <script>
       function one(){
           document.getElementById('one').style.display='block';
           document.getElementById('down1').style.display='none';
           document.getElementById('up1').style.display='block';

       }
       function one1(){
           document.getElementById('one').style.display='none';
           document.getElementById('up1').style.display='none';
           document.getElementById('down1').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down1" onclick="one()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up1" onclick="one1()"><</span> 
   <div id="one" style="display:none; transition:2s;"><input type="file" name="logo" accept="image/*" class="form-control" rquired mutiple>
   <input type="submit" value="ቀይር" name="update-logo" class="btn form-control">
   </div>
   </div>
    </form>
    <?php 
    if(isset($_POST['update-logo'])){
        $logo =str_shuffle('giugsdiu').$_FILES['logo']['name'];
            
             $Temp =  $_FILES['logo']['tmp_name'];
             $MyLocation= "../../images/".$logo ;
             move_uploaded_file($Temp,$MyLocation);
             $query = "UPDATE bussiness SET logo='$logo' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ተሳክቷል::'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of logo form -->
    <!-- update cover -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>የሽፋን ፎቶ</span> 
   <script>
       function two(){
           document.getElementById('two').style.display='block';
           document.getElementById('down2').style.display='none';
           document.getElementById('up2').style.display='block';

       }
       function two1(){
           document.getElementById('two').style.display='none';
           document.getElementById('up2').style.display='none';
           document.getElementById('down2').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down2" onclick="two()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up2" onclick="two1()"><</span> 
      <div id="two" style="display:none;">
   <input type="file" name="cover" accept="image/*" class="form-control" rquired mutiple>
   <input type="submit" value="ቀይር" name="update-cover" class="btn form-control">
    </div>
   </div>
    </form>
    <?php 
    if(isset($_POST['update-cover'])){
        $cover =str_shuffle('giugsdiu').$_FILES['cover']['name'];
            
             $Temp =  $_FILES['cover']['tmp_name'];
             $MyLocation= "../../images/".$cover ;
             move_uploaded_file($Temp,$MyLocation);
             $query = "UPDATE bussiness SET photo='$cover' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ተሳክቷል::'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of cover form -->

     <!-- update name -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container">
   <span>የንግድ ስም</span>
   <script>
       function three(){
           document.getElementById('three').style.display='block';
           document.getElementById('down3').style.display='none';
           document.getElementById('up3').style.display='block';
           document.getElementById('bizname').focus();


       }
       function three1(){
           document.getElementById('three').style.display='none';
           document.getElementById('up3').style.display='none';
           document.getElementById('down3').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down3" onclick="three()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up3" onclick="three1()"><</span> 
      <div id="three" style="display:none;">     
   <input type="text" id="bizname" name="name" pattern="^[a-zA-Z\s]*" value="<?php echo $fetch['name']?>" maxlength="25" class="form-control" rquired>
   <input type="submit" value="ቀይር" name="update-name" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-name'])){
             $name= $_POST['name'];
             $new_name = str_replace(' ','_',$name);  
              $query_num = "SELECT count(email) AS num FROM bussiness WHERE name='$name'";
             $sql_num = mysqli_query($conn,$query_num);
             $fetch_num = mysqli_fetch_array($sql_num);
             $num = $fetch_num['num']+1;
             $id = $new_name.'_'.$num; 

            $query = "UPDATE bussiness SET name='$name',id='$id' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ተሳክቷል::'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of update name -->

    <!-- update category -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container">
   <span>ዘርፍ</span> 
   <script>
       function four(){
           document.getElementById('four').style.display='block';
           document.getElementById('down4').style.display='none';
           document.getElementById('up4').style.display='block';

       }
       function four1(){
           document.getElementById('four').style.display='none';
           document.getElementById('up4').style.display='none';
           document.getElementById('down4').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down4" onclick="four()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up4" onclick="four1()"><</span> 
      <div id="four" style="display:none;">    
   <select name="category" class="form-control" style="background-color:#e6e6ff;">
       <optgroup label="Food and Entertainment" style="background-color:#00bfff; color:white;">
            
            <option value="Bakery and Cake">Bakery and Cake</option>
            <option value="Bar and Lounge">Bar and Lounge</option>
            <option value="Burger and Pizza">Burger and Pizza</option>
           <option value="Caffe and Restaurant" selected>Caffe and Restaurant</option>
           <option value="Cinema">Cinema</option>
           <option value="Coffee Shop">Coffee Shop</option>
           <option value="Foreign Restaurant">Foreign Restaurant</option>
           <option value="Guest House">Guest House</option>
            <option value="Hotel">Hotel</option>
            <option value="Muslim Restaurant">Muslim Restaurant</option>
            <option value="Night Club">Night Club</option>
            <option value="Resort and Lodge">Resort and Lodge</option>
           <option value="Traditional Restaurant">Traditional Restaurant</option>
           
          
           
           
           
       </optgroup>
       <optgroup label="Beauty, Health and Fitness" style="background-color:#00bfff; color:white;">
       
            <option value="Barber Shop">Barber Shop</option>
             <option value="Clinic">Clinic</option>
             <option value="Dental Clinic">Dental Clinic</option>
             <option value="Eye Clinic">Eye Clinic</option>
            <option value="Fitness Coach">Fitness Coach</option>
           <option value="Gym">Gym</option>
           <option value="Hair Salon">Hair Salon</option>
           <option value="Hospital">Hospital</option>
            <option value="Massage and Spa">Massage and Spa</option>
           <option value="Men Fashion">Men's Fashion</option>
           <option value="Nail Salon">Nail Salon</option>
           <option value="Pharmacy">Pharmacy</option>
           <option value="Sports Wear Shop">Sports Wear Shop</option>
            <option value="Women Fashion">Women's Fashion</option>
       </optgroup>
       <optgroup label="Services and Professionals" style="background-color:#00bfff; color:white;">
            <option value="Baby Care">Baby Care</option>
            <option value="Building Finishing Worker">Building Finishing Worker</option>
            <option value="Cell Phone Repair">Cell Phone Repair</option>
            <option value="Cleaner">Cleaner (Building & Cars)</option>
           <option value="Electronics Repair">Electronics Repair</option>
           <option value="Flatter">Flatter</option>
           <option value="Garage">Garage</option>
           <option value="Job Agency">Job Agency</option>
           <option value="Lawyer">Lawyer</option>
           <option value="Plumber">Plumber</option>
           <option value="Printing Press">Printing Press</option>
           <option value="Psychologist">Psychologist</option>
            <option value="Psychiatrist">Psychiatrist</option>
            <option value="Tour and Travel">Tour and Travel</option>
</optgroup>
      
<optgroup label="Education & Training" style="background-color:#00bfff; color:white;">
            <option value="Beauty and Fashion School">Beauty and Fashion School</option>
            <option value="College and University">College and University</option>
             <option value="Computer Training">Computer Training</option>
             <option value="Distance Education">Distance Education</option>
             <option value="Driving License Trainer">Driving License Trainer</option>
           <option value="Music School">Music School</option>
             <option value="School">School</option>
           
       </optgroup>
       <optgroup label="Shopping" style="background-color:#00bfff; color:white;">
        <option value="Book Store">Book Store</option>
        <option value="Boutique">Boutique</option>
        <option value="Car Sales/Rent">Car Sales/Rent</option>
        <option value="Electronics Shop">Electronics Shop</option>
        <option value="Furniture">Furniture</option>
         <option value="Real Estate">Real Estate</option>
        <option value="Sell and Buy">Sell and Buy</option>
           <option value="Shopping Center">Shopping Center</option>
           <option value="Supermarket">Supermarket</option>
           <option value="Wholesale">Wholesale</option>
           
       </optgroup>
       <optgroup label="Arts" style="background-color:#00bfff; color:white;">
            <option value="DJ">DJ</option>
             <option value="Event Organizer">Event Organizer</option>
             <option value="Film/Video Producer">Film/Video Producer</option>
             <option value="Interior Designer">Interior Designer</option>
            <option value="Photographer">Photographer</option>
            <option value="Tatoo Artist">Tatoo Artist</option>
            
       </optgroup>
       <optgroup label="Tech" style="background-color:#00bfff; color:white;">
            <option value="E-Payment">E-Payment</option>
            <option value="E-Commerce">E-Commerce</option>
            <option value="E-Money Agent">E-Money Agent</option>
            <option value="Software Developer">Software Developer</option>
            <option value="Sports Betting">Sports Betting</option>
            <option value="Virtual Reality Center">Virtual Reality Center</option>
            <option value="Website/App">Website/App</option>
            
       </optgroup>
    </select>
   <input type="submit" value="ቀይር" name="update-category" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-category'])){
             $category= $_POST['category'];
             

            $query = "UPDATE bussiness SET category='$category' WHERE email='$email'";
              $sql = mysqli_query($conn,$query);
              if($sql){
                echo "<script>alert('ተሳክቷል::'); location.href='my-profile';</script>";
              }
    }
    ?>
    <hr>
    <!-- end of update name -->

      <!-- update desc -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>ገለፃ</span> 
    <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down5" onclick="five()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up5" onclick="five1()"><</span> 
      <div id="five" style="display:none;">  
   <br><span style="font-size:10px;">Describe your bussiness in less than 500 words.<br>Customers will see it on your profile.</span>
   <script>
       function five(){
           document.getElementById('five').style.display='block';
           document.getElementById('down5').style.display='none';
           document.getElementById('up5').style.display='block';
            document.getElementById('bizdesc').focus();

       }
       function five1(){
           document.getElementById('five').style.display='none';
           document.getElementById('up5').style.display='none';
           document.getElementById('down5').style.display='block';
           
       }
   </script> 
    
   <textarea name="desc" maxlength="200" id="bizdesc" class="form-control" rquired></textarea>
   <input type="submit" value="ቀይር" name="update-desc" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-desc'])){
             $desc= mysql_real_escape_string($_POST['desc']);
             
            $query = "UPDATE bussiness SET description ='$desc' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ተሳክቷል::'); location.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of update address -->

       <!-- update city -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container">
   <span>ከተማ </span>
   <script>
       function six(){
           document.getElementById('six').style.display='block';
           document.getElementById('down6').style.display='none';
           document.getElementById('up6').style.display='block';

       }
       function six1(){
           document.getElementById('six').style.display='none';
           document.getElementById('up6').style.display='none';
           document.getElementById('down6').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down6" onclick="six()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up6" onclick="six1()"><</span> 
      <div id="six" style="display:none;">     
   <span>current city: <?php echo $fetch['city']?></span><br>
   <span style="color:black; font-size:10px;">(If your city is not in the list, select your nearest city.)</span>
   <select name="city" class="form-control" style="background-color:#e6e6ff;">
       
          <optgroup label="A" style="background-color:#00bfff; color:white;">
           <option>Adama/Nazareth</option>
           <option selected>Addis Ababa</option>
           <option>Addis Alem/Ejersa</option>
           <option>Addis Zemen</option>
           <option>Adigrat</option>
           <option>Adwa</option>
           <option>Agaro</option>
           <option>Alaba Kolito</option>
           <option>Alamata</option>
           <option>Aleta Wondo</option>
           <option>Ambo</option>
           <option>Angecha</option>
           <option>Ankober</option>
           <option>Arba Minch</option>
           <option>Asaita</option>
           <option>Asella</option>
           <option>Assosa</option>
           <option>Axum</option>
       </optgroup>

       <optgroup label="B" style="background-color:#00bfff; color:white;">
        <option>Babille</option>
        <option>Baco</option>
        <option>Bahir Dar</option>
        <option>Bale Goba</option>
        <option>Bale Robe</option>
        <option>Bati</option>
        <option>Batu/Ziway</option>
        <option>Bedele</option>
        <option>Bekoji</option>
        <option>Bichena</option>
        <option>Bishoftu/Debre Zeit</option>
        <option>Bonga</option>
        <option>Burie Damot</option>
        <option>Butajira</option>
      </optgroup>

      <optgroup label="C" style="background-color:#00bfff; color:white;">
      <option>Chiro/Asebe Teferi</option>
      <option>Chelenko</option>
      <option>Chencha</option>
      <option>Chuahit</option>
      </optgroup>

      <optgroup label="D" style="background-color:#00bfff; color:white;">
      <option>Dabat</option>
      <option>Dangilla</option>
      <option>Debark</option>
      <option>Debre Birhan</option>
      <option>Debre Markos</option>
      <option>Debre Tabor</option>
      <option>Debre Worq</option>
      <option>Dejen</option>
      <option>Dembi Dolo</option>
      <option>Dessie</option>
      <option>Dilla</option>
      <option>Dire Dawa</option>
      <option>Dodola</option>
      <option>Durame</option>
      <option>Dukem</option>

      </optgroup>

      <optgroup label="F" style="background-color:#00bfff; color:white;">
      <option>Fiche</option>
      <option>Finote Selam</option>
      <option>Fireweyni</option>
      </optgroup>

      <optgroup label="G" style="background-color:#00bfff; color:white;">
      <option>Gambella</option>
      <option>Gelemso</option>
      <option>Ghimbi</option>
     
      <option>Gode</option>
      <option>Gondar</option>
      <option>Gorgora</option>
      </optgroup>

      <optgroup label="H" style="background-color:#00bfff; color:white;">
      <option>Harar</option>
      <option>Hawassa</option>
      <option>Hayq</option>
      <option>Holleta</option>
      <option>Hosaena</option>
      <option>Humera</option>
      <option>Huruta</option>
      <option>Hadero</option>
      </optgroup>

      <optgroup label="J" style="background-color:#00bfff; color:white;">
      <option>Jigjiga</option>
      <option>Jimma</option>
      <option>Jinka</option>
      </optgroup>

      <optgroup label="K" style="background-color:#00bfff; color:white;">
      <option>Kebri Dehar</option>
      <option>Kibre Mengist</option>
      <option>Kobo</option>
      <option>Kombolcha</option>
      <option>Konso</option>
      <option>Kulubi</option>
      </optgroup>

      <optgroup label="L" style="background-color:#00bfff; color:white;">
      <option>Lalibela</option>
      </optgroup>

      <optgroup label="M" style="background-color:#00bfff; color:white;">
      <option>Maychew</option>
      <option>Mekele</option>
      <option>Mersa</option>
      <option>Metemma</option>
      <option>Metu</option>
      <option>Mille</option>
      <option>Mizan Teferi</option>
      <option>Mojo</option>
      <option>Mota</option>
      <option>Moyale</option>
      </optgroup>

      <optgroup label="N" style="background-color:#00bfff; color:white;">
      <option>Negash</option>
      <option>Negele Borena</option>
      <option>Nejo</option>
      <option>Nekemte</option>

      <optgroup label="S" style="background-color:#00bfff; color:white;">
      <option>Sawla</option>
      <option>Semera</option>
      <option>Shashemene</option>
      <option>Shambu</option>
      <option>Shiraro</option>
      <option>Shire</option>
      <option>Sodo</option>
      <option>Sekota</option>
      </optgroup>

      <optgroup label="T" style="background-color:#00bfff; color:white;">
      <option>Teppi</option>
      <option>Tulu Bolo</option>
      </optgroup>

      <optgroup label="W" style="background-color:#00bfff; color:white;">
      <option>Weliso</option>
      <option>Welkite</option>
      <option>Welwel</option>
      <option>Werabe</option>
      <option>Werder</option>
      <option>Wereta</option>
      <option>Woldia</option>
      <option>Wuchale</option>
      <option>Wukro</option>
      <option>Washera</option>
      

      </optgroup>

      <optgroup label="Y" style="background-color:#00bfff; color:white;">
      <option>Yabelo</option>
      <option>Yeha</option>
      <option>Yirga Alem</option>
      </optgroup>
          </select>
   <input type="submit" value="ቀይር" name="update-city" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-city'])){
             $city= $_POST['city'];
            

            $query = "UPDATE bussiness SET city='$city' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ተሳክቷል::'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of update city -->
     <!-- update address -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>አድራሻ</span> 
   <script>
       function seven(){
           document.getElementById('seven').style.display='block';
           document.getElementById('down7').style.display='none';
           document.getElementById('up7').style.display='block';
            document.getElementById('bizaddress').focus();

       }
       function seven1(){
           document.getElementById('seven').style.display='none';
           document.getElementById('up7').style.display='none';
           document.getElementById('down7').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down7" onclick="seven()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up7" onclick="seven1()"><</span> 
      <div id="seven" style="display:none;">    
   <input type="text" name="address" id="bizaddress" value="<?php echo $fetch['address']?>"  maxlength="100" class="form-control" rquired>
   <input type="submit" value="ቀይር" name="update-address" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-address'])){
             $address= mysql_real_escape_string($_POST['address']);
             
            $query = "UPDATE bussiness SET address='$address' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ተሳክቷል::'); location.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of update address -->

       <!-- update phone -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>ሞባይል ስልክ</span></span>
   <script>
       function eight(){
           document.getElementById('eight').style.display='block';
           document.getElementById('down8').style.display='none';
           document.getElementById('up8').style.display='block';
            document.getElementById('bizphone').focus();

       }
       function eight1(){
           document.getElementById('eight').style.display='none';
           document.getElementById('up8').style.display='none';
           document.getElementById('down8').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down8" onclick="eight()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up8" onclick="eight1()"><</span> 
      <div id="eight" style="display:none;">
        
   <input type="text" name="phone" id="bizphone" value="<?php echo $fetch['phone']?>" pattern="[0][9][0-9]{8}" maxlength="10" class="form-control" rquired>
   <span style="font-size:10px; color:red" id="error-phone"></span>
   <input type="submit" value="ቀይር" name="update-phone" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-phone'])){
             $phone= $_POST['phone'];
             $query_phone="SELECT count(email) AS num FROM bussiness WHERE phone=$phone";
             $sql_phone = mysqli_query($conn,$query_phone);
             $fetch_phone = mysqli_fetch_array($sql_phone);
             $phones = $fetch_phone['num'];

             
             
             if($phones>0){
                 ?><script>
                 document.getElementById("error-phone").innerHTML="Phone already in use. Try another.";
                 document.getElementById("bizphone").style.border="1px red solid";
                 document.getElementById("bizphone").focus();</script><?php

             }
             if($phones<1){
                 $query = "UPDATE bussiness SET phone='$phone' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ተሳክቷል::'); locaton.href='my-profile';</script>";
             }
             }

           
    }
    ?>
    <hr>
    <!-- end of update phone -->

     <!-- update office -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>የቢር ስልክ</span>
   <script>
        function thirteen(){
           document.getElementById('thirteen').style.display='block';
           document.getElementById('down13').style.display='none';
           document.getElementById('up13').style.display='block';
            document.getElementById('bizoffice').focus();

       }
       function thirteen1(){
           document.getElementById('thirteen').style.display='none';
           document.getElementById('up13').style.display='none';
           document.getElementById('down13').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down13" onclick="thirteen()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up13" onclick="thirteen1()"><</span> 
      <div id="thirteen" style="display:none;">
        
   <input type="text" name="office" id="bizoffice" value="<?php echo $fetch['office_phone']?>" pattern="[0][0-9]{9}" maxlength="10" class="form-control" rquired>
   <span style="font-size:10px; color:red" id="error-office"></span>
   <input type="submit" value="ቀይር" name="update-office" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-office'])){
             $phone= $_POST['office'];
             $query_phone="SELECT count(email) AS num FROM bussiness WHERE office_phone=$phone";
             $sql_phone = mysqli_query($conn,$query_phone);
             $fetch_phone = mysqli_fetch_array($sql_phone);
             $phones = $fetch_phone['num'];

             
             
             if($phones>0){
                 ?><script>
                 document.getElementById("error-office").innerHTML="Phone already in use. Try another.";
                 document.getElementById("bizoffice").style.border="1px red solid";
                 document.getElementById("bizoffice").focus();</script><?php

             }
             if($phones<1){
                 $query = "UPDATE bussiness SET office_phone='$phone' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ተሳክቷል::'); locaton.href='my-profile';</script>";
             }
             }

           
    }
    ?>
    <hr>
    <!-- end of update phone -->

     <!-- update website -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>ድህረ-ገፅ</span>
   <script>
       function nine(){
           document.getElementById('nine').style.display='block';
           document.getElementById('down9').style.display='none';
           document.getElementById('up9').style.display='block';
            document.getElementById('bizsite').focus();

       }
       function nine1(){
           document.getElementById('nine').style.display='none';
           document.getElementById('up9').style.display='none';
           document.getElementById('down9').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down9" onclick="nine()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up9" onclick="nine1()"><</span> 
    <span style="font-size:11px;">(ምሳሌ: www.yoursite.com)</span>
      <div id="nine" style="display:none;">     
   <input type="text" name="website" id="bizsite" value="<?php echo $fetch['website']?>" maxlength="10" class="form-control" rquired>
   <input type="submit" value="ቀይር" name="update-website" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-website'])){
             $website= $_POST['website'];
             
            $query = "UPDATE bussiness SET website='$website' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ተሳክቷል::'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of update website -->

     <!-- update hours -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>የአገልግሎት ሰዓት</span> 
   <script>
       function ten(){
           document.getElementById('ten').style.display='block';
           document.getElementById('down10').style.display='none';
           document.getElementById('up10').style.display='block';

       }
       function ten1(){
           document.getElementById('ten').style.display='none';
           document.getElementById('up10').style.display='none';
           document.getElementById('down10').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down10" onclick="ten()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up10" onclick="ten1()"><</span> 
      <div id="ten" style="display:none;">
   <br>    
   <span>ከ:</span><input type="time" name="from" value="<?php echo $fetch['open_from']?>" class="form-control" rquired>
   <span>እስከ:</span><input type="time" name="to" value="<?php echo $fetch['open_to']?>" class="form-control" rquired>
   <input type="submit" value="ቀይር" name="update-serv" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-serv'])){
             $from= $_POST['from'];
              $to= $_POST['to'];
             
             
            $query = "UPDATE bussiness SET open_from='$from',open_to='$to' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ተሳክቷል::'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of update hours -->

      <!-- update founded -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>የተመሠረተበት ቀን</span>
   <script>
       function eleven(){
           document.getElementById('eleven').style.display='block';
           document.getElementById('down11').style.display='none';
           document.getElementById('up11').style.display='block';

       }
       function eleven1(){
           document.getElementById('eleven').style.display='none';
           document.getElementById('up11').style.display='none';
           document.getElementById('down11').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down11" onclick="eleven()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up11" onclick="eleven1()"><</span> 
      <div id="eleven" style="display:none;">
   <br>  
   <?php $today = date('20y-m-d');?>  
  <input type="date" name="founded" max="<?php echo $today?>" value="<?php echo $fetch['date_founded']?>" class="form-control" rquired>
   
   <input type="submit" value="ቀይር" name="update-found" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-found'])){
             $found= $_POST['founded'];
             
             
            $query = "UPDATE bussiness SET date_founded='$found' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ተሳክቷል::'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of update founded -->

     <!-- update emp -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>የሰራተኞች ብዛት</span>
   <script>
       function twelve(){
           document.getElementById('twelve').style.display='block';
           document.getElementById('down12').style.display='none';
           document.getElementById('up12').style.display='block';
            document.getElementById('bizemp').focus();

       }
       function twelve1(){
           document.getElementById('twelve').style.display='none';
           document.getElementById('up12').style.display='none';
           document.getElementById('down12').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down12" onclick="twelve()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up12" onclick="twelve1()"><</span> 
      <div id="twelve" style="display:none;">
   <br>  
   
  <input type="number" id="bizemp" name="emp" min="1" class="form-control" rquired>
   
   <input type="submit" value="ቀይር" name="update-emp" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-emp'])){
             $emp= $_POST['emp'];
             
             
            $query = "UPDATE bussiness SET emp_size='$emp' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ተሳክቷል::'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
     <p class="text-center recentlyopened">ፎቶዎች</p>
    <!-- end of update emp -->

     <!-- update add new photo -->
     <?php
     $query = "SELECT photo,id FROM pics WHERE email='$email'";
     $sql = mysqli_query($conn,$query);
     if(mysqli_num_rows($sql)>0){
         ?><div class="recent">
             <?php
             while($fetch = mysqli_fetch_array($sql)){
                $id = $fetch['id'];
                $pic = $fetch['photo'];
                ?>
                <div class="car" style="z-index:-100;">
                    <img src="../../images/<?php echo $pic?>" width="150" height="150" style="border-radius:10px"><br>
                    <a href="my-profile?del-pic=<?php echo $id?>" class="btn text-white mt-1"><img src="../../icons/Tg/PicsArt_02-18-11.51.00.png" width="25" height="25"> አጥፋ</a>
                </div>
                <?php
             }?>
         </div><hr><?php
     }
     if(isset($_GET['del-pic'])){
         $pic = $_GET['del-pic'];
         ?>
            
         <?php
         $ftf = "SELECT photo FROM pics WHERE id=$pic";
         $ftfs = mysqli_query($conn,$ftf);
         $ftff = mysqli_fetch_array($ftfs);
         $photo = $ftff['photo'];
          rename('../../images/'.$photo,'../../del/'.$photo);
         $query = "DELETE FROM pics WHERE id=$pic";
         $sql = mysqli_query($conn,$query);
        
        
         ?>
        <script>location.href='my-profile';</script>
         <?php
     }
     ?>
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <img src="../../icons/camera.png" width="30" height="30"> አዲስ ፎቶ ለጥፍ<br> 
   <label for="add-image" class="form-control" style="background-color:#e6e6ff; border-bottom:2px solid #00bfff;"><img src="../../icons/Tg/PicsArt_02-18-11.30.51.png" style="filter:invert(1);" width="22" height="22"> ፎቶ ምረጥ</label>
  <input type="file" id="add-image" name="add-photo" accept="image/*" class="form-control" rquired hidden>
   
   <input type="submit" value="ለጥፍ" name="Post" class="btn form-control">
   </div>
    </form>
    <?php 
    if(isset($_POST['Post'])){
              $photo = str_shuffle('gdigidsg').$_FILES['add-photo']['name'];
             //echo $FileName;
             $Temp =  $_FILES['add-photo']['tmp_name'];
             $MyLocation= "../../images/".$photo ;
             move_uploaded_file($Temp,$MyLocation);
             
             $query_num = "SELECT count(id) AS num FROM pics WHERE email='$email'";
             $sql_num = mysqli_query($conn,$query_num);
             $fetch_num = mysqli_fetch_array($sql_num);
             $num = $fetch_num['num'];

              $qi = "SELECT COUNT(id) AS num FROM pics";
            $si = mysqli_query($conn,$qi);
            $fi = mysqli_fetch_array($si);
            $event_id = $fi['num']+1;

             if($lvl=='none' && $num>=5){
                echo "<script>alert('Get a badge to post more than 5 photos.'); locaton.href='my-profile';</script>";
                
             }

             
             if($lvl=='none' && $num<5){
                $query = "INSERT INTO pics VALUES ('$email','$photo',$event_id)";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ቀይርተሳክቷል::'); location.href='my-profile'; </script>";
             }
            }
             if($lvl=='blue' && $nu>=30){
                echo "<script>alert('Delete atleast one photo to post new.'); locaton.href='my-profile';</script>";
             }
             if($lvl=='blue' && $num<30){
                  $query = "INSERT INTO pics VALUES ('$email','$photo',$event_id)";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ተሳክቷል::'); location.href='my-profile'; </script>";
             }
             }
             if($lvl=='green' && $num>=30){
                echo "<script>alert('Delete atleast one photo to post new.'); locaton.href='my-profile';</script>";
             }
             if($lvl=='green' && $num<30){
                  $query = "INSERT INTO pics VALUES ('$email','$photo',$event_id)";
             $sql = mysqli_query($conn,$query);
             $query = "INSERT INTO feed (date_updated,feed_type,name,username,image,id) VALUES (NOW(),'photo','$email','$email','$photo',$event_id)";
            $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('ተሳክቷል::'); location.href='my-profile'; </script>";
             }
             }
             
           
    }
    ?>
    <hr>
    <!-- end of add new photo -->


    
    
    

  
 <p class="text-center recentlyopened"><img src="../../icons/destination.png" width="30" height="30"> የካርታ አድራሻ</p>
    
    <form method="post" class="container" enctype="">
        
     <p class="text-center" style="font-size:13px;">
        አድራሻዎን አሁን ወዳሉበት ቦታ መቀየር ከፈለጉ ከስር ቀይር የሚለውን ይጫኑ::

     </p>
   
    <hr>
    <p class="text-center">
    <?php
        if($lvl!='none'){
            ?>
            <button onClick="getLocation()" class="btn form-control">ቀይር</button>
            <?php
        }
         if($lvl=='none'){
            ?>
            <button class="form-control" style="background-color:#00bfff; color:white;" disabled>ቀይር</button>
            <div class="alert alert-info" style="margin-top:-15px; font-size:11px;">Update your account to blue or green badge to use this feature.</div>
            <?php
        }
    ?>
    
        <span style="color:red; font-size:10px;" id="error-loc"></span><br>
        <script>
            function getLocation(){
              if(navigator.geolocation){
                   navigator.geolocation.getCurrentPosition(savePosition);
               }
               else{
                    document.getElementById("error-loc").innerHTML="Failed to get location.";
               }
               function savePosition(position){
                   document.cookie='lat='+position.coords.latitude+'';
                   document.cookie='lng='+position.coords.longitude+'';
                   var email = <?php echo $email?>;
                   document.cookie='email='+email'';
                   

               }
            
              
            }
            if(!getLocation()){
                document.getElementById("error-loc").innerHTML="Failed to get location. Either you denied location request or your device/browser doesn't support geolocation.";
            }
             if(!saveposition()){
                   document.getElementById("error-loc").innerHTML="Failed to get location.";
               }
               if(savePosition()){
                  alert('Location successfully updated.');
               }
        </script>
    </p>
  
</form>
<?php

?>


</div>


</div>
<hr><hr>
<footer class="mt-2">
    <p class="text-center" style="font-size:12px;">
    <span style="color:white; background-color:#00bfff; padding:5px; border-radius:10px;"><strong>Yetale.et</strong></span> 
    የንግድ ባለቤቶች በቀላሉ ምርትና አገልግሎታቸውን ከሚፈልጉ ደንበኞቻቸው ጋር በቀላሉ መገናኘት 
     እንዲችሉ እንዲሁም ደንበኞች የሚፈልጉትን በቀላሉ እንዲያገኙ የሚረዳ ድህረ-ገፅ ነዉ:: <br>
   
</p>
<p class="text-center" style="font-size:12px;">
ሊያገኙን ይፈልጋሉ?<br>
<a href="tel:+251 91 888 8225" style="text-decoration:none; font-size:12px; padding:5px; border-right:2px solid #00bfff; color:black;"><img class="myname" src="../../icons/phone.png" width="20" height="20"> +251 91 888 8225</a>
<a href="tel:+251 91 888 8233" style="font-size:12px; padding:5px; border-right:2px solid #00bfff; color:black;"><img class="myname" src="../../icons/phone.png" width="20" height="20"> +251 91 888 8233</a>

<a href="mailto:contact@yetale.com" style="font-size:12px; padding:3px; 
color:black;"><img class="myname" src="../../icons/envelope-square-256.png" width="20" height="20"> contact@yetale.com</a>
<br>


 <span style="color:#00bfff; font-size:12px;">&copy; 20<?php echo date('y');?> የታለ</span><br> <br>
</p>
</footer>


</body>
</html>

