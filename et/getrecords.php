

<?php
/*
 * @author Shahrukh Khan
 * @website http://www.thesoftwareguy.in
 * @facebbok https://www.facebook.com/Thesoftwareguy7
 * @twitter https://twitter.com/thesoftwareguy7
 * @googleplus https://plus.google.com/+thesoftwareguyIn
 */
require_once("../db/connection.php");


$limit = (intval($_GET['limit']) != 0 ) ? $_GET['limit'] : 9;
$offset = (intval($_GET['offset']) != 0 ) ? $_GET['offset'] : 0;

$query = "SELECT * FROM feed WHERE 1 ORDER BY feed_id DESC LIMIT $limit OFFSET $offset";
$sql = mysqli_query($conn,$query);

?>
    <div class="row" style="padding:20px;"><?php
        while($fetch = mysqli_fetch_array($sql)){
 ?>
 
      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4" style="box-shadow: 0px 8px 8px 0px rgba(0,0,0,0.1); border-radius:10px; width:100%; height:auto; padding:10px; font-size:13px;">

      <?php
    if($fetch['feed_type']=='job'){
        $id = $fetch['id'];
        $q = "SELECT * FROM vacancy WHERE id=$id";
        $s = mysqli_query($conn,$q);
        $f = mysqli_fetch_array($s);
        $position = $f['position'];
        $quan = $f['quan'];
        $username = $fetch['username'];
                $username = str_replace('_',' ',$username);
                $username = str_replace('1','',$username);
                $username = str_replace('2','',$username);
                $username = str_replace('3','',$username);
                $username = str_replace('4','',$username);
                $username = str_replace('5','',$username);
                $username = str_replace('6','',$username);
                $username = str_replace('7','',$username);
                $username = str_replace('8','',$username);
                $username = str_replace('9','',$username);
                 $id = $fetch['username'];


        ?>
        <a href="user/profile?id=<?php echo $id?>" style="font-size:13px;"><?php echo $username;?> <span style="font-size:11px;"></span></a> የስራ ማስታወቂያ ለጥፈዋል::<br>
        <span style="font-size:10px;"><?php echo $fetch['date_updated']?></span><br>
        <img src="../icons/position.png" width="70" height="70"><br>
        <span style="font-size:13px;"><?php echo $position?> (<?php echo $quan?>)</span><br>

       <a href="user/view-vacancy?id=<?php echo $fetch['id']?>" class="btn btn-sm mt-1 text-white">ዝርዝር <img src="../icons/Tg/PicsArt_02-18-11.38.55.png" width="15" height="15"></a><br>
        <?php
    }
    if($fetch['feed_type']=='event'){
        $id = $fetch['id'];
        $q = "SELECT * FROM events WHERE id=$id";
        $s = mysqli_query($conn,$q);
        $f = mysqli_fetch_array($s);
        $photo = $f['photo'];
        $caption = $f['caption'];
        $place = $f['place'];
        $starts_on = $f['starts_on'];
        $tym = $f['tym'];
        $days = $f['days'];
       

        ?>
        
        <a href="user/profile?id=<?php echo $fetch['username']?>" style="font-size:13px;"><?php echo $fetch['name'];?></a> ሁነት ለጥፈዋል::<br>
        <span style="font-size:10px;"><?php echo $fetch['date_updated']?></span><br>
        <img src="../images/<?php echo $photo?>" width="100%" height="200" class="mb-2">
        <span style="font-size:16px; text-transform:uppercase; color:#00bfff;"><?php echo $f['caption']?></span><br>
                 <div style="font-size:13px;"> <img src="../icons/destination.png" width="18" height="18"> <?php echo $place?></div>
                <?php $date = $f['starts_on'];
                   $day = date('D',strtotime($date));
                   if($day=='Mon') $day = "ሰኖ";
                   if($day=='Tue') $day = "ማክሰኖ";
                   if($day=='Wed') $day = "ረቡእ";
                   ?>

                <div style="font-size:13px;"><img src="../icons/calendar-512.png" width="18" height="18"> <?php echo $day?>, <?php echo $starts_on?></div>
                <div style="font-size:13px;"><img src="../icons/business_12-20-512.png" width="18" height="18"> <?php echo $days?> <?php if($days>1) echo "ቀናት"; if($days==1) echo "ቀን";?></div>

<?php 
$ticket = $f['ticket'];
if($ticket!='') echo "<div style='font-size:13px;' class=''>
    <img src='../icons/1630682.png' width='18' height='18'> $ticket</div>" ?>
            <div class="mb-1">
                    
                <a href="HOME?ev_lk=<?php echo $fetch['id']?>" class="btn text-white"><img src="../icons/thumbs-o-up-256.png" width="25" height="25" style="filter:invert(1);"> ውደድ</a><br>
                <?php
                $id = $fetch['id'];
                $qu = "SELECT count(email) AS num FROM interested WHERE id=$id";
                $sq = mysqli_query($conn,$qu);
                $fh = mysqli_fetch_array($sq);
                $num = $fh['num'];
                ?>
                <sup><?php echo $num?> ሰዎች ወደውታል::</sup>
                </div>
                
                  


        
        <?php
    }
      if($fetch['feed_type']=='review'){
          $id = $fetch['id'];
          $q = "SELECT * FROM rating WHERE id=$id";
          $s = mysqli_query($conn,$q);
          $f = mysqli_fetch_array($s);
          $giver = $f['giver'];

          $qpu = "SELECT photo FROM normal WHERE id='$giver'";
        $spu = mysqli_query($conn,$qpu);
        $fpu = mysqli_fetch_array($spu);
                $giver = str_replace('_',' ',$giver);
                $giver = str_replace('1',' ',$giver);
                $giver = str_replace('2',' ',$giver);
                $giver = str_replace('3',' ',$giver);
                $giver = str_replace('4',' ',$giver);
                $giver = str_replace('5',' ',$giver);
                $giver = str_replace('6',' ',$giver);
                $giver = str_replace('7',' ',$giver);
                $giver = str_replace('8',' ',$giver);
                $giver = str_replace('9',' ',$giver);

        $name = $fetch['name'];

        $qp = "SELECT photo FROM bussiness WHERE id='$name'";
        $sp = mysqli_query($conn,$qp);
        $fp = mysqli_fetch_array($sp);
        $photo = $fp['photo'];
                $name = str_replace('_',' ',$name);
                $name = str_replace('1',' ',$name);
                $name = str_replace('2',' ',$name);
                $name = str_replace('3',' ',$name);
                $name = str_replace('4',' ',$name);
                $name = str_replace('5',' ',$name);
                $name = str_replace('6',' ',$name);
                $name = str_replace('7',' ',$name);
                $name = str_replace('8',' ',$name);
                $name = str_replace('9',' ',$name);

        ?>
        <a style="font-size:13px;"><?php echo $giver;?></a> ለ <a href="user/profile?id=<?php echo $fetch['name']?>"><span style="font-size:13px;"><?php echo $name?></span></a>አስተያየት ሰጥተዋል::<br>
        <span style="font-size:10px;"><?php echo $fetch['date_updated']?></span><br>
        <img src="../images/<?php echo $photo?>" width="100%" height="200" class="mb-2">
        
        <?php
        $id = $fetch['id'];
        $qrt = "SELECT * FROM rating WHERE id=$id";
        $srt = mysqli_query($conn,$qrt);
        $frt = mysqli_fetch_array($srt);
        ?>

        <a href="../images/<?php echo $fpu['photo']?>"><img src="../images/<?php echo $fpu['photo']?>" width="50" height="50" style="border-radius:100%;"></a> <span style="font-size:13px;"><?php echo $giver?></span><hr>
        <div style="font-size:12px;" class="ml-2 mb-2"><?php echo $frt['comment']?></div>

        

        <?php
    }
     if($fetch['feed_type']=='photo'){
         $email = $fetch['name'];
         $qnm = "SELECT name,id FROM bussiness WHERE email='$email'";
         $snm = mysqli_query($conn,$qnm);
         $fnm = mysqli_fetch_array($snm);
         $name = $fnm['name'];
         $id = $fnm['id'];
        ?>
        <a href="user/profile?id=<?php echo $id?>" style="font-size:13px;"><?php echo $name;?></a> አዲስ ፎቶ ለጥፈዋል::<br>
        <span style="font-size:10px;"><?php echo $fetch['date_updated']?></span><br>
        <img src="../images/<?php echo $fetch['image']?>" width="100%" height="200" class="mb-1">

        

        <?php
    }
    if($fetch['feed_type']=='location'){
        ?>
        <a href="user/profile?id=<?php echo $fetch['name']?>" style="font-size:13px;"><?php echo $fetch['name'];?></a> updated its location.<br>
        <span style="font-size:10px;"><?php echo $fetch['date_updated']?></span><br>
        <img src="../images/<?php echo $fetch['image']?>" width="100%" height="200" class="mb-2">

        <div class="btn mb-2">ከሁሉ በላይ ታላቅ ሀያል ንጉስ</div>

        <?php
    }

    if($fetch['feed_type']=='join'){
        ?>
        <a href="user/profile?id=<?php echo $fetch['username']?>" style="font-size:13px;"><?php echo $fetch['name'];?></a> የታለን ተቀላቅለዋል::<br>
        <span style="font-size:10px;"><?php echo $fetch['date_updated']?></span><br>
        

       <a href="user/profile?id=<?php echo $fetch['username']?>" class="btn btn-sm mt-1 text-white">ተመልከት <img src="../icons/Tg/PicsArt_02-18-11.38.55.png" width="15" height="15"></a><br>

        <?php
    }
 ?>


</div>
 <?php
}?>
   
</div><?php



?>