

<?php require("../db/connection.php");?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>  Sign Up | Yetale</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/mystyle.css?version=50">
    <style>
       .container-fluid{
           width: 95%;
  padding-right: 60px;
  padding-left: 60px;
  padding-bottom:30px;
  padding-top:30px;
  margin-right: auto;
  margin-left: auto;
 
    
   box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.5);
       }
      
    </style>
</head>
<body>
   <?php
    if(!isset($_GET['type'])){
        ?>
         
                 <div id="select">
                
    <a href="../HOME"><img src="../../icons/yet.png" width="60" height="60" class="ml-1 mt-1" style=""></a><br><br>
         
    <div class="container-fluid mt-5">
         <p class="text-center recentlyopened">የመለያ አይነት ይምረጡ</p>
         <form method="post">
             
             <p class="text-center" style="padding:4px; box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.5); border-radius:5px;">
                
                    <a href="register?type=biz" style="">
                    <img src="../../icons/x-14-512.png" width="80" height="80"> <br> 
                    <u class="ml-5">የንግድ መለያ</u> <img src="../../icons/chevron-right-512.png"  width="50" height="50" style="margin-top:-70px; float:right;"></a><br>
                                        </p>
                                         
                     <p class="text-center" style="padding:4px; box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.5); border-radius:5px;">
                    <a href="register?type=user" style="">
                    <img src="../../icons/user.png" width="80" height="80"> <br> 
                    <u class="ml-5">የተጠቃሚ መለያ</u><img src="../../icons/chevron-right-512.png"  width="50" height="50" style="margin-top:-70px; float:right;"></a><br>
                    
             </p>
             <p class="text-center">
              <span style="font-size:11px; color:black;">ቀድመው ተመዝግበዋል? <a href="login" style="font-size:11px;">ይግቡ</a></span>
        </p>
            
    
        <?php
    }

        if(isset($_GET['type'])){
            
            $type = $_GET['type'];
            
            if($type=='biz'){
                     ?>
                     <a href="../HOME"><img src="../../icons/yet.png" width="60" height="60" class="mt-1 ml-1"></a><br><br>
                    <div class="container-fluid">
                        <p class="text-center recentlyopened mt-3" style="border-bottom:1px solid black">
                         <img src="../../icons/x-14-512.png" width="80" height="80"> <br>     
                        የንግድ መለያ<br><span>ከሴኮንዶች በላይ አይፈጅም::</span></p>
                        <form method="post">
                        
                        <span>የንግድ ስያሜ</span><br>
                        <input type="text" name="name-biz" id="name-biz" maxlength="20" class="form-control" autofocus required>
                        <span id="error-name-biz" style="font-size:10px; color:red;"></span>
       
                        
                        <span>ኢሜይል</span><br>
                        <input type="email" name="email-biz" id="email-biz" class="form-control" autofocus required>
                        <span id="error-email-biz" style="font-size:10px; color:red;"></span>
                      

                        <div>የይለፍ ቃል</div>
                        <input type="password" name="pass-biz" id="pass-biz" class="form-control" required>
                        <span id="error-pass-biz" style="font-size:10px; color:red;"></span>
                        
                        <div>የይለፍ ቃል (ድጋሚ)</div>
                        <input type="password" name="pass1-biz" id="pass1-biz" class="form-control" required>
                        <span id="error-pass-biz1" style="font-size:10px; color:red;"></span>
                        
                        

                        <p class="text-center">
                            <input type="submit" class="btn text-white mt-3 mb-5" name="add-biz" value="ቀጥል" style="width:150px;"> 
                            <br><br><br>
                        </p>
                        </form>
                        
                        <?php
                            if(isset($_POST['add-biz'])){

                                $name= $_POST['name-biz'];
                                 $new_name = str_replace(' ','_',$name);  
                                 $query_num = "SELECT count(email) AS num FROM bussiness WHERE name='$name'";
                                 $sql_num = mysqli_query($conn,$query_num);
                                 $fetch_num = mysqli_fetch_array($sql_num);
                                 $num = $fetch_num['num']+1;
                                 $id = $new_name.'_'.$num; 

                                $pass= $_POST['pass-biz'];
                                $pass1= $_POST['pass1-biz'];
                                $pass_len = strlen($pass);
                                $email = $_POST['email-biz'];

                                
                                 // delete if there is incomplete sign-up attempt before 
                                 $query_jmr = "SELECT count(*) AS num FROM user WHERE email='$email' AND verified!='yes'";
                                $sql_jmr = mysqli_query($conn,$query_jmr);
                                $fetch_jmr = mysqli_fetch_array($sql_jmr);
                                $jmr = $fetch_jmr['num'];

                                if($jmr==1){
                                    $q1 = "DELETE FROM user WHERE email='$email'";
                                    $q2 = "DELETE FROM bussiness WHERE email='$email'";
                                    $s1 = mysqli_query($conn,$q1);
                                    $s1 = mysqli_query($conn,$q2);
                                }
                                 // end of prev delete


                                
                                $query_email = "SELECT count(*) AS num FROM user WHERE email='$email'";
                                $sql_email = mysqli_query($conn,$query_email);
                                $fetch_email = mysqli_fetch_array($sql_email);
                                $emails = $fetch_email['num'];
                                
                                if($emails>0){
                                     ?>
                                    <script>
                document.getElementById("error-email-biz").innerHTML = "የማያገለግል ኢሜይል። እባክዎ ሌላ ያስገቡ።";
                var ez = document.getElementById("email-biz");
                ez.style.border = "1px solid red";
                ez.focus();
                </script>
                                    <?php
                                }
                                if($pass_len<6){
                                    ?>
                                    <script>
                document.getElementById("error-pass-biz").innerHTML = "የይለፍ ቃል ከ6 ፊደላት ማነስ የለበትም።";
                var ps = document.getElementById("pass-biz");
                ps.style.border = "1px solid red";
                ps.focus();
                </script>
                                    <?php
                                }

                                 if($pass!=$pass1){
                                    ?>
                                    <script>
                document.getElementById("error-pass-biz").innerHTML = "የይለፍ ቃሎች አልተዛመዱም።";
                document.getElementById("error-pass-biz1").innerHTML = "የይለፍ ቃሎች አልተዛመዱም።";
                var ps = document.getElementById("pass-biz");
                ps.style.border = "1px solid red";
                var ps1 = document.getElementById("pass1-biz");
                ps1.style.border = "1px solid red";
                         
                </script>
                                    <?php
                                }

                                if($pass_len>=6 && $pass==$pass1 && $emails<1){
                                     $confirm = mt_rand(1000,9999);
                                     $today = date('20y-m-d');
                                     $pass = md5($pass);
                                     $invitor = '';
                                        if(isset($_GET['i'])){
                                             $invitor = strval($_GET['i']);
                                        }
                                    
                                    $query = "INSERT INTO user VALUES ('','$email','$pass','bussiness',$confirm)";
                                    $sql = mysqli_query($conn,$query);
                                     $query = "INSERT INTO bussiness (name,id,acc_type,password,email,date_joined,invitor) VALUES ('$name','$id','none','$pass','$email','$today','$invitor')";
                                    $sql = mysqli_query($conn,$query);
                                    $header = "From yetale.com";
                                    $subject = "Sign Up Verification";
                                    $message = "Your confirmation code is $confirm.\n\n This email was sent to you because of your sign up request. \n\n
                                    If you didn't request to sign up, simply ignore it.\n\n Yours\n The YETALE team\n https://www.yetale.com";
                                    $mail = mail($email,$subject,$message,$header);
                                   
                        
                                    
                                   
                                    ?>
                                    <!-- js for detecting the location and saving in cookie-->
                                    <?php
                                    ?>
                                    <script>
                                       
                                        location.href="verify?em=<?php echo $email?>"
                                    </script><?php
                                   
                                }
                            }
                        ?>


                    </div>
               <?php
                      }

        if($type=='user'){
               ?>
               <a href="../HOME"><img src="../../icons/yet.png" width="60" height="60" class="mt-1 ml-1"></a><br><br>
                    <div class="container-fluid">
                        <p class="text-center recentlyopened mt-3" style="border-bottom:1px solid black">
                        <img src="../../icons/user.png" width="80" height="80"> <br>     
                       የተጠቃሚ መለያ<br><span>ከሴኮንዶች በላይ አይፈጅም::</span></p>
                        <form method="post" enctype="multipart/form-data">
                        <span>ሙሉ ስም</span><br>
                        <input type="text" maxlength="25" id="name" name="name" class="form-control" autofocus required>
                     
                        <br><label for="photo-user" style="border-bottom:2px solid #00bfff; background-color:#e6e6ff;" class="form-control"><img src="../../icons/camera.png" width="22" height="22"> የእርስዎ ፎቶ</label> 
                        
                        <input type="file" name="photo-user" id="photo-user" style="background-color:#e6e6f2; border-bottom:2px solid #00bfff;" class="form-control" accept="image/*" hidden> 
                        <span id="error-photo" style="font-size:10px; color:red;"></span>
                       

                        <div>ኢሜይል</div>
                        <input type="email" name="email-user" id="e-user" class="form-control" required>
                        <span id="err-em" style="color:red; font-size:10px;"></span>

                        <div>የይለፍ ቃል</div>
                        <input type="password" name="pass" id="pass-u" class="form-control" required>
                        <span id="error-pass-u" style="color:red; font-size:10px;"></span>
                        
                        <div>የይለፍ ቃል (ድጋሚ)</div>
                        <input type="password" name="pass1" id="pass1-u" class="form-control" required>
                        <span id="error-pass1-u" style="color:red; font-size:10px;"></span><br>

                        <p class="text-center">
                            <input type="submit" class="btn text-white mt-2" name="add-user" value="ቀጥል" style="width:150px;"> 
                        </p>
                        </form>
                        <?php
                            if(isset($_POST['add-user'])){
                               
                                $pass= $_POST['pass'];
                                $pass1= $_POST['pass1'];
                                $pass_len = strlen($pass);
                                 $email = $_POST['email-user'];

                                 // delete if there is incomplete sign-up attempt before 
                                 $query_jmr = "SELECT count(*) AS num FROM user WHERE email='$email' AND verified!='yes'";
                                $sql_jmr = mysqli_query($conn,$query_jmr);
                                $fetch_jmr = mysqli_fetch_array($sql_jmr);
                                $jmr = $fetch_jmr['num'];

                                if($jmr==1){
                                    $q1 = "DELETE FROM user WHERE email='$email'";
                                    $q2 = "DELETE FROM normal WHERE email='$email'";
                                    $s1 = mysqli_query($conn,$q1);
                                    $s1 = mysqli_query($conn,$q2);
                                }
                                 // end of prev delete
                                

                                $query_email = "SELECT count(email) AS num FROM user WHERE email='$email'";
                                $sql_email = mysqli_query($conn,$query_email);
                                $fetch_email = mysqli_fetch_array($sql_email);
                                $emails = $fetch_email['num'];

                               

                                 $size = $_FILES['photo-user']['size']/1024;

                             
                                 
                                if($size>1000){
                                    ?>
                                    <script>
                                    document.getElementById("error-photo").innerHTML = "እባክዎ ከ1 ሜጋ ባይት ያልበለጠ ፎቶ ይምረጡ።";
                                    var phuser = document.getElementById("photo-user");
                                    phuser.style.border = "1px solid red";
                                    
                                    </script>
                                    <?php
                                }
                                 if($emails>0){
                                     ?>
                                    <script>
                document.getElementById("err-em").innerHTML = "የማያገለግል ኢሜይል። እባክዎ ሌላ ያስገቡ።";
                var eer = document.getElementById("e-user");
                eer.style.border = "1px solid red";
                eer.focus();
                </script>
                                    <?php
                                }

                                if($pass_len<6){
                                    ?>
                                    <script>
                document.getElementById("error-pass-u").innerHTML = "የይለፍ ቃል ከ6 ፊደላት ማነስ የለበትም።";
                var passu = document.getElementById("pass-u");
                passu.style.border = "1px solid red";
                passu.focus();
                </script>
                                    <?php
                                }

                                 if($pass!=$pass1){
                                    ?>
                                    <script>
                document.getElementById("error-pass-u").innerHTML = "የይለፍ ቃሎች አልተዛመዱም።";
                document.getElementById("error-pass1-u").innerHTML = "የይለፍ ቃሎች አልተዛመዱም።";
                var pass = document.getElementById("pass-u");
                pass.style.border = "1px solid red";
                var pass1 = document.getElementById("pass1-u");
                pass1.style.border = "1px solid red";
                pass.focus();
                
                </script>
                                    <?php
                                }

                                if($pass_len>=6 && $pass==$pass1 && $emails<1 && $size<=1000){
                                    $name= $_POST['name'];
                                    $name1 = str_replace(' ','_',$name);
                                    // submit
                                    $query_id = "SELECT count(id) AS num FROM normal WHERE full_name='$name'";
                                    $sql_id = mysqli_query($conn,$query_id);
                                    $fetch_id = mysqli_fetch_array($sql_id);
                                    $id = $fetch_id['num']+1;
                                    $user_id = $name1.'_'.$id;
                                    $today = date('20y-m-d');

                                    
                                    $email = $_POST['email-user'];
                                    $pass = $_POST['pass'];

                                    if($_FILES['photo-user']['name']==''){
                                        $photo = '';
                                    }

                                    if($_FILES['photo-user']['name']!=''){
                                        
                                         $photo = str_shuffle('abvuggyuih').$_FILES['photo-user']['name'];
                                   
                                   
                                    $Temp =  $_FILES['photo-user']['tmp_name'];
                                    $MyLocation= "../images/".$photo ;
                                    move_uploaded_file($Temp,$MyLocation);
                                        
                                    }
                                    
                                    $confirm = mt_rand(1000,9999);
                                    $pass = md5($pass);

                                     $query = "INSERT INTO normal VALUES ('$user_id','$photo','$email','$pass','$name','$today')";
                                     $sql = mysqli_query($conn,$query);
                                    
                                     $query = "INSERT INTO user VALUES ('$user_id','$email','$pass','user',$confirm)";
                                     $sql = mysqli_query($conn,$query);
                                      $header = "From yetale.com";
                                    $subject = "Sign Up Verification";
                                    $message = "Your confirmation code is $confirm.\n\n This email was sent to you because of your sign up request. \n\n
                                    If you didn't request to sign up, simply ignore it.\n\n Yours\n The YETALE team\n https://www.yetale.com";
                                    mail($email,$subject,$message,$header);

                                   ?>
                                    <script>location.href="verify?em=<?php echo $email?>"</script>
                                   <?php
                                    
                                }
                            }
                        ?>


                    </div>
             


                    </div>
               <?php
                }
        
        }

        ?>

        
       
</body>
</html>
