<?php
require("../db/connection.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=
    , initial-scale=1.0">
    <title>Categories | Yetale </title>
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/mystyle.css?version=50">
    <style>
        #load{
             margin:auto;
            animation: eyoha 6s infinite;
            -webkit-animation: eyoha 6s infinite;
        } 
        @keyframes eyoha{
           from{
                transform:rotate(360deg);
            }
            to{
                transform:rotate(-360deg);
            }
        }
        @-webkit-keyframes eyoha{
           from{
                transform:rotate(360deg);
            }
            to{
                transform:rotate(-360deg);
            }
        }
        .animate-bottom{
            position:relative;
            -webkit-animation-name:animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s;
        }
        
        @keyframes animatebottom{
             from {
                bottom:-100px; opacity:0;
            }
            to {
                bottom:0px; opacity:1;
            }
        }
        @-webkit-keyframes animatebottom{
             from {
                bottom:-100px; opacity:0;
            }
            to {
                bottom:0px; opacity:1;
            }
        }
        #loaded{
            display:none;
            
        }
    </style>
     <script>
         var myVar;
        function Load(){
            myVar = setTimeout(showPage,4000);
        }
        function showPage(){
            document.getElementById("loader").style.display="none";
             document.getElementById("loaded").style.display = "block";

        }
    </script>
</head>
<body onload="Load()">
<div id="loader" style="position:absolute; top:50%; left:50%; transform:translate(-50%,-50%);">

<span style="color:black; font-size:13px;"></span><span style="color:#00bfff; font-size:50px;" id="auto-write"></span>
<script>
    const text = document.getElementById("auto-write");
const prog = '...';

let idx=1;
setInterval(Write,250);

function Write(){
text.innerText = prog.slice(0,idx);
idx++;

if(idx>prog.length){
    idx=1;
}
}
</script>


</div>
<div id="loaded" class="">
        <a style="width:50px; height:50px;" class="btn" onclick="window.history.back()"><img src="../../icons/Tg/PicsArt_02-18-11.36.20.png" width="40" height="40" style="cursor:pointer;" class=""></a><br>
    <?php $bussiness = strval($_GET['cat']);?>
    <p class="text-center"><img src="../../icons/covv.png" width="100%" height="300" style="max-width:500px;"><br>
      <span style="font-size:20px;"><?php echo $bussiness;?></span>
   </p>
   
    <div class="mt-5">
       
         <?php   $biz = strval($_GET['cat']);
                $query = "SELECT count(id) AS num FROM bussiness WHERE category='$biz'";
                $sql = mysqli_query($conn,$query);
                $fetch = mysqli_fetch_array($sql);
                $num = $fetch['num'];
                if($num==0){
                    ?>
                    <script>document.getElementById("choose").innerHTML="";</script>
                    <?php
                }
                ?>
		<?php
			if($num>0){
				?>
				<p class="text-center recentlyopened" style="font-size:17px;"><?php echo $num?> ውጤቶች::</p>

				<?php
					}

			if($num==0){
				?>
				<p class="text-center recentlyopened" style="font-size:17px;">ይቅርታ ምንም ውጤት አልተገኘም።</p>

				<?php
					}
				?>
                </div>
   

    
    
    <div class="container-fluid">
        

        <p class="text-center">

       
        <?php 
        $qc = "SELECT count(city) AS num FROM bussiness";
        $sc = mysqli_query($conn,$qc);
        $fc = mysqli_fetch_array($sc);
        $cities = $fc['num'];

        
           
           
                $biz = strval($_GET['cat']);
            
               
                $query = "SELECT count(id) AS num,city FROM bussiness WHERE category='$biz'";
                $sql = mysqli_query($conn,$query);

			if($num>0){
				?>
				<p class="text-center recentlyopened" style="font-size:17px;">በከተማ ለይተው ይፈልጉ።</p>

				<?php
					}
                
                    ?><p class="text-center"><span class="recentlyopened" style="font-size:17px;" id="choose"></span><?php
               
                while($fetch = mysqli_fetch_array($sql)){
                     $num = $fetch['num'];
                     $city = $fetch['city'];
                     
                if($num>0){
                    ?>
                    <a href="search-results?bussiness=<?php echo $biz?> & city=<?php echo $city?>"><?php echo $city.' ('.$num.')'?></a><br>
                    
                    <?php
                
               
              
            } }
        ?></p>
         </p>
         
       
      
  
  </div>
        
        
       

    </p>
        
   
          <hr>
    <hr>
   
<hr>
<footer class="mt-2">
    <p class="text-center" style="font-size:12px;">
    <span style="color:white; background-color:#00bfff; padding:5px; border-radius:10px;"><strong>Yetale.et</strong></span> 
    የንግድ ባለቤቶች በቀላሉ ምርትና አገልግሎታቸውን ከሚፈልጉ ደንበኞቻቸው ጋር በቀላሉ መገናኘት 
     እንዲችሉ እንዲሁም ደንበኞች የሚፈልጉትን በቀላሉ እንዲያገኙ የሚረዳ ድህረ-ገፅ ነዉ:: <br>
   
</p>
<p class="text-center" style="font-size:12px;">
ሊያገኙን ይፈልጋሉ?<br>
<a href="tel:+251 91 888 8225" style="text-decoration:none; font-size:12px; padding:5px; border-right:2px solid #00bfff; color:black;"><img class="myname" src="../../icons/phone.png" width="20" height="20"> +251 91 888 8225</a>
<a href="tel:+251 91 888 8233" style="font-size:12px; padding:5px; border-right:2px solid #00bfff; color:black;"><img class="myname" src="../../icons/phone.png" width="20" height="20"> +251 91 888 8233</a>

<a href="mailto:contact@yetale.com" style="font-size:12px; padding:3px; 
color:black;"><img class="myname" src="../../icons/envelope-square-256.png" width="20" height="20"> contact@yetale.com</a>
<br><br>

ንግድዎን ያስመዝግቡ:: <a href="register" class="btn-sm text-white" style="">አስመዝግብ</a> <br>
የሚፈልጉትን ያግኙ፣አስተያየትዎን ያጋሩ፣ለጓደኞችዎ ይጋብዙ፣
የንግድ መለያዎን ያዘምኑ:: <a href="login" class="btn-sm text-white">ግባ</a> <br>

 <span style="color:#00bfff; font-size:12px;">&copy; 20<?php echo date('y');?> የታለ</span><br> <br>
</p>
</footer>
        </div>
         </div>
      

</body>
</html>