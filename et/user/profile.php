
<?php

session_start();
require("../db/connection.php");
                    $id = strval($_GET['id']);
                           $q = "SELECT avg(star) AS num FROM rating WHERE bussiness='$id'";
                           $s = mysqli_query($conn,$q);
                           $f = mysqli_fetch_array($s);
                           $avg = $f['num'];
                           $q = "UPDATE bussiness SET avg_rating=$avg WHERE id='$id'";
                           $s = mysqli_query($conn,$q);
if(isset($_COOKIE['email']) && isset($_COOKIE['lat']) && isset($_COOKIE['lng'])){
$email = $_COOKIE['email'];
$lat = $_COOKIE['lat'];
$lng = $_COOKIE['lng'];

	$query = "UPDATE bussiness SET lat='$lat',lng='$lng' WHERE email='$email'";
	$sql = mysqli_query($conn,$query);
} 
 

    $query = "SELECT * FROM bussiness WHERE id='$id'";
    $sql = mysqli_query($conn,$query);
    $fetch = mysqli_fetch_array($sql);
    $photo = $fetch['photo'];
    $category = $fetch['category'];
     $city = $fetch['city'];
     $logo = $fetch['logo'];
     $lvl = $fetch['acc_type'];
     $vid = $fetch['video'];
     $name = $fetch['name'];
     $views = $fetch['views']+1;
     $lat = $fetch['lat'];
     $lng = $fetch['lng'];
     $id = $fetch['id'];

     $query = "UPDATE bussiness SET views=$views WHERE id='$id'";
     $sql = mysqli_query($conn,$query);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> <?php echo $name?> | Yetale</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/mystyle.css?version=51">
    <style>
        #close,#closee{
            cursor:pointer;
        }
        .m{
            padding:20px;
            background-color:white;
            border-radius:5px;
            width:90%;
            height:50vh;
            position:absolute;
            top:80%;
            left:50%;
            transform:translate(-50%,-50%);
            display:none;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.5);
        }
        .animate-bottom{
           
            -webkit-animation-name:animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 3s;
        }
        
        @keyframes animatebottom{
             from {
                
                opacity:0;
            }
            to {
                
                opacity:1;
            }
        }
        #mm{
            opacity:1;
        }

#close{
    color:red;
    float:right;
    font-size: 20px;
    margin-top:-20px;
    margin-right:-10px;

}
#close:hover ,
#close:focus{
    color:red;
    text-decoration: none;
    cursor: pointer;
}



        video{
            width:100%;
           
            object-fit:cover;
            
        }
        .logo-anim{
            animation:1s oo infinite;
        }
        @keyframes oo {
            0%,50%{
                opacity:0.5;
                width:100;
                height:100;
            }
            100%{
                opacity:0.8;
                width:200;
                height:200;
            }
        }
        .option{
            
            color:black;
            overflow-x:auto;
            
            

        }
        a{
            text-decoration:none;
        }
        
    </style>
  
</head>
<body>
    
    <a href="../HOME"><img src="../../icons/yet.png" width="60" height="60" style="cursor:pointer; position:absolute; top:0; left:0;" class="mt-1 ml-1"></a>
    <div id="none-modal">
    <?php
    $id = strval($_GET['id']);

    $query = "SELECT * FROM bussiness WHERE id='$id'";
    $sql = mysqli_query($conn,$query);
    $fetch = mysqli_fetch_array($sql);
    $photo = $fetch['photo'];
    $category = $fetch['category'];
     $city = $fetch['city'];
     $logo = $fetch['logo'];
     $lvl = $fetch['acc_type'];
     $vid = $fetch['video'];
     $name = $fetch['name'];
     $views = $fetch['views']+1;
     $lat = $fetch['lat'];
     $lng = $fetch['lng'];
     $id = $fetch['id'];

     $query = "UPDATE bussiness SET views=$views WHERE id='$id'";
     $sql = mysqli_query($conn,$query);

    ?>
    
         <div class="image mb-2">
        <img src="../../images/<?php echo $photo?>" width="100%" height="300">
    </div>
        
   <div class="nav">
<a href="" style="color:white; border-bottom:4px solid white;">Bio & Review</a>
<a href="images?id=<?php echo $id?>">Images</a>
<a href="event?id=<?php echo $id?>">Events</a>
<a href="vacancy?id=<?php echo $id?>"> Vacancy</a>
</div>

    <div class="info">
        <p class="text-center" id="info">
            <img src="../../images/<?php echo $logo?>" width="100" height="100" style="border-radius:100%;" alt="logo"><br> 
            <strong style="font-size:2em;"><?php echo $fetch['name']?></strong>
             <?php if($lvl=='blue'){?><img src="../../icons/blue.png" width="30" height="30" style="margin-top:-10px;"><?php }
              if($lvl=='green'){?><img src="../../icons/green.png" width="30" height="30" style="margin-top:-10px;"><?php }
            ?><br>
            <span style="font-size:14px;">Since <?php echo $fetch['date_founded']?></span><br>
            <span style="color:#00bfff; font-weight:700;">@<?php echo $fetch['id']?></span><br>
            <span style="font-size:12px;"><img src="../../icons/eye-128.png" width="13" height="13"> <?php echo $views?></span><br>
            <a href="map?id=<?php echo $id?>" class="btn" style="text-decoration:none; background-color:#00bfff; color:white;"><img src="../../icons/Tg/PicsArt_02-18-12.06.46.png" width="22" height="22"> View on Map</a>
            <button class="btn mt-1" id="im"><img src="../../icons/Tg/PicsArt_02-18-12.04.17.png" width="20" height="20"> Invite a Friend</button>
            <br>
            
            <?php if($lvl !='none'){
                ?><a href="mailto:<?php echo $fetch['email']?>" class="btn text-white mt-1"><img src="../../icons/Tg/PicsArt_02-18-12.18.18.png" width="20" height="20"> Send Email</a>
               <a href="tel:<?php echo $fetch['phone']?>" class="btn text-white mt-1"><img src="../../icons/Tg/CALL.png" width="30" height="30"> Call</a>
                <?php
            }?><br>
           
            <img src="../../icons/about.png" width="20" height="20"> <?php echo $fetch['description'];?><br>
            <?php

                $query_avg = "SELECT avg(star) as average FROM rating WHERE bussiness='$id'";
                $sql_avg = mysqli_query($conn,$query_avg);
                $fetch_avg = mysqli_fetch_array($sql_avg);
                $avrg = $fetch_avg['average'];
                $rate = round($avrg,1);

                $query_sum = "SELECT count(star) as total FROM rating WHERE bussiness='$id'";
                $sql_sum = mysqli_query($conn,$query_sum);
                $fetch_sum = mysqli_fetch_array($sql_sum);
                $sum = $fetch_sum['total'];
            
               
            ?><br>
            <?php

                if($rate=='0'){
    ?><img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='1'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'1' && $rate<2){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='2'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'2' && $rate<3){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='3'){
    ?><img src="../../icons/full-star.png" width="18" height="18">
    <img src="../../icons/full-star.png" width="18" height="18">
    <img src="../../icons/full-star.png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'3' && $rate<4){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='4'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'4' & $rate<5){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <?php 
}
if($rate=='5'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <?php 
}

            ?>
            
            
            <br>
            <span><?php echo $rate?></span><br>
            <?php
            $queryN = "SELECT count(id) as num FROM rating WHERE bussiness='$id'";
            $sqlN = mysqli_query($conn,$queryN);
            $fetchN = mysqli_fetch_array($sqlN);
            $num = $fetchN['num'];?>
            <span style="font-size:13px;">based on <?php echo $sum?> reviews</span>
        


    
    <!-- end of modal -->
            
        </p>
       
            <?php 
            if($lat=='' || $lng==''){
                ?><p class="text-center">Bussiness location isn't added yet.</p><?php
            }
             if($lat!='' && $lng!=''){
                ?><p class="text-center"><img src="https://maps.geoapify.com/v1/staticmap?style=osm-carto&width=600&height=400&center=lonlat:<?php echo $lng?>,<?php echo $lat?>&zoom=14&marker=lonlat:<?php echo $lng?>,4;type:awesome;color:%23655e90;size:large;icon:industry|lonlat:<?php echo $lng?>,<?php echo $lat?>;type:awesome;color:%23655e90;size:large;icon:industry&apiKey=fc3cdb31c2cb4193896604076f10fc11" width="200" height="200"></p><?php
            }
            ?>
            
       
        <div class="container">
             <span style="float:left;"><img src="../../icons/f041-256.png" width="20" height="23"> City</span>
            <span style="float:right;"><?php echo $fetch['city']?>, Ethiopia</span>
            <br><hr>
            <span style="float:left;"><img src="../../icons/destination.png" width="20" height="20"> Address</span>
            <span style="float:right;"><?php echo $fetch['address']?></span>
            <br><hr>
            <span style="float:left; margin-left:-5px;"><img src="../../icons/mobile-256.png" width="35" height="28"> Mobile</span>
            <span style="float:right;"><?php echo $fetch['phone']?></span>
            <br><hr>
            <span style="float:left; margin-left:-5px;"><img src="../../icons/phone.png" width="23" height="23"> Office Phone</span>
            <span style="float:right;"><?php echo $fetch['office_phone']?></span>
            <br><hr>
            <span style="float:left;"><img src="../../icons/mail.png" width="20" height="20"> Email</span>
            <span style="float:right;"><?php echo $fetch['email']?></span>
            <br><hr>
            <span style="float:left;"><img src="../../icons/globe-480.png" width="20" height="20"> Website</span>
            <span style="float:right;"><a href="https://<?php echo $fetch['website']?>"><?php echo $fetch['website']?></a></span>
            <br><hr>
             <span style="float:left;"><img src="../../icons/globe-480.png" width="20" height="20"> Service hours</span>
            <span style="float:right;"><?php echo $fetch['open_from']?> --- 
            <?php echo $fetch['open_to']?></span>
            <br><hr>
            
           
        </div>
        
    </div>
    
    
    
 <?php
 if($lvl !='green'){
     ?>

    

        <span class="recentlyopened ml-1"><br>Similar nearby bussinesses</span>
    <div class="suggested-bussiness ml-1 recent">
    
    <?php
    
    $query = "SELECT * FROM bussiness WHERE category='$category' AND city='$city' AND acc_type!='none' AND name!='$name' ORDER BY avg_rating DESc,views DESC";
    $sql = mysqli_query($conn,$query);
    if(mysqli_num_rows($sql)>0){
        while($fetch=mysqli_fetch_array($sql)){
             ?><div class="car ml-1">
        <span class="bussiness-name"><?php echo $fetch['name'];?></span>
        <?php if($lvl=='blue'){?><img src="../../icons/blue.png" width="17" height="17"><?php }
         if($lvl=='green'){?><img src="../../icons/green.png" width="17" height="17"><?php }
        ?>
        
        <br>
        <?php 
            $id = $fetch['id']; 
            $query_rate = "SELECT avg(star) AS num FROM rating WHERE bussiness='$id'";
            $sql_rate = mysqli_query($conn,$query_rate);
            $fetch_rate = mysqli_fetch_array($sql_rate);
            $rate = round($fetch_rate['num'],1);

                           if($rate=='0'){
    ?><img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='1'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'1' && $rate<2){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='2'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'2' && $rate<3){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='3'){
    ?><img src="../../icons/full-star.png" width="18" height="18">
    <img src="../../icons/full-star.png" width="18" height="18">
    <img src="../../icons/full-star.png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'3' && $rate<4){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='4'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'4' & $rate<5){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <?php 
}
if($rate=='5'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <?php 
}

        ?><br>
        <img src="../../images/<?php echo $fetch['logo'];?>" width="150" height="150" style="border-radius:10px;" class="mt-1"><br>
        <img src="../../icons/destination.png" width="20" height="20"> <span style="font-size:14px; overflow:auto;"><?php echo $fetch['address']?></span><br>
        </div>
        <?php
    }}?></div><?php
    if(mysqli_num_rows($sql)<1){
       ?>
       <p class="text-center">No Suggestions found.</p>
       <?php
    }?>

    <div>
     <?php
 }?>
    
        <span class="recentlyopened ml-1"><br>Customer Reviews</span><hr>

        <div class="container">
            <?php
            $id = strval($_GET['id']);
            $query_bicha = "SELECT * FROM rating WHERE bussiness='$id'";
            $sql_bicha = mysqli_query($conn,$query_bicha);
            if(mysqli_num_rows($sql_bicha)<1){
                ?>
                <span>No reviews yet.</span><br>
                <?php
            }
            if(mysqli_num_rows($sql_bicha)>0){
                while($fetch_bicha=mysqli_fetch_array($sql_bicha)){
                    $giver = $fetch_bicha['giver'];
                    $rate = $fetch_bicha['star'];
                   $query_red = "SELECT * FROM normal WHERE id='$giver'";
                   $sql_red = mysqli_query($conn,$query_red);
                   $fetch_red = mysqli_fetch_array($sql_red);
                   $photo = $fetch_red['photo'];
                   $name = $fetch_red['full_name'];
                   $name = substr($name,0,1);
                  
                   ?>
                   <?php
                    if($photo==''){
                        ?>
                        <span 
                        style="text-transform:uppercase; color:white;
                               background-color:#00bfff; padding:17px; font-style:normal; clip-path:circle(); font-size:20px;">
                        <?php echo $name?></span>
                        <?php

                    }
                    if($photo!=''){
                        ?>
                        <a href="../../images/<?php echo $photo?>"><img src="../../images/<?php echo $photo?>" width="50" height="50" style="border-radius:100%;"></a>
                        <?php

                    }?>
                
                    
                    <span style="font-size:12px;"><?php echo $fetch_red['full_name']?></span>
                   <?php 
if($rate=='1'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='2'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='3'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='4'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='5'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <?php 
}                  
                   ?>
                    <br><br>
                    <span style="font-size:13px; font-style:italic;"><?php echo $fetch_bicha['comment']?></span>
                    <br><span style="font-size:10px;"><?php echo $fetch_bicha['date']?></span>
                    <hr>
                   <?php
                $id = $fetch_bicha['id'];
                $q_r = "SELECT * FROM replies WHERE rate_id=$id";
                $s_r = mysqli_query($conn,$q_r);
                $n_r = mysqli_num_rows($s_r);
                if($n_r>0){
                    while($f_r = mysqli_fetch_array($s_r)){
                        ?>
                        <div class="ml-4">
                           <a style=""><span style="font-size:12px;"><?php 
                           $name = $f_r['replied_by'];
                           $name = str_replace('_',' ',$name);
                           $name = str_replace('0','',$name);
                           $name = str_replace('1','',$name);
                           $name = str_replace('2','',$name);
                           $name = str_replace('3','',$name);
                           $name = str_replace('4','',$name);
                           $name = str_replace('5','',$name);
                           $name = str_replace('6','',$name);
                           $name = str_replace('7','',$name);
                           $name = str_replace('8','',$name);
                           $name = str_replace('9','',$name);
                           
                           echo $name?></span></a>&nbsp;<img src="../../icons/<?php echo $f_r['acc_type']?>" width="14" height="14"><br>
                           <span style="font-size:11px; font-style:italic;"><?php echo $f_r['reply_content']?></span><br> 
                           <span style="font-size:10px;"><?php echo $f_r['date']?></span>
                        </div><hr>
                        <?php
                    }
                }          
            }
            }
            ?>
        </div>
    </div>

    
     </div>
      <span class="recentlyopened ml-1"><br>Leave Review</span><hr>
     <div class="reviews container-fluid">
            <form method="post">
                <span>Number of stars</span>
                <br>
                
                <input type="radio" name="star" value="1">
                    <img src="../../icons/full-star.png" width="20" height="20"><br>
                <input type="radio" name="star" value="2">
                    <img src="../../icons/full-star.png" width="20" height="20">
                    <img src="../../icons/full-star.png" width="20" height="20"><br>
                <input type="radio" name="star" value="3" checked>
                    <img src="../../icons/full-star.png" width="20" height="20">
                    <img src="../../icons/full-star.png" width="20" height="20">
                    <img src="../../icons/full-star.png" width="20" height="20"><br>
                <input type="radio" name="star" value="4">
                    <img src="../../icons/full-star.png" width="20" height="20">
                    <img src="../../icons/full-star.png" width="20" height="20">
                    <img src="../../icons/full-star.png" width="20" height="20">
                    <img src="../../icons/full-star.png" width="20" height="20"><br>
                <input type="radio" name="star" value="5">
                    <img src="../../icons/full-star.png" width="20" height="20">
                    <img src="../../icons/full-star.png" width="20" height="20">
                    <img src="../../icons/full-star.png" width="20" height="20">
                    <img src="../../icons/full-star.png" width="20" height="20">
                    <img src="../../icons/full-star.png" width="20" height="20"><hr>
                    <span>Comment</span><br>
                    <textarea rows="4" maxlength="500" cols="50" class="form-control" id="phonefield" 
                                         placeholder="eg: They have an amazing hospitality and service... " name="comment" required></textarea><br>
               <p class="text-center"> <input type="submit" value="Post" name="review" class="btn mb-5" style="width:300px;"><br>
               <span id="days" style="font-size:11px; color:red;"></span></p>
            </form>
            <?php
           
                if(isset($_POST['review'])){
                     
                    if(isset($_SESSION['email']) && $_SESSION['type']=='user'){
               $today = date('20y-m-d');
               $em = $_SESSION['email'];
               $q = "SELECT id FROM normal WHERE email='$em'";
               $s = mysqli_query($conn,$q);
               $f = mysqli_fetch_array($s);
               $u = $f['id'];
                $biz = strval($_GET['id']);
               $q1 = "SELECT date FROM rating WHERE giver='$u' AND bussiness='$biz'";
               $s1 = mysqli_query($conn,$q1);
               $f1 = mysqli_fetch_array($s1);
               $interval=8;
               if(mysqli_num_rows($s1)>0){
                    $date = $f1['date'];
                $date = strtotime($f1['date']);
                $today = strtotime(date('20y-m-d'));
            $interval = ($today-$date)/60/60/24;
            if($interval<=7){
                echo "<script>alert('You cant give review more than once to same review within 7 days.');</script>";
            }

               }
              
              
            }

                    $type= $_SESSION['type'];

                    if(!isset($_SESSION['email'])){
                        echo "<script>alert('Please login first to give reviews.');</script>";
                        ?><script>location.href='login';</script><?php

                    }
                    if($type!='user'){
                        echo "<script>alert('Please login with user account give reviews.');</script>";
                        ?><script>location.href='login';</script><?php

                    }
                    if(isset($_SESSION['email']) && $type=='user' && $interval>7){
                        $email = $_SESSION['email'];
                        $star= $_POST['star'];
                        $comm = mysql_real_escape_string($_POST['comment']);
                        $comment = htmlSpecialChars($comm);

                        $query_giver = "SELECT id FROM normal WHERE email='$email'";
                        $sql_giver = mysqli_query($conn,$query_giver);
                        $fetch_giver = mysqli_fetch_array($sql_giver);
                        $idd = $fetch_giver['id'];

                        $qi = "SELECT COUNT(id) AS num FROM rating";
                        $si = mysqli_query($conn,$qi);
                        $fi = mysqli_fetch_array($si);
                        $event_id = $fi['num']+1;
        
                             
                        $query = "INSERT INTO rating VALUES($star,'$comment','$idd','$id',NOW(),$event_id)";
                        $sql = mysqli_query($conn,$query);

                        $query = "INSERT INTO feed (date_updated,feed_type,name,username,image,id) VALUES (NOW(),'review','$id','$id','',$event_id)";
                        $sql = mysqli_query($conn,$query);

                        if($sql){
                           
                            echo "<script>alert('Success.'); location.href='profile?id=$id'; </script>";
                        }

                    }
                   
                    
                }
            ?>
     </div>
      

         <div class="m animate-bottom" id="imm">
           <span style="float:right; margin-top:-15px; color:red; font-size:26px;" id="closee">&times;</span>
             <span style="color:black; text-transform:uppercase;"> <img src="../../icons/3239280.png" width="25" height="25"> Invite <?php echo $name?> to your friend.</span><br><br>

             <form method="post">
                 <input type="email" placeholder="Your friend's email" name="receiver" class="form-control" style="font-size:13px;" required autofocus><br>
                 <?php if(isset($_SESSION['email']) && $_SESSION['type']=='user'){
                     ?>
                     <button type="submit" class="btn form-control" name="invite"> <img src="../icons/Tg/PicsArt_02-18-12.18.18.png" width="30" height="30"></button>
                     <?php
                 }
                 else{
                     ?>
                     <button class="btn form-control" style="background-color:#00bfff;" disabled> <img src="../icons/Tg/PicsArt_02-18-12.18.18.png" width="30" height="30"></button>
                     <span style="color:black; font-size:12px">You need to <a href="login">login</a> to invite places to friends.</span>


                     <?php
                 }
                 
                 ?>
                  
             </form>
             <?php
                if(isset($_POST['invite'])){
                    if(isset($_SESSION['email']) && $_SESSION['type']=='user'){
                        $email = $_SESSION['email'];
                        $reciever = $_POST['receiver'];
                        $subject = "From yetale.com";

                        $txt = "$email invited you to check out $name. \n Check it out here https://www.yetale.com/user/profile?id='$id'";
                        $mail = mail($reciever,$subject,$txt);
                        if($mail){
                            echo "<script>alert('Yes');</script>";
                        }
                    }
                   
                }
             ?>
            
         </div>
         <script>
             var btn1 = document.getElementById('im');
             var modal1 = document.getElementById('imm');
             var close1 = document.getElementById('closee');
              var body1 = document.getElementById('none-modal');

              btn1.onclick = function(){
                 
            modal1.style.display = "block";
             modal1.scrollIntoview();
            body1.style.opacity='0.3';
        }
         close1.onclick = function(){
            modal1.style.display = "none";
            body1.style.opacity='1';
        }

         </script>
    
</body>
</html>


