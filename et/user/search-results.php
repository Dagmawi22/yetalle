
<?php
require("../db/connection.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>  Search Results | Yetale</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/mystyle.css">
    <style>
        a{
            color:#00bfff;
        }
         #load{
             margin:auto;
            animation: eyoha 6s infinite;
            -webkit-animation: eyoha 6s infinite;
        } 
        @keyframes eyoha{
           from{
                transform:rotate(360deg);
            }
            to{
                transform:rotate(-360deg);
            }
        }
        @-webkit-keyframes eyoha{
           from{
                transform:rotate(360deg);
            }
            to{
                transform:rotate(-360deg);
            }
        }
        .animate-bottom{
            position:relative;
            -webkit-animation-name:animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s;
        }
        
        @keyframes animatebottom{
             from {
                bottom:-100px; opacity:0;
            }
            to {
                bottom:0px; opacity:1;
            }
        }
        @-webkit-keyframes animatebottom{
             from {
                bottom:-100px; opacity:0;
            }
            to {
                bottom:0px; opacity:1;
            }
        }
        #loaded{
            display:none;
            
        }
      
    </style>
     <script>
         var myVar;
        function Load(){
            myVar = setTimeout(showPage,4000);
        }
        function showPage(){
            document.getElementById("loader").style.display="none";
             document.getElementById("loaded").style.display = "block";

        }
    </script>

</head>
<body onload="Load()">

<?php 
    $biz = $_GET['bussiness'];
    $ct = $_GET['city'];

    // $qc = "SELECT lat,lng,city FROM cities WHERE city='$ct'";
    // $sc = mysqli_query($conn,$qc);
    // $fc = mysqli_fetch_array($sc);
    // $city = $fc['city'];
    // $lat = $fc['lat'];
    // $lng = $fc['lng'];

    // $query = "SELECT lat,lng,name FROM bussiness WHERE category='$biz' AND city='$ct'";
    // $sql = mysqli_query($conn,$query);
    //   $arr_lat = array();
    //   $arr_lng = array();
    //   $arr_name = array();
    
    // if(mysqli_num_rows($sql)>0){
    //     while($fetch= mysqli_fetch_array($sql)){
    //         array_push($arr_lat,$fetch['lat']);
    //         array_push($arr_lng,$fetch['lng']);
    //         array_push($arr_name,$fetch['name']);
    //     }
    // }
    
?>


<div id="loader" style="position:absolute; top:50%; left:50%; transform:translate(-50%,-50%); display:flex; flex-direction:row;">

<span style="color:black; font-size:13px;"></span><span style="color:#00bfff; font-size:50px;" id="auto-write"></span>
<script>
    const text = document.getElementById("auto-write");
const prog = '...';

let idx=1;
setInterval(Write,250);

function Write(){
text.innerText = prog.slice(0,idx);
idx++;

if(idx>prog.length){
    idx=1;
}
}
</script>

</div>
<div id="loaded" class="">
   <a style="width:50px; height:50px;" class="btn" href="../HOME"><img src="../../icons/Tg/PicsArt_02-18-11.36.20.png" width="40" height="40" style="cursor:pointer;" class=""></a>
   <p class="text-center"> 
   <img src="../../icons/repl.png" width="300" height="300" style="">
   </p>
    
    <div style="width:100%; height:300;" id="map">
        
    </div>

    <div class="mt-5">
        <?php
        $city = strval($_GET['city']);
        $bussiness = strval($_GET['bussiness']);?>
        <p class="text-center">
            <span class="results" style="font-size:16px;">የፍለጋ ውጤቶች ለ <span style="color:#00bfff; font-size:20px;"><?php echo $bussiness?></span> በ <span style="color:#00bfff; font-size:20px;"><?php echo $city?></span> አቅራቢያ</span>
        </p>
    </div>
    <?php
     $query = "SELECT * FROM bussiness WHERE city='$city' AND category='$bussiness' AND acc_type !='none' ORDER BY acc_type DESC";
    $sql = mysqli_query($conn,$query);
    $suggested = mysqli_num_rows($sql); 

     $query1 = "SELECT * FROM bussiness WHERE city='$city' AND category='$bussiness'";
    $sql1 = mysqli_query($conn,$query1);
    $normal = mysqli_num_rows($sql1); 
    ?>

    <span class="recentlyopened" style="font-size:16px;">የሚመከሩ ውጤቶች (<?php echo $suggested?>) </span>
<div class="ml-1 recent" style="">
    <?php
   

    if(mysqli_num_rows($sql)>0){
        while($fetch = mysqli_fetch_array($sql)){
            ?>
         <div class="car ml-1">
        <span class="bussiness-name"><a href="profile?id=<?php echo $fetch['id']?>"><?php echo $fetch['name']; ?></a> <?php if($fetch['acc_type']=='green') echo "<img src='../../icons/green.png' width='15' height='15'>"; if($fetch['acc_type']=="<img src='../../icons/blue.png' width='15' height='15'>") echo "blue";?> </span><br>
        <?php
        $id = $fetch['id'];
        $query_rate = "SELECT avg(star) as num FROM rating WHERE bussiness='$id'";
        $sql_rate = mysqli_query($conn,$query_rate);
        $fetch_rate = mysqli_fetch_array($sql_rate);
        $rate = round($fetch_rate['num'],1);
        $query_rate1 = "SELECT count(id) as num FROM rating WHERE bussiness='$id'";
        $sql_rate1 = mysqli_query($conn,$query_rate1);
        $fetch_rate1 = mysqli_fetch_array($sql_rate1);
        $rate1 =$fetch_rate1['num'];
        ?><span style="font-size:11px;"><?php echo $rate1?> reviews</span><br><?php

                        if($rate=='0'){
    ?><img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='1'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'1' && $rate<2){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='2'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'2' && $rate<3){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../icons/half-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='3'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'3' && $rate<4){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='4'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'4' & $rate<5){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <?php 
}
if($rate=='5'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <?php 
}

?><br>
        <img src="../../images/<?php echo $fetch['logo']?>" width="150" height="150" style="border-radius:10px;" class="mt-1"><br>
        <span><img src="../../icons/destination.png" width="20" height="20"> <?php echo $fetch['address']?></span>
        </div>
            <?php

        }
    }?></div><?php
    if(mysqli_num_rows($sql)<1){
?>
<span><p class="text-center" style="font-size:16px;">የሚመከር ውጤት የለም</p></span>
<?php
    }

    ?>
    
  
    
<hr>

      <div>
      
        <?php
         if(isset($_GET['page']) && $_GET['page']!=1){
                 $start = $_GET['page']+2;
                 $page = $_GET['page'];
            }
            if(isset($_GET['page'])){
                 include('res-paging.php');
            }

           if(!isset($_GET['page'])){
               $start=0;
               $page =1;
           }
           
            $limit = 6;

             $query_last = "SELECT count(id) as num FROM bussiness WHERE category='$bussiness' AND city='$city'";
            $sql_last = mysqli_query($conn,$query_last);
            $fetch_last = mysqli_fetch_array($sql_last);
            $last = $fetch_last['num'];
            $maxpg = ceil($last/6);
           
            ?>
              <span class=""><strong class="recentlyopened" style="font-size:16px;">ሁሉም ውጤቶች (<?php echo $last?>)</strong></span><br>
             <?php if($last >0){
                 ?>
                 <p class="text-center mt-1"><span>ገፅ <span style="color:#00bfff; font-size:15px;"><?php echo $page?></span> / <span style="color:#00bfff; font-size:15px;"><?php echo $maxpg?></span></span></p>
                 <?php
             }

             if($last <1){
                 ?>
                 <p class="text-center mt-1" style="font-size:16px;">ይቅርታ ምንም ውጤት አልተገኘም።</p>
                 <?php
             }
             
             ?>
       
        <div class="container-fluid mb-3">
            <div class="row">
        <?php 
            $bussiness = strval($_GET['bussiness']);
            $city = strval($_GET['city']);

            $query = "SELECT * FROM bussiness WHERE category='$bussiness' AND city='$city' ORDER BY avg_rating DESC,views DESC limit $start,$limit";
            $sql = mysqli_query($conn,$query);

            while($fetch = mysqli_fetch_array($sql)){
                ?>
                          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4" style="box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2); border-radius:10px; width:100%;">
<div style="font-weight:700; font-size:22px;">
<a href="profile?id=<?php echo $fetch['id']?>"><?php echo $fetch['name'];?></a>
<?php if($fetch['acc_type']=='blue'){
    ?>
    <img src="../icons/../blue.png" width="20" height="20"></div>
    <?php

}
if($fetch['acc_type']=='green'){
    ?>
    <img src="../../icons/green.png" width="15" height="15"></div>
    <?php
}?>

<br>

<div style="font-size:13px;"><img src="../../icons/f041-256.png" width="20" height="25"> <?php echo $fetch['city'];?>, Ethiopia</div>
<?php 
$id = $fetch['id'];
$query_rating = "SELECT avg(star) as num FROM rating WHERE bussiness='$id'";
$sql_rating = mysqli_query($conn,$query_rating);
$fetch_rating = mysqli_fetch_array($sql_rating);
$rate = round($fetch_rating['num'],1);
 $query_rate1 = "SELECT count(id) as num FROM rating WHERE bussiness='$id'";
        $sql_rate1 = mysqli_query($conn,$query_rate1);
        $fetch_rate1 = mysqli_fetch_array($sql_rate1);
        $rate1 =$fetch_rate1['num'];
        ?><span style="font-size:12px;"><?php echo $rate1?> reviews</span><br><?php

                                if($rate=='0'){
    ?><img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='1'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'1' && $rate<2){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='2'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'2' && $rate<3){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../icons/half-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='3'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'3' && $rate<4){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='4'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'4' & $rate<5){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/half-star.png" width="20" height="20">
    <?php 
}
if($rate=='5'){
    ?><img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <img src="../../icons/full-star.png" width="20" height="20">
    <?php 
}

?><br><br>
<?php

$query_max = "SELECT max(star) as maximum FROM rating WHERE bussiness='$id'";
$sql_max = mysqli_query($conn,$query_max);
$fetch_max = mysqli_fetch_array($sql_max);
$max = $fetch_max['maximum'];
if($max){
$query_comment = "SELECT comment FROM rating WHERE star=$max";
$sql_comment = mysqli_query($conn,$query_comment);
$fetch_comment = mysqli_fetch_array($sql_comment);
$comment = $fetch_comment['comment'];
?><div style="color:black; font-size:13px;"><img src="../../images/default.png" style="" width="40" height="40"> <?php echo $comment?></div><?php
}



?>

<img src="../../images/<?php echo $fetch['photo']?>" width="100%" height="200" class="mb-2"><br>
<div style="font-size:13px;" class="mb-3"><img src="../../icons/destination.png" width="20" height="20"> <?php echo $fetch['address'];?></div>


</div>
</div>
           
                <?php
                
            }
        ?>

            
        </div>
       <div class="mt-1">
        <?php
                    if(isset($_GET['page'])){
                         $biz = $_GET['bussiness'];
                         $ct = $_GET['city'];
      
                        $page = strval($_GET['page']);
                        

                        if($page==1 || $page<$maxpg){
                            ?>
                            <div style="float:right;" class="mr-2"><a href="search-results.php?bussiness=<?php echo $biz?> & city=<?php echo $ct?> & page=<?php echo $page + 1?>" class="btn text-white" style="font-size:25px;">></a></div>
                            <?php
                        }

                        
                        if($page>=2){
                            ?>
                            <div style="float:left;" class="ml-2"><a href="search-results.php?bussiness=<?php echo $biz?> & city=<?php echo $ct?> & page=<?php echo $page - 1?>" class="btn text-white" style="font-size:25px;"><</a></div>
                            <?php
                        }
                    }
                    if(!isset($_GET['page'])){
                          if($maxpg>1){
                            ?>
                            <div style="float:right;" class="mr-2"><a href="search-results.php?bussiness=<?php echo $biz?> & city=<?php echo $ct?> & page=2" class="btn text-white" style="font-size:25px;">></a></div>
                            <?php
                        }
                    }
                ?>
                </div>
    </div>
     
<div style="margin-top:70px;">
<hr>
<footer class="mt-2">
    <p class="text-center" style="font-size:12px;">
    <span style="color:white; background-color:#00bfff; padding:5px; border-radius:10px;"><strong>Yetale.et</strong></span> 
    የንግድ ባለቤቶች በቀላሉ ምርትና አገልግሎታቸውን ከሚፈልጉ ደንበኞቻቸው ጋር በቀላሉ መገናኘት 
     እንዲችሉ እንዲሁም ደንበኞች የሚፈልጉትን በቀላሉ እንዲያገኙ የሚረዳ ድህረ-ገፅ ነዉ:: <br>
   
</p>
<p class="text-center" style="font-size:12px;">
ሊያገኙን ይፈልጋሉ?<br>
<a href="tel:+251 91 888 8225" style="text-decoration:none; font-size:12px; padding:5px; border-right:2px solid #00bfff; color:black;"><img class="myname" src="../../icons/phone.png" width="20" height="20"> +251 91 888 8225</a>
<a href="tel:+251 91 888 8233" style="font-size:12px; padding:5px; border-right:2px solid #00bfff; color:black;"><img class="myname" src="../../icons/phone.png" width="20" height="20"> +251 91 888 8233</a>

<a href="mailto:contact@yetale.com" style="font-size:12px; padding:3px; 
color:black;"><img class="myname" src="../../icons/envelope-square-256.png" width="20" height="20"> contact@yetale.com</a>
<br><br>

ንግድዎን ያስመዝግቡ:: <a href="register" class="btn-sm text-white" style="">አስመዝግብ</a> <br>
የሚፈልጉትን ያግኙ፣አስተያየትዎን ያጋሩ፣ለጓደኞችዎ ይጋብዙ፣
የንግድ መለያዎን ያዘምኑ:: <a href="login" class="btn-sm text-white">ግባ</a> <br>

 <span style="color:#00bfff; font-size:12px;">&copy; 20<?php echo date('y');?> የታለ</span><br> <br>
</p>
</footer>
                </div>
    </div>
      

    
    
</body>
</html>