
<?php
 session_unset();
session_start();
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>  Login | Yetale</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/mystyle.css?version=50">
    <style>
        .col-10{
            padding:20px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.3);
            margin-top:30px;
        }
         
    </style>
</head>
<body>

 <a href="../HOME"><img src="../../icons/yet.png" width="60" height="60" class="ml-1 mt-1" style=""></a>
 <div class="container-fluid">
     <div class="row">
         <div class="col-1">

         </div>
         <div class="col-10 form">

         <?php
require("../db/connection.php");


?>
              
    <form method="post">
        <?php
            if(isset($_POST['login'])){
                $email = $_POST['email'];
                $pass = md5($_POST['pass']);

                $query_check = "SELECT email,password FROM user WHERE email='$email' AND password='$pass' AND verified='yes'";
                $sql_check = mysqli_query($conn,$query_check);

                if(mysqli_num_rows($sql_check)<1){
                    ?> <div class="container-fluid">
        <div class="alert alert-info"><img src="../../icons/notify.png" width="20" height="20"> የማያገለግል ኢሜይል ወይም የይለፍ ቃል።</div>
        </div><?php
                }
                if(mysqli_num_rows($sql_check)>0){
                   
                    $query_type = "SELECT type,id FROM user WHERE email='$email'";
                    $sql_type = mysqli_query($conn,$query_type);
                    $fetch_type = mysqli_fetch_array($sql_type);
                    $status = $fetch_type['type'];
                    $id = $fetch_type['id'];

                    if($status=='user'){

                        $_SESSION['email']="$email";
                        $_SESSION['type']="user";

                         ?>
                         <script>
                              location.href='../HOME';
                         </script> 
       <?php
                    }
                     if($status=='bussiness'){
                          $_SESSION['email']="$email";
                        $_SESSION['type']="bussiness";

                         ?>
                         <script>
                              location.href='../bussiness/my-profile';
                              
                         </script> 
                         
                         <?php
                    }

                }
            }
            
        ?>
        
        <p class="text-center"><span style="font-size:2.1em; color:#00bfff;">ይግቡ</span><br>
    <span style="font-size:2.5em; color:#00bfff; font-size:13px; float:center;">ያግኙ፣ይገኙ::</span>
    </p>
    <span>ኢሜይል</span><br>
   
    <input style="" type="email" name="email" id="ver" placeholder="" value="" class="mb-2 container-fluid" autofocus required>
   
    <span class="">የይለፍ ቃል</span><br>
    <input type="password" name="pass" placeholder="" id="pass" class="mb-2 container-fluid" required>
    
    <br>
   
    <p class="text-center">
        <input type="submit" value="ግባ" name="login" class="btn btn-md" style="width:150px; height:35px;"><br>
            <span style="font-size:11px;">ገና አልተመዘገቡም? <a href="register" style="font-size:11px;">ይመዝገቡ</a></span><br>
    <span style="font-size:11px;">የይለፍ ቃልዎን ረሱ? <a href="forgot-password" style="font-size:11px;">እዚህ ይጫኑ</a></span>
</p>
    </form>
    


         </div>
     </div>
 </div>


    
</body>
</html>
