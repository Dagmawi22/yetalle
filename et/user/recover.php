<?php
session_start();
require("../db/connection.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>  Recover Password | Yetale</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/mystyle.css?version=50">
    <style>
        .container{
            padding:20px;
             box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.3);
        }
    </style>
   
</head>
<body>
    
    <div class="mt-1"><a href="../HOME" class="ml-2"><img src="../../icons/yet.png" width="60" height="60"></a></div>

    <div class="mt-5 container">
        <p class="text-center recentlyopened">
           የይለፍ ቃል ይቀይሩ።
        </p>
        <p class="text-center">በኢሜይልዎ የተላከውን ባለ 4 አሀዝ ቁጥር ያስገቡ።</p>
        <form method="post">
            <span>የመቀየሪያ ኮድ</span>
            <input type="text" maxlength="4" pattern="[0-9]{4}" name="code" id="code" class="form-control" autofocus required>
            <span>አዲስ የይለፍ ቃል</span>
            <input type="password" name="pass" id="pass" class="form-control" required>
            <span style="color:red; font-size:10px;" id="error-pass"></span>
            <div>አዲስ የይለፍ ቃል (ድጋሚ)</div>
            <input type="password" name="pass1" id="pass1" class="form-control" required>
            <span style="color:red; font-size:10px;" id="error-pass1"></span><br>
            
            <br>
            <script>
                var foc = document.getElementById("ver");
                foc.focus();
            </script>
            <p class="text-center"><input style="width:100px;" type="submit" name="change" class="btn" value="ቀይር"></p>
        </form>
    </div>
    
</body>
</html>

<?php
if(isset($_POST['change'])){
$code = $_POST['code'];
$email = strval($_GET['em']);
$q = "SELECT * FROM rec WHERE email='$email'";
$s = mysqli_query($conn,$q);
$f = mysqli_fetch_array($s);
$c = $f['cd'];


if($code==$c){
$pass = $_POST['pass'];
$pass_len = strlen($pass);
$pass1 = $_POST['pass1'];

if($pass_len<6){
    ?><script>
    document.getElementById("error-pass").innerHTML = "የይለፍ ቃል ከ6 ፊደላት ማነስ የለበትም።";
                var ez = document.getElementById("pass");
                ez.style.border = "1px solid red";
                ez.focus();
                </script>
    <?php
}
if($pass!=$pass1){
    ?><script>
    document.getElementById("error-pass").innerHTML = "የይለፍ ቃሎች አልተዛመዱም።";
    document.getElementById("error-pass1").innerHTML = "የይለፍ ቃሎች አልተዛመዱም።";
                var ez = document.getElementById("pass");
                ez.style.border = "1px solid red";
                ez.focus();
                var ez1 = document.getElementById("pass1");
                ez1.style.border = "1px solid red";
                
                </script>
    <?php
}
if($pass_len>=6 && $pass==$pass1){
    $pass = md5($pass);
    $query = "UPDATE user SET password='$pass' WHERE email='$email'";
    $sql = mysqli_query($conn,$query);
    $query = "DELETE FROM rec WHERE email='$email'";
    $sql = mysqli_query($conn,$query);
    ?>
   <script>
       alert('የይለፍ ቃልዎን ቀይረዋል። መግባት ይችላሉ።');
       location.href="login";
   </script>
    <?php
}
}
else{
    echo "<script>alert('የተሳሳተ ኮድ!');</script>";
}
}

?>