<?php
session_start();
require("../db/connection.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>  Recover Password | Yetale</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/mystyle.css?version=50">
    <style>
        .container{
            padding:20px;
             box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.3);
        }
    </style>
   
</head>
<body>
    
    <div class="mt-1"><a href="../HOME" class="ml-2"><img src="../../icons/yet.png" width="60" height="60"></a></div>

    <div class="mt-5 container">
        <p class="text-center recentlyopened">
            ኢሜይልዎን ያስገቡ።
        </p>
        <p class="text-center" style="color:#00bfff;">የይለፍ ቃል መቀየሪያ ኮድ እንዲደርስዎ ኢሜይልዎን ያስገቡ።</p>
        <form method="post">
            <span>ኢሜይል</span>
            <input type="email" name="email" id="ver" class="form-control" required>
            <span style="color:red; font-size:10px" id="error-ver"></span>
            <br>
            <script>
                var foc = document.getElementById("ver");
                foc.focus();
            </script>
            <p class="text-center"><input style="width:100px;" type="submit" name="get" class="btn" value="ኮድ አስልክ"></p>
        </form>
    </div>
    
</body>
</html>

<?php
if(isset($_POST['get'])){
$email = $_POST['email'];
$query = "SELECT count(email) AS num FROM user WHERE email='$email'";
$sql = mysqli_query($conn,$query);
$fetch = mysqli_fetch_array($sql);
$emails  = $fetch['num'];


if($emails==1){
    $code = mt_rand(1000,9999);
   
    ?>
    
    <?php
    $header = "From yetale.com";
                                    $subject = "Password Recovery";
                                    $message = "Your password recovery code is $code.\n\n This email was sent to you because of your sign up request. \n\n
                                    If you didn't request to sign up, simply ignore it.\n\n Yours\n The YETALE team\n https://www.yetale.com";
                                    mail($email,$subject,$message,$header);
                                    ?><script>location.href="recover?em=<?php echo $email?>";</script><?php
                                    $query = "DELETE FROM rec WHERE email='$email'";
                                    $sql = mysqli_query($conn,$query);
                                    $query = "INSERT INTO rec VALUES ('$email',$code)";
                                    $sql = mysqli_query($conn,$query);
                                    
}
else{
    
echo "<script>alert('ምንም መለያ አልተገኘም።')</script>";
}
}

?>