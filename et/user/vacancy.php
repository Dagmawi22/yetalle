
<?php
session_start();
require("../db/connection.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Vacancy | Yetale</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/mystyle.css?version=51">
    <style>
        .m{
            padding:20px;
            background-color:white;
            border-radius:5px;
            min-width:200px;
            position:absolute;
            top:50%;
            left:50%;
            transform:translate(-50%,-50%);
            display:none;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.5);
        }
        #mm{
            opacity:1;
        }

#close{
    color:red;
    float:right;
    font-size: 20px;
    margin-top:-20px;
    margin-right:-10px;

}
#close:hover ,
#close:focus{
    color:red;
    text-decoration: none;
    cursor: pointer;
}



        video{
            width:100%;
            height:400px;
            object-fit:cover;
            
        }
        .logo-anim{
            animation:1s oo infinite;
        }
        @keyframes oo {
            0%,50%{
                opacity:0.5;
                width:100;
                height:100;
            }
            100%{
                opacity:0.8;
                width:200;
                height:200;
            }
        }
        .option{
            
            color:black;
            
            

        }
        a{
            text-decoration:none;
        }
    </style>
  
</head>
<body>
    <a href="../HOME" style="position:absolute; top:0; left:0;"><img src="../icons/yet.png" width="60" height="60"></a>
    <div id="none-modal">
    <?php
    $id = strval($_GET['id']);

    $query = "SELECT * FROM bussiness WHERE id='$id'";
    $sql = mysqli_query($conn,$query);
    $fetch = mysqli_fetch_array($sql);
    $photo = $fetch['photo'];
    $category = $fetch['category'];
     $city = $fetch['city'];
     $logo = $fetch['logo'];
     $lvl = $fetch['acc_type'];
     $vid = $fetch['video'];
     $name = $fetch['name'];

    ?>
   
         <div class="image mb-2">
        <img src="../images/<?php echo $photo?>" width="100%" height="300">
    </div>
     <div class="nav" style="z-index:100;">
<a href="profile?id=<?php echo $id?>">Bio & Review</a>
<a href="images?id=<?php echo $id?>">Images</a>

<a href="event?id=<?php echo $id?>">Events</a>
<a href="" style="color:white; border-bottom:4px solid white;">Vacancy</a>
</div>
       
   

    <div class="info">
        <p class="text-center" id="info">
            <img src="../../images/<?php echo $logo?>" width="100" height="100" style="border-radius:100%;"><br> 
            <strong style="font-size:2em;"><?php echo $fetch['name']?></strong><br>
            <span style="font-size:14px;">Since <?php echo $fetch['date_founded']?></span><br>
            <span style="color:#00bfff; font-weight:700;">@<?php echo $fetch['id']?></span><br>
            <a href="map.php?id=<?php echo $id?>" class="btn" style="padding:4px 10px; text-decoration:none; background-color:#00bfff; color:white;">view on map</a><br>
            
           
        </div>

    <span class="recentlyopened">Vacancy</span>
<div class="ml-1 recent" style="">
    
<?php
    $today = date('20y-m-d');
    $query = "SELECT * FROM vacancy WHERE dadeline>='$today' AND id='$id' ORDER BY date_posted DESC";
    $sql = mysqli_query($conn,$query);
    
    if(mysqli_num_rows($sql)<1){
        ?>
        <p class="text-center">No unexpired vacancy found from <?php echo $name?>.</p>
        <?php
    }

    if(mysqli_num_rows($sql)>0){
        ?>
        <div class="container-fluid">
        <div class="row" style=""><?php
        while($fetch = mysqli_fetch_array($sql)){
            ?>
          
                 <?php
                    $biz= $fetch['bussiness'];
                    $query_biz = "SELECT logo,photo,name FROM bussiness WHERE id='$biz'";
                    $sql_biz = mysqli_query($conn,$query_biz);
                    $fetch_biz = mysqli_fetch_array($sql_biz);
                    $logo = $fetch_biz['logo'];
                    $photo = $fetch_biz['photo'];
                    $name = $fetch_biz['name']; 
                ?>
                  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4" style="box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2); border-bottom:1px solid #00bfff; padding:15px;">
                <div><a href=""><img src="../../images/<?php echo $logo?>" width="50" height="50" style="border-radius:100%;"> &nbsp;
                <span style="font-weight:900; font-size:16px;"><?php echo $name?></a> <span style="font-size:10px"><?php echo $fetch['date_posted']?></span></span> 
       </div>
                <span><span style="font-weight:900; font-size:16px;"> Position</span><br> <?php echo $fetch['position']?> (<?php echo $fetch['quan']?>)</span><br><br>
                               
                <span><span style="font-weight:900; font-size:16px;">Application Deadline</span><br> <?php echo $fetch['dadeline']?></span><br>
                <a href="view-vacancy.php?id=<?php echo $fetch['id']?>" class="btn btn-sm mt-1 text-white">Read more</a><br>
                
            </div>
            <?php
        }
       
    }
?></div></div>
    
     
    
     
</div>
<hr>


     
     </div>
     
    
</body>
</html>