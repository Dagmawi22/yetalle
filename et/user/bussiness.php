
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/mystyle.css">
    <style>
       
    </style>
</head>
<body>
    <img src="../images/login-bg.jpg" width="100%" height="300"><br>
    <div class="profile-info">
        <p class="text-center bussiness-name" style="font-size:25px;">
        <br><img src="../icons/globe-480.png" width="100" height="100"><br>
            Darik Coffee <img src="../icons/verified.png" width="25" height="25"><br>
            <span style="color:#00bfff; font-size:18px;">@darik22</span><br>
            <img src="../icons/filled.png" width="20" height="20">
            <img src="../icons/filled.png" width="20" height="20">
            <img src="../icons/filled.png" width="20" height="20">
            <img src="../icons/filled.png" width="20" height="20">
            <img src="../icons/filled.png" width="20" height="20"><br>
           <div class="txt"> 
               <p class="text-center"><span>45 reviews</span>
            <br>
            <img src="../icons/destination.png" width="20" height="20"> <span>Adama</span><br>
            <img src="../icons/destination.png" width="20" height="20"> <span>The best quality coffee in town</span><br>
             <img src="../icons/destination.png" width="20" height="20"> <span>On the way from Wonji Mazoriya to Franco infront of Adama Ras hotel</span><br>
             <img src="../icons/wwwlogo.png" width="20" height="20"> <span><a href="">www.zella-et.com</a></span><br>
              <img src="../icons/wwwlogo.png" width="20" height="20"> <span>dagtt316@gmail.com</span><br>
              <img src="../icons/wwwlogo.png" width="20" height="20"> <span>+251918888225</span><br>
              <img src="../icons/wwwlogo.png" width="20" height="20"> <span>Open from</span> <span>07:00am</span> To <span>08:00pm</span><br>
                </p></div>
            </p>
    </div>
    <p class="text-center mt-4"><span class="recentlyopened">Similar nearby bussinesses </span></p><br>
    <div class="suggested-bussiness">
       
        <div class="sug">
            <span>Lovely Coffee</span><br>
            <img src="../images/login-bg.jpg" width="150" height="150">
        </div>
         <div class="sug">
            <span>Rose Coffee</span><br>
            <img src="../images/login-bg.jpg" width="150" height="150">
        </div>
         <div class="sug">
            <span>Super Coffee</span><br>
            <img src="../images/login-bg.jpg" width="150" height="150">
        </div>

    </div>

    <p class="text-center mt-4"><span class="recentlyopened">Customers say these about <span>Darik Coffee</span></span></p><br>

    <div class="container review">
        <div class="comment">
             <span class="ml-5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;21/02/2021 07:38</span><br>
            <img src="../images/login-bg.jpg" class="circle" width="70" height="70"> <span>Dagmawi Teka</span> &nbsp; 
            <span>
                <img src="../icons/filled.png" width="15" height="15">
                <img src="../icons/filled.png" width="15" height="15">
                <img src="../icons/filled.png" width="15" height="15">
                <img src="../icons/filled.png" width="15" height="15">
                <img src="../icons/filled.png" width="15" height="15">
            </span><br>
            <span>
                Andande ewnetun lemenager kehone bzum mashkabet mnamn aymechegnm gn yaw yhegnaw leyet slalebgn new yemr wedjewalew 
                akebabelachew ebd neger alew yemr hulum ymokrew 
            </span><hr>
           
        </div>

         <div class="comment">
             <span class="ml-5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;21/02/2021 07:38</span><br>
            <img src="../images/login-bg.jpg" class="circle" width="70" height="70"> <span>Dagmawi Teka</span> &nbsp; 
            <span>
                <img src="../icons/filled.png" width="15" height="15">
                <img src="../icons/filled.png" width="15" height="15">
                <img src="../icons/filled.png" width="15" height="15">
                <img src="../icons/filled.png" width="15" height="15">
                <img src="../icons/filled.png" width="15" height="15">
            </span><br>
            <span>
                Andande ewnetun lemenager kehone bzum mashkabet mnamn aymechegnm gn yaw yhegnaw leyet slalebgn new yemr wedjewalew 
                akebabelachew ebd neger alew yemr hulum ymokrew 
            </span><hr>
           
        </div>
    </div>

  <p class="text-center"><span class="recentlyopened">Give Review</span></span></p><br>

</body>
</html>