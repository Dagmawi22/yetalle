<?php
session_start();
require("../db/connection.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>  Verify Email | Yetale</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/mystyle.css?version=50">
    <style>
        .container{
            padding:20px;
             box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.3);
        }
    </style>
   
</head>
<body>
    
    <div class="mt-1"><a href="../HOME" class="ml-2"><img src="../../icons/yet.png" width="60" height="60"></a></div>

    <div class="mt-5 container">
        <p class="text-center recentlyopened">
            ኢሜይልዎን ያረጋግጡ።
        </p>
        <p class="text-center">ወደ ኢሜይልዎ የተላከውን ባለ 4 አሀዝ ቁጥር ያስገቡ።</p>
        <form method="post">
            <input type="text" maxlength="4" pattern="[0-9]{4}" name="verification" id="ver" class="form-control" focused required>
            <span style="color:red; font-size:10px" id="error-ver"></span>
            <br>
            <script>
                var foc = document.getElementById("ver");
                foc.focus();
            </script>
            <p class="text-center"><input style="width:100px;" type="submit" name="verify" class="btn" value="አረጋግጥ"></p>
        </form>
    </div>
    
</body>
</html>

<?php
if(isset($_POST['verify'])){
$num = $_POST['verification'];
$email = strval($_GET['em']);
$query = "SELECT type,verified FROM user WHERE email='$email'";
$sql = mysqli_query($conn,$query);
$fetch = mysqli_fetch_array($sql);
$type  = $fetch['type'];
$ver = $fetch['verified'];

if($num==$ver){
  
       $query = "UPDATE user SET verified='yes' WHERE email='$email'";
       $sql = mysqli_query($conn,$query);

       $q = "SELECT * FROM bussiness WHERE email='$email'";
       $s = mysqli_query($conn,$q);
       $f = mysqli_fetch_array($s);
       $id = $f['id'];
       $name = $f['name'];
    if($type=='bussiness'){
        $query = "INSERT INTO feed (date_updated,feed_type,name,username,image,id) VALUES (NOW(),'join','$name','$id','','')";
        $sql = mysqli_query($conn,$query);
    }
       

    echo "<script>alert('አረጋግጠዋል። አሁን መግባት ይችላሉ።'); location.href='login';</script>";
  
    
    
   
}
if($num!=$ver){
     ?>
                                    <script>
                document.getElementById("error-ver").innerHTML = "ተሳስተዋል። እንደገና ይሞክሩ።";
                var ez = document.getElementById("ver");
                ez.style.border = "1px solid red";
                ez.focus();
                </script>
                                    <?php
}

}

?>