
<?php
session_start();
require("../db/connection.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Images | Yetale</title>
    <link rel="stylesheet" href="../../css/bootstrap.css">
    <link rel="stylesheet" href="../../css/mystyle.css?version=51">
    <style>
        .m{
            padding:20px;
            background-color:white;
            border-radius:5px;
            min-width:200px;
            position:absolute;
            top:50%;
            left:50%;
            transform:translate(-50%,-50%);
            display:none;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.5);
        }
        #mm{
            opacity:1;
        }
       

#close{
    color:red;
    float:right;
    font-size: 20px;
    margin-top:-20px;
    margin-right:-10px;

}
#close:hover ,
#close:focus{
    color:red;
    text-decoration: none;
    cursor: pointer;
}



        video{
            width:100%;
            height:400px;
            object-fit:cover;
            
        }
        .logo-anim{
            animation:1s oo infinite;
        }
        @keyframes oo {
            0%,50%{
                opacity:0.5;
                width:100;
                height:100;
            }
            100%{
                opacity:0.8;
                width:200;
                height:200;
            }
        }
        .option{
            
            color:black;
            
            

        }
        a{
            text-decoration:none;
        }
         
    </style>
  
</head>
<body>
    
    <div id="none-modal">
    <?php
    $id = strval($_GET['id']);


    $query = "SELECT * FROM bussiness WHERE id='$id'";
    $sql = mysqli_query($conn,$query);
    $fetch = mysqli_fetch_array($sql);
    $photo = $fetch['photo'];
    $category = $fetch['category'];
     $city = $fetch['city'];
     $logo = $fetch['logo'];
     $lvl = $fetch['acc_type'];
     $vid = $fetch['video'];
     $name = $fetch['name'];
     $email = $fetch['email'];

    ?>
    <a href="../HOME" style="position:absolute; top:0; left:0;"><img src="../../icons/yet.png" width="60" height="60"></a>
    
    
         <div class="image mb-2">
        <img src="../../images/<?php echo $photo?>" width="100%" height="300">
    </div>
     
       

           
    <div class="info">
        <p class="text-center" id="info">
            <img src="../../images/<?php echo $logo?>" width="100" height="100" style="border-radius:100%;"><br> 
            <strong style="font-size:2em;"><?php echo $fetch['name']?></strong><br>
            <span style="font-size:14px;">Since <?php echo $fetch['date_founded']?></span><br>
            <span style="color:#00bfff; font-weight:700;">@<?php echo $fetch['id']?></span><br>
            <a href="map.php?id=<?php echo $id?>" class="btn" style="padding:4px 10px; text-decoration:none; background-color:#00bfff; color:white;">view on map</a><br>
            
           
       
        
        
    </div>
     <?php
        $query = "SELECT * FROM pics WHERE email='$email'";
        $sql = mysqli_query($conn,$query);
        
        
        
     ?>

     <div class="container-fluid mb-5">
         <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
                    <img src="../../images/<?php echo $fetch['logo']?>" width="100%" height="200">
                    </div>
           <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
                    <img src="../../images/<?php echo $fetch['photo']?>" width="100%" height="200">
                    </div>
                  
                  <?php
                  if(mysqli_num_rows($sql)>0){
                  while($fetch=mysqli_fetch_array($sql)){
                      ?>
                         <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
                    <img src="../../images/<?php echo $fetch['photo']?>" width="100%" height="200">
                    </div>
                      <?php

                  }}
                    ?>
            
    
     </div>
     </div>
     
     <br>
    
    


    

    
     </div>
      <div class="nav" >
<a href="profile?id=<?php echo $id?>" style="font-size:11px; padding:3px;">Bio & Review</a>
<a href="" style="color:white; border-bottom:4px solid white; font-size:11px; padding:0;">Images</a>
<a href="event?id=<?php echo $id?>" style="font-size:11px; padding:3px;">Events</a>
<a href="vacancy?id=<?php echo $id?>" style="font-size:11px; padding:3px;">Vacancy</a>
</div>
     
    
</body>
</html>