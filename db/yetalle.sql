-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 12, 2022 at 11:44 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `yetalle`
--

-- --------------------------------------------------------

--
-- Table structure for table `bussiness`
--

CREATE TABLE IF NOT EXISTS `bussiness` (
  `invitor` varchar(8) NOT NULL,
  `password` varchar(30) NOT NULL,
  `acc_type` varchar(30) NOT NULL,
  `expiry` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `office_phone` varchar(10) NOT NULL,
  `lat` varchar(50) NOT NULL,
  `lng` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `address` varchar(200) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `category` varchar(100) NOT NULL,
  `category_type` varchar(15) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `id` varchar(100) NOT NULL,
  `name` varchar(200) NOT NULL,
  `date_founded` date NOT NULL,
  `open_from` time NOT NULL,
  `open_to` time NOT NULL,
  `video` varchar(100) NOT NULL,
  `emp_size` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `avg_rating` float NOT NULL,
  `date_joined` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bussiness`
--

INSERT INTO `bussiness` (`invitor`, `password`, `acc_type`, `expiry`, `email`, `website`, `phone`, `office_phone`, `lat`, `lng`, `description`, `address`, `logo`, `city`, `category`, `category_type`, `photo`, `id`, `name`, `date_founded`, `open_from`, `open_to`, `video`, `emp_size`, `views`, `avg_rating`, `date_joined`) VALUES
('', '5f4dcc3b5aa765d61d8327deb882cf', 'green', '2023-04-19', 'ab@y.com', '', '0910101010', '', '', '', 'Best spot to rest in the town.', 'Mexico near Hiber Tower', 'gisduiugh2.jpg', 'Addis Ababa', 'Hotels', 'primary', 'ugsiugdiddisggig6.jpg', 'AB_Hotel_2', 'AB Hotel', '2000-11-02', '00:00:00', '00:00:00', '', 0, 70, 5, '2022-03-25'),
('', '5f4dcc3b5aa765d61d8327deb882cf', 'none', '0000-00-00', 'cd@y.com', '', '', '', '', '', '', '', 'dsguigiuexpand-arrow.png', 'Addis Ababa', 'Hotels', 'primary', 'dgugisuigigiusudh1.jpg', 'CD_Hotel_1', 'CD Hotel', '0000-00-00', '00:00:00', '00:00:00', '', 0, 18, 0, '2022-03-25'),
('', '5f4dcc3b5aa765d61d8327deb882cf', 'none', '2023-03-31', 'ef@y.com', '', '0911111111', '', '', '', 'We strive for your harmony.', 'Bole Airport Road', 'udugigish4.jpg', 'Addis Ababa', 'Hotels', 'primary', 'ggdiusuic1.jpg', 'EF_Hotel_2', 'EF Hotel', '2013-07-05', '00:00:00', '00:00:00', '', 0, 25, 0, '2022-03-25'),
('', '5f4dcc3b5aa765d61d8327deb882cf', 'none', '0000-00-00', 'xy@y.com', '', '', '', '', '', '', '', '', 'Addis Ababa', 'Hotels', 'primary', '', 'XY_Hotel_1', 'XY Hotel', '0000-00-00', '00:00:00', '00:00:00', '', 0, 0, 0, '2022-03-28'),
('', '5f4dcc3b5aa765d61d8327deb882cf', 'none', '0000-00-00', 'k@y.com', '', '', '', '', '', '', 'sample address', '', 'Addis Ababa', 'Hotels', 'primary', 'iugidsguc2.jpg', 'K_Hotel_1', 'K Hotel', '0000-00-00', '00:00:00', '00:00:00', '', 0, 6, 0, '2022-03-29'),
('', '5f4dcc3b5aa765d61d8327deb882cf', 'none', '0000-00-00', 'ioiyoyoyo@uguigiu', '', '0967557754', '', '', '', '', '', '', 'Addis Alem/Ejersa', 'Agricultural Services & Products', 'primary', '', 'iyoiyoi_1', 'iyoiyoi', '0000-00-00', '00:00:00', '00:00:00', '', 0, 0, 0, '2022-03-30'),
('', '5f4dcc3b5aa765d61d8327deb882cf', 'green', '2023-04-19', 'ab@y.com', '', '0910101010', '', '', '', 'Best spot to rest in the town.', 'Mexico near Hiber Tower', 'gisduiugh2.jpg', 'Addis Ababa', 'Hotels', 'primary', 'ugsiugdiddisggig6.jpg', 'AB_Hotel_2', 'AB Hotel', '2000-11-02', '00:00:00', '00:00:00', '', 0, 70, 5, '2022-03-25'),
('', '5f4dcc3b5aa765d61d8327deb882cf', 'none', '0000-00-00', 'cd@y.com', '', '', '', '', '', '', '', 'dsguigiuexpand-arrow.png', 'Addis Ababa', 'Hotels', 'primary', 'dgugisuigigiusudh1.jpg', 'CD_Hotel_1', 'CD Hotel', '0000-00-00', '00:00:00', '00:00:00', '', 0, 18, 0, '2022-03-25'),
('', '5f4dcc3b5aa765d61d8327deb882cf', 'none', '2023-03-31', 'ef@y.com', '', '0911111111', '', '', '', 'We strive for your harmony.', 'Bole Airport Road', 'udugigish4.jpg', 'Addis Ababa', 'Hotels', 'primary', 'ggdiusuic1.jpg', 'EF_Hotel_2', 'EF Hotel', '2013-07-05', '00:00:00', '00:00:00', '', 0, 25, 0, '2022-03-25'),
('', '5f4dcc3b5aa765d61d8327deb882cf', 'none', '0000-00-00', 'xy@y.com', '', '', '', '', '', '', '', '', 'Addis Ababa', 'Hotels', 'primary', '', 'XY_Hotel_1', 'XY Hotel', '0000-00-00', '00:00:00', '00:00:00', '', 0, 0, 0, '2022-03-28'),
('', '5f4dcc3b5aa765d61d8327deb882cf', 'none', '0000-00-00', 'k@y.com', '', '', '', '', '', '', 'sample address', '', 'Addis Ababa', 'Hotels', 'primary', 'iugidsguc2.jpg', 'K_Hotel_1', 'K Hotel', '0000-00-00', '00:00:00', '00:00:00', '', 0, 6, 0, '2022-03-29'),
('', '5f4dcc3b5aa765d61d8327deb882cf', 'none', '0000-00-00', 'ioiyoyoyo@uguigiu', '', '0967557754', '', '', '', '', '', '', 'Addis Alem/Ejersa', 'Agricultural Services & Products', 'primary', '', 'iyoiyoi_1', 'iyoiyoi', '0000-00-00', '00:00:00', '00:00:00', '', 0, 0, 0, '2022-03-30');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `city` varchar(30) NOT NULL,
  `lat` varchar(30) NOT NULL,
  `lng` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL,
  `bussiness` varchar(100) NOT NULL,
  `caption` varchar(150) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `place` varchar(150) NOT NULL,
  `starts_on` date NOT NULL,
  `ends_on` date NOT NULL,
  `tym` time NOT NULL,
  `days` int(11) NOT NULL,
  `date_posted` datetime NOT NULL,
  `ticket` varchar(51) NOT NULL,
  `description` varchar(600) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `bussiness`, `caption`, `photo`, `place`, `starts_on`, `ends_on`, `tym`, `days`, `date_posted`, `ticket`, `description`) VALUES
(1, 'AB_Hotel_2', 'f f wyetfe', 'yfffuuyfuc1.jpg', 'gf gfuyg', '2022-08-24', '2022-08-27', '03:09:00', 3, '2022-08-02 01:52:00', 'uhiwuegiur', ''),
(1, 'AB_Hotel_2', 'f f wyetfe', 'yfffuuyfuc1.jpg', 'gf gfuyg', '2022-08-24', '2022-08-27', '03:09:00', 3, '2022-08-02 01:52:00', 'uhiwuegiur', ''),
(3, 'AB_Hotel_2', 'jgfjhsdg', 'uffyfyuuf', 'gsdg', '2022-08-16', '2022-08-20', '09:08:00', 4, '2022-08-09 08:40:32', '', 'yuaygdyfyu'),
(4, 'AB_Hotel_2', 'gfsg', 'yfuffuufy', 'giuegf', '2022-08-30', '2022-09-05', '09:09:00', 6, '2022-08-09 08:41:36', '', ' fgfiugiufgfiu fgiufgie rgiugriufgiu fiegfiu gfuigfui w guf gwfgiuw gfiugf iuiu iugiuwg fiugfiugiu fuigfegiuf ifiufg iugfiugiugiu giweg g ifgiwugfiu e fiuegfiuug f');

-- --------------------------------------------------------

--
-- Table structure for table `feed`
--

CREATE TABLE IF NOT EXISTS `feed` (
  `date_updated` datetime NOT NULL,
  `feed_type` varchar(30) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `image` varchar(100) NOT NULL,
  `feed_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`feed_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=70 ;

--
-- Dumping data for table `feed`
--

INSERT INTO `feed` (`date_updated`, `feed_type`, `name`, `username`, `image`, `feed_id`, `id`) VALUES
('2022-03-25 08:54:53', 'join', 'CD Hotel', 'CD_Hotel_1', '', 56, 0),
('2022-03-25 09:04:17', 'join', 'EF Hotel', 'EF_Hotel_1', '', 57, 0),
('2022-03-25 09:29:22', 'review', 'AB_Hotel_2', 'AB_Hotel_2', '', 58, 1),
('2022-03-28 08:46:19', 'photo', 'ab@y.com', 'ab@y.com', 'idsggdigc4.jpg', 59, 1),
('2022-03-28 08:46:31', 'photo', 'ab@y.com', 'ab@y.com', 'dgigsgidc5.jpg', 60, 2),
('2022-03-28 08:46:47', 'photo', 'ab@y.com', 'ab@y.com', 'gddiisggh2.jpg', 61, 3),
('2022-03-28 08:46:57', 'photo', 'ab@y.com', 'ab@y.com', 'dgiggidsh5.jpg', 62, 4),
('2022-03-29 10:36:20', 'join', 'K Hotel', 'K_Hotel_1', '', 63, 0),
('2022-06-07 15:59:01', 'job', 'CD_Hotel_1', 'CD_Hotel_1', '', 64, 1),
('2022-08-02 14:44:54', 'job', 'AB_Hotel_2', 'AB_Hotel_2', '', 65, 1),
('2022-08-02 14:52:00', 'event', 'AB Hotel', 'AB_Hotel_2', 'yfffuuyfuc1.jpg', 66, 1),
('2022-08-02 15:27:47', 'job', 'AB_Hotel_2', 'AB_Hotel_2', '', 67, 2),
('2022-08-09 09:40:32', 'event', 'AB Hotel', 'AB_Hotel_2', 'uffyfyuuf', 68, 3),
('2022-08-09 09:41:36', 'event', 'AB Hotel', 'AB_Hotel_2', 'yfuffuufy', 69, 4);

-- --------------------------------------------------------

--
-- Table structure for table `interested`
--

CREATE TABLE IF NOT EXISTS `interested` (
  `email` varchar(100) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `interested`
--

INSERT INTO `interested` (`email`, `id`) VALUES
('abcd@y.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `invitation`
--

CREATE TABLE IF NOT EXISTS `invitation` (
  `phone` varchar(10) NOT NULL,
  `invitor_code` varchar(16) NOT NULL,
  `bonus_amount` float NOT NULL,
  PRIMARY KEY (`invitor_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invitation`
--

INSERT INTO `invitation` (`phone`, `invitor_code`, `bonus_amount`) VALUES
('0918888225', 'yduydfeuyfy', 200);

-- --------------------------------------------------------

--
-- Table structure for table `job_application`
--

CREATE TABLE IF NOT EXISTS `job_application` (
  `email` varchar(50) NOT NULL,
  `job_id` int(11) NOT NULL,
  `application_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_application`
--

INSERT INTO `job_application` (`email`, `job_id`, `application_time`) VALUES
('abcd@y.com', 1, '2022-08-02 15:14:38'),
('abcd@y.com', 2, '2022-08-02 15:28:48');

-- --------------------------------------------------------

--
-- Table structure for table `normal`
--

CREATE TABLE IF NOT EXISTS `normal` (
  `id` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(100) NOT NULL,
  `full_name` varchar(150) NOT NULL,
  `opened_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `normal`
--

INSERT INTO `normal` (`id`, `photo`, `email`, `password`, `full_name`, `opened_at`) VALUES
('ABCD_1', 'igvuahgyubdefault.png', 'abcd@y.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'ABCD', '2022-03-25'),
('Dag_Sha_1', 'yvuiubahgg1.jpg', 'abc@y.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'Dag Sha', '2022-08-02');

-- --------------------------------------------------------

--
-- Table structure for table `pics`
--

CREATE TABLE IF NOT EXISTS `pics` (
  `email` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pics`
--

INSERT INTO `pics` (`email`, `photo`, `id`) VALUES
('ab@y.com', 'idsggdigc4.jpg', 1),
('ab@y.com', 'dgigsgidc5.jpg', 2),
('ab@y.com', 'gddiisggh2.jpg', 3),
('ab@y.com', 'dgiggidsh5.jpg', 4),
('cd@y.com', 'dgiggdis3.jpg', 5),
('cd@y.com', 'digggsidc3.jpg', 6),
('cd@y.com', 'gggsididh4.jpg', 7),
('cd@y.com', 'gsgidgidh4.jpg', 8),
('cd@y.com', 'dggigdish5.jpg', 9);

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE IF NOT EXISTS `rating` (
  `star` float NOT NULL,
  `comment` text NOT NULL,
  `giver` varchar(100) NOT NULL,
  `bussiness` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`star`, `comment`, `giver`, `bussiness`, `date`, `id`) VALUES
(5, 'It''s really amazing. I''ve had an unforgettable experience there.', 'ABCD_1', 'AB_Hotel_2', '2022-03-25 09:29:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rec`
--

CREATE TABLE IF NOT EXISTS `rec` (
  `email` varchar(100) NOT NULL,
  `cd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rec`
--

INSERT INTO `rec` (`email`, `cd`) VALUES
('ab@y.com', 5407);

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

CREATE TABLE IF NOT EXISTS `replies` (
  `rate_id` int(11) NOT NULL,
  `replied_to` varchar(50) NOT NULL,
  `replied_by` varchar(50) NOT NULL,
  `acc_type` varchar(15) NOT NULL,
  `date` datetime NOT NULL,
  `reply_content` text NOT NULL,
  `reply_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`reply_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `replies`
--

INSERT INTO `replies` (`rate_id`, `replied_to`, `replied_by`, `acc_type`, `date`, `reply_content`, `reply_id`) VALUES
(1, 'ABCD_1', 'AB_Hotel_2', 'green.png', '2022-03-31 11:17:06', 'Thanks', 2),
(1, 'ABCD_1', 'AB_Hotel_2', 'green.png', '2022-03-31 11:18:46', 'yme abo', 3),
(1, 'ABCD_1', 'AB_Hotel_2', 'green.png', '2022-06-21 22:20:20', 'giui', 4),
(1, 'ABCD_1', 'AB_Hotel_2', 'green.png', '2022-08-03 09:37:31', 'hv', 5);

-- --------------------------------------------------------

--
-- Table structure for table `reported`
--

CREATE TABLE IF NOT EXISTS `reported` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reported`
--

INSERT INTO `reported` (`id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `verified` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `type`, `verified`) VALUES
('', 'ab@y.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'bussiness', 'yes'),
('', 'cd@y.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'bussiness', 'yes'),
('', 'ef@y.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'bussiness', 'yes'),
('ABCD_1', 'abcd@y.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'user', 'yes'),
('', 'xy@y.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'bussiness', '2065'),
('', 'k@y.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'bussiness', 'yes'),
('', 'ioiyoyoyo@uguigiu', '5f4dcc3b5aa765d61d8327deb882cf99', 'bussiness', '1786'),
('Dag_Sha_1', 'abc@y.com', '5f4dcc3b5aa765d61d8327deb882cf99', 'user', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `vacancy`
--

CREATE TABLE IF NOT EXISTS `vacancy` (
  `bussiness` varchar(100) NOT NULL,
  `position` varchar(200) NOT NULL,
  `quan` int(11) NOT NULL,
  `tasks` text NOT NULL,
  `req` text NOT NULL,
  `id` int(11) NOT NULL,
  `date_posted` datetime NOT NULL,
  `dadeline` datetime NOT NULL,
  `place_of_work` varchar(200) NOT NULL,
  `minex` int(11) NOT NULL,
  `maxex` int(11) NOT NULL,
  `app_link` text NOT NULL,
  `app_email` varchar(100) NOT NULL,
  `people_applied` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vacancy`
--

INSERT INTO `vacancy` (`bussiness`, `position`, `quan`, `tasks`, `req`, `id`, `date_posted`, `dadeline`, `place_of_work`, `minex`, `maxex`, `app_link`, `app_email`, `people_applied`) VALUES
('AB_Hotel_2', 'fydfd', 2, 'dgh sfhiuh iuf hfduiif   khi\r<br>fuhiugfivg\r<br>fhiudfgiuf', 'hfu dgfgdgdfy', 1, '2022-08-02 01:44:54', '2022-08-16 00:00:00', 'uhfiug vigifugviufd', 2, 4, 'bit.ly/uhfsiug', 'gufy@hg', 0),
('AB_Hotel_2', 'dgfuysdgu', 54, 'dhf fydsg', 'vvii', 2, '2022-08-02 02:27:47', '2022-08-25 00:00:00', 'gigi', 6, 10, 'hjf fuyf', 'gig@uguig', 0);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal`
--

CREATE TABLE IF NOT EXISTS `withdrawal` (
  `phone` varchar(10) NOT NULL,
  `balance` float NOT NULL,
  PRIMARY KEY (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `withdrawal`
--

INSERT INTO `withdrawal` (`phone`, `balance`) VALUES
('0918888225', 200);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
