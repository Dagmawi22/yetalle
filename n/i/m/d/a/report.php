<?php
require('connection.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Report</title>
    <link rel="stylesheet" href="bootstrap.css">
    <link rel="stylesheet" href="mystyle.css">
</head>
<body>
   
    <div class="container">
         <?php
         $query = "SELECT * FROM reported";
         $sql = mysqli_query($conn,$query);
         while($fetch = mysqli_fetch_array($sql)){
             $id = $fetch['id'];

              $q = "SELECT * FROM rating WHERE id=$id";
              $s = mysqli_query($conn,$q);
                while($f=mysqli_fetch_array($s)){
                    echo "To:".' '.$f['bussiness'];?><br><?php
                    echo "By:".' '.$f['giver'];?><br><?php
                    echo "Comment:".' '.$f['comment'];?><hr><?php
                    $id = $f['id'];
                    echo "<a href='report.php?del=$id' class='btn'>Delete</a><br><a href='report.php?cancel=$id' class='btn'>Cancel</a>";

                }
         }
         if(isset($_GET['del'])){
             $id = strval($_GET['del']);
             $query = "DELETE FROM rating WHERE id=$id";
             $sql = mysqli_query($conn,$query);
             $query = "DELETE FROM reported WHERE id=$id";
             $sql = mysqli_query($conn,$query);
             ?>
             <script>
                 location.href='report.php';
             </script>
             <?php
         }
          if(isset($_GET['cancel'])){
             $id = strval($_GET['cancel']);
             $query = "DELETE FROM reported WHERE id=$id";
             $sql = mysqli_query($conn,$query);
              ?>
             <script>
                 location.href='report.php';
             </script>
             <?php
         }
    ?>
    </div>
    
</body>
</html>