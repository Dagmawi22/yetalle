<?php
session_start();

if(!isset($_SESSION['email'])  || $_SESSION['type']!='bussiness'){
    ?>
    <script>alert('Please login first.'); location.href="../user/login";</script>
    <?php
}
require("../db/connection.php");

if(isset($_COOKIE['email']) && isset($_COOKIE['lat']) && isset($_COOKIE['lng'])){
$email = $_COOKIE['email'];
$lat = $_COOKIE['lat'];
$lng = $_COOKIE['lng'];

	$query = "UPDATE bussiness SET lat='$lat',lng='$lng' WHERE email='$email'";
	$sql = mysqli_query($conn,$query);
} 

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Accounts | Yetalle</title>
    <link rel="icon" type="image/png" href="../icons/yet.png" hreflang="en-us">
     <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/mystyle.css?version=50">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <style>
       *{
            font-family: 'Source Sans Pro', sans-serif;
            font-style: normal;
            font-weight:400;
        }
        
    </style>
</head>
<body>
    <?php
    $email = $_SESSION['email'];
    $type = $_SESSION['type'];

    if($type=='bussiness'){
         $query = "SELECT * FROM bussiness WHERE email='$email'";
    $sql = mysqli_query($conn,$query);
    $fetch = mysqli_fetch_array($sql);
    $name = $fetch['name'];
     $photo = $fetch['photo'];
    $lvl = $fetch['acc_type'];
    $view = $fetch['views'];
    $id = $fetch['id'];
    $logo = $fetch['logo'];
    $desc= $fetch['description'];

    }?>
    <div class="topnav" id="myTopnav" style="position:fixed; z-index:100; background-color:#00bfff; width:100%;">
        <a href="../HOME" class="mr-5"><img src="../icons/yet.png" width="60" height="60"></a>
         <?php
        if(isset($_SESSION['email']) && $_SESSION['type']=='bussiness'){
            ?>
                <a href="" class="text-center" style="color:black; font-weight:600; text-decoration:none; margin:0 auto;">
         <img src="../images/<?php echo $photo?>" width="100" height="100" style="border-radius:100%;"><br>  
       <span style="text-transform:uppercase; font-size:27px; color:white;"><?php echo $name?></span>
       
   
     <a href="log-out.php" class="text-center" style="color:white; font-weight:600; text-decoration:none; margin-top:-35px;">
           
        Log Out</a>
    </a>
            <?php
        }
        ?>
        <a href="my-profile" class="" style="font-weight:600; color:black; text-decoration:none; margin-top:-20px;">
        <!-- <img src="icons/destination.png" width="18" height="18">  -->
        My Profile</a>
        <a href="" class="" style="font-weight:600; color:white; border-bottom:5px solid white; text-decoration:none;">
        <!-- <img src="icons/blog 1.png" width="18" height="18">  -->
        Premium</a>
        <a href="my-reviews" class="" style="color:black; font-weight:600; text-decoration:none;">
         <!-- <img src="icons/food.png" width="18" height="18">  -->
        My Reviews</a>
        <a href="../user/profile?id=<?php echo $id?>" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        View-As</a>
        <a href="my-events" class="" style="color:black; font-weight:600; text-decoration:none;">
         <!-- <img src="icons/food.png" width="18" height="18">  -->
        Post an Event</a>
        <a href="my-vacancy" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        Post a Job</a>
        
        
       <a href="javascript:void(0);" class="icon mt-3" style="margin-right:50px;" onclick="myFunction()"> 
           <img src="../icons/2747254.png" width="40" height="40" style="" class=""> 
            </a>
            <br>
             </div>
        
    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
   
    <!--end of nav-->
    <br><br><br><br><br>
    
    
    <?php
        
    ?>
    <!-- <div class="alert alert-info">
        The places in the images below aren't real places. They are only for illustration.
    </div> -->
    
    
<div class="level container-fluid ml-5 mt-2" style="color:black">
        <span style="font-size:25px;">Yetalle Premium</span> <img src="../icons/green.png" width="26" height="26"> </div><br>
  
    <div class="container-fluid leveldesc" style="font-size:16px;">
        This level of Yetalle bussiness account gives you more chance of reaching your customers.<br>
        <div class="" style="margin-left:10%; width:80%;">
            <div class="row">

            
<!--             
           <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
           1/ enables you post more photos (upto 10) to illustrate your customers about your services<br>    
           <img src="../demo/post_photo.png" width="100%" height="300" style="margin:auto;"></div> -->
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-4">
            <span style="color:#00bfff; font-size:30px;">|</span>
             <span style="font-size:20px;">suggests your business on profile pages of similar bussinesses.</span> <br>    
                <!-- <button 
                style="padding:10px 10px; background-color:#00bfff; border:none;
                 color:white; font-size:15px; cursor:pointer; border-radius:3px;">
                View Demo</button> -->
            
            </div>
           
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-4">
             <span style="color:#00bfff; font-size:30px;">||</span>
             <span style="font-size:20px;">suggests your business on top of others when users search for nearby bussinesses.</span> <br>    
            <!-- <img src="../demo/sugg_result.png" width="100%" height="300" style="margin:auto;"> -->
        </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-4">
            <span style="color:#00bfff; font-size:30px;">|||</span>
             <span style="font-size:20px;">removes suggestion of competitors from your profile.</span> <br> 
            <!-- <img src="../demo/no-sugg.png" width="100%" height="300" style="margin:auto;"> -->
        </div>

            <!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
            <span style="color:#00bfff; font-size:20px;">|</span> enables users find your profile easily by your username<br>   
            <img src="../demo/username.png" width="100%" height="300" style="margin:auto;"></div> -->
            
             <!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
              4/ enables users call you and send you emails from your profile page in one click<br>    
             <img src="../demo/call-em.png" width="100%" height="300" style="margin:auto;"></div> -->
             </div>
            <p class="text-center">
                <!-- <div class="mt-2">
                <img src="../icons/1614832.png" width="20" height="20"> <span style="font-size:14px;">1800ETB/Year</span></div> -->
                
            </p>
        </div>
    </div>
    <div>
        <!-- <img src="../icons/pay.gif" width="100%" height="200"> -->
    </div>
    <?php
        $query = "SELECT phone FROM bussiness WHERE email='$email'";
        $sql = mysqli_query($conn,$query);
        $fetch = mysqli_fetch_array($sql);
        $phone = $fetch['phone'];
         $p1 = substr($phone,0,2);
        $p2 = substr($phone,2,2);
        $p3 = substr($phone,4,2);
        $p4 = substr($phone,6,2);
        $p5 = substr($phone,8,2);
        $phone = $p1.' '.$p2.' '.$p3.' '.$p4.' '.$p5;
    ?>
    <p class="text-center">
        <!-- Pay 1800 birr<br>from <span style="font-size:17px; padding:3px; border-radius:5px; background-color:#00bfff; color:white;"><?php if($phone !='') echo $phone; if($phone==' ' || $phone==0) echo "<a href='profile.php' class='text-white' style='font-size:11px; text-decoration:underline;'>Fill your mobile number first.</a>"?></span><br><br> to&nbsp;&nbsp; <span class="" style="font-size:17px; padding:3px; border-radius:5px; background-color:#00bfff; color:white;">09 18 88 82 25</span><br> to get silver badge.
        <br>You can pay through:<br>
        <img src="../icons/amole.png" width="50" height="50" style="border-radius:100%;" class="amole">&nbsp;
        <img src="../icons/hellocash.jpg" width="50" height="50" style="border-radius:100%;" class="hellocash">&nbsp;
        <img src="../icons/telebirr.png" width="50" height="50" style="border-radius:100%;" class="telebirr">
        <img src="../icons/cbe.png" width="50" height="50" style="border-radius:100%;" class="cbe">&nbsp;
        <img src="../icons/ebirr.png" width="50" height="50" style="border-radius:100%;" class="ebirr">&nbsp;
        <img src="../icons/awash.png" width="50" height="50" style="border-radius:100%;" class="awash">
        <br> -->
        <span style="font-size:16px;">For more info and upgrade to premium talk to us on
         <a href="https://www.t.me/yetalle01" style="font-size:16px;">telegram</a>
          or <a href="tel:0918888225" style="font-size:16px;">call us</a> </span>
    </p>
    <hr>

   <!-- <div class="level container-fluid ml-5 mt-5" style="color:black">
       <span style="font-size:17px;">Golden Badge</span> <img src="../icons/green.png" width="20" height="20"> </div><br>
    <div class="container-fluid leveldesc">
        This level of Yetale account provides the best exposure to customers.<br>
        <div class="container-fluid">
            <div class="row">

            
           
             <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
              1/ enables you post photos (upto 20 photos)<br>    
             <img src="../demo/post_photo.png" width="100%" height="300" style="margin:auto;"></div>
           
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4"> 
             2/ removes suggestions of similar bussinesses from your profile page<br>
            3/ suggests your bussiness on similar profile pages<br>    
            <img src="../demo/suggestion.png" width="100%" height="300" style="margin:auto;"></div>
            
            
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
            4/ enables users call you and send you emails from your profile page in one click<br>    
            <img src="../demo/call-email.png" width="100%" height="300" style="margin:auto;"></div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
            5/ enables you share your profile page to social media<br>   
            <img src="../demo/share.png" width="100%" height="300" style="margin:auto;"></div>

             <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
            5/ enables users find your profile easily by your username<br>   
            <img src="../demo/username.png" width="100%" height="300" style="margin:auto;"></div>
           
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4"> 
             
            6/ enables you post your upcoming events<br>    
            <img src="../demo/event.png" width="100%" height="300" style="margin:auto;"></div>
            
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 mt-4">
            7/ enables you post your vacancy<br>    
            <img src="../demo/vacancy.png" width="100%" height="300" style="margin:auto;"></div>
        
        </div>
    </div>
         <p class="text-center">
                <div class="mt-2">
                <img src="../icons/1614832.png" width="20" height="20"> <span style="font-size:14px;">3200ETB/Year</span></div>
            </p>
           
    </div>
    <?php 
       
    ?>
     <div>
        <img src="../icons/pay.gif" width="100%" height="200">
    </div>
    <?php
        $query = "SELECT phone FROM bussiness WHERE email='$email'";
        $sql = mysqli_query($conn,$query);
        $fetch = mysqli_fetch_array($sql);
        $phone = $fetch['phone'];
        $p1 = substr($phone,0,2);
        $p2 = substr($phone,2,2);
        $p3 = substr($phone,4,2);
        $p4 = substr($phone,6,2);
        $p5 = substr($phone,8,2);
        $phone = $p1.' '.$p2.' '.$p3.' '.$p4.' '.$p5;
    ?>
     <p class="text-center">Pay 3200 birr<br>from <span style="font-size:17px; padding:3px; border-radius:5px; background-color:#00bfff; color:white;"><?php if($phone !='') echo $phone; if($phone==' ' || $phone==0) echo "<a href='profile.php' class='text-white' style='font-size:11px; text-decoration:underline;'>Fill your mobile number first.</a>"?></span><br><br> to&nbsp;&nbsp; <span class="" style="font-size:17px; padding:3px; border-radius:5px; background-color:#00bfff; color:white;">09 18 88 82 25</span><br> to get golden badge.
        <br>You can pay through:<br>
        <img src="../icons/amole.png" width="50" height="50" style="border-radius:100%;" class="amole">&nbsp;
        <img src="../icons/hellocash.jpg" width="50" height="50" style="border-radius:100%;" class="hellocash">&nbsp;
        <img src="../icons/telebirr.png" width="50" height="50" style="border-radius:100%;" class="telebirr">
        <img src="../icons/cbe.png" width="50" height="50" style="border-radius:100%;" class="cbe">&nbsp;
        <img src="../icons/ebirr.png" width="50" height="50" style="border-radius:100%;" class="ebirr">&nbsp;
        <img src="../icons/awash.png" width="50" height="50" style="border-radius:100%;" class="awash">
        <br>
        <span>For more info talk to us on <a href="https://www.t.me/yetalle01">telegram</a> </span>
    </p> -->
    <hr>
    <hr>
<footer class="mt-2">
    <p class="text-center" style="font-size:14px;">
    <span style="color:white; background-color:#00bfff; padding:5px; border-radius:10px;"><strong>Yetalle</strong></span> is a platform to help customers easily explore the goods and 
    services in their surrounding and to help bussinesses to easily be explored. <br>
   
</p>
<?php include('../includes/contacts2.htm');?>
<?php include('../includes/social.htm');?>
</footer>

<div style="display:none; position:absolute; top:0; left:0; width:100vw; height:100vh; z-index:100;">
<img src="../demo/suggestion.png" width="100%" height="100%">
<div style="position:absolute; top:5px; right:20px; cursor:pointer;" class="mr-5 mt-2">
<img src="../icons/close.png" width="40" height="40">
</div>
</div>

    
    
</body>
</html>