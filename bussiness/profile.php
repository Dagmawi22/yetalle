
<?php
    session_start();
   
  if(!isset($_SESSION['email']) || $_SESSION['type']=='user'){
      ?><script>
      alert('You must login first.');
      location.href="../user/login";
      </script><?php
      $email = $_SESSION['email'];
  }
   
    require("../db/connection.php");
    $email = $_SESSION['email'];
    $type = $_SESSION['type'];


    $query = "SELECT * FROM bussiness WHERE email='$email'";
    $sql = mysqli_query($conn,$query);
    $fetch = mysqli_fetch_array($sql);
    $name = $fetch['name'];
     $photo = $fetch['photo'];
    $lvl = $fetch['acc_type'];
    $view = $fetch['views'];
    $id = $fetch['id'];
    $logo = $fetch['logo'];
    $desc= $fetch['description'];
    $address = $fetch['address'];
    $city = $fetch['city'];
    $avg_rating = $fetch['avg_rating'];
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $name?> | Yetalle</title>
    <link rel="icon" type="image/png" href="../icons/yet.png" hreflang="en-us">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/mystyle.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <style>
        .social{
             transition:2s;
        }
       .social:hover{
           opacity:0.5;
          
       }
       option{
           color:black;
           font-size:13px;
       }
       *{
            font-family: 'Source Sans Pro', sans-serif;
            font-style: normal;
            font-weight:400;
        }
       
    </style>
   
         
    
</head>

<body>
    <?php
    
    ?>
    <!-- Profile Image -->

     <div class="topnav" id="myTopnav" style="position:fixed; background-color:#00bfff; width:100%; z-index:100;">
        <a href="../HOME" class="mr-5"><img src="../icons/yet.png" width="60" height="60"></a>
         <?php
        if(isset($_SESSION['email']) && $_SESSION['type']=='bussiness'){
            ?>
                <a href="#" class="text-center" style="color:black; font-weight:600; text-decoration:none; margin:0 auto;">
         <img src="../images/<?php echo $photo?>" width="100" height="100" style="border-radius:100%;"><br>  
       <span style="text-transform:uppercase; font-size:27px; color:white;"><?php echo $name?></span>
       
   
     <a href="log-out.php" class="text-center" style="color:white; font-weight:600; text-decoration:none; margin-top:-35px;">
           
        Log Out</a>
    </a>
            <?php
        }
        ?>
        <a href="" class="" style="font-weight:600; color:white; border-bottom:5px solid white; text-decoration:none; margin-top:-20px;">
        <!-- <img src="icons/blog 1.png" width="18" height="18">  -->
        My Profile</a>
        <a href="my-badge" class="" style="font-weight:600; color:black; text-decoration:none; ">
        <!-- <img src="icons/destination.png" width="18" height="18">  -->
        Premium</a>
        <a href="my-reviews" class="" style="color:black; font-weight:600; text-decoration:none;">
         <!-- <img src="icons/food.png" width="18" height="18">  -->
        My Reviews</a>
        <a href="../user/profile?id=<?php echo $id?>" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        View-As</a>
         <a href="my-events" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        Post an Event</a>
         <a href="my-vacancy" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        Post a Job</a>
        <?php
        if(isset($_SESSION['email']) && $_SESSION['type']=='bussiness'){
            ?>
               
            <?php
        }
        ?>
        
        
       
      
        
    
       <a href="javascript:void(0);" class="icon mr-3 mt-3" style="position:fixed; right:0; top:0;" onclick="myFunction()"> 
           <img src="../icons/2747254.png" width="40" height="40" style="" class="ham"> 
            </a>
            <br>
             </div>
                
   
    
   
    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
   
    <!--end of nav-->
    <br>
    <br>
    <br>
    

    <?php if($lvl=='none'){
        ?>
    <div class="alert alert-info mt-4" style="font-size:12px;">
    Hello <span style="font-weight:800;">X Hotel</span>, do you want to upgrade your bussiness account to premium? 
    It is targeted to give you more customer leads and grow your business easily.<br>
    <a href="my-badge"><u style="font-size:12px;">learn more about premium account here</u></a>

    </div>
        <?php
    }

    if($lvl!='none'){
        ?>
   
       
    <div class="alert alert-info mt-4" style="font-size:12px;">
     your badge: <img src="../icons/green.png" width="18" height="18">
        <br>expires on: <?php echo $fetch['expiry']?>
    </div>
        <?php
    }

   ?>
    
    
    

<p class="text-center mt-2">
   
    
    <span style="font-weight:700; font-size:1.5em;" class="margin-left:-70px;"><?php if($name!='') echo $name; if($name=='') echo 'Your bussiness name';?></span>
    <?php
    if($lvl=='blue'){
        echo "<img src='../icons/blue.png' width='25' height='25' style='margin-top:-10px;'>";
    }
     if($lvl=='green'){
        echo "<img src='../icons/green.png' width='25' height='25' style='margin-top:-10px;'>";
    }
    ?>
    <br>
    <span style="font-weight:700; font-size:1.2em; color:#00bfff;">
    @<?php if($fetch['id']!='') echo $fetch['id'];
    if($fetch['id']=='') echo 'your_username';
    ?>
    </span><br>
    <?php
         $query_avg = "SELECT avg(star) as average FROM rating WHERE bussiness='$id'";
                $sql_avg = mysqli_query($conn,$query_avg);
                $fetch_avg = mysqli_fetch_array($sql_avg);
                $avrg = $fetch_avg['average'];
                $rate = round($avrg,1);

                if($rate=='0'){
    ?><img src="../icons/empty (2).png" width="18" height="18">
    <img src="../icons/empty (2).png" width="18" height="18">
    <img src="../icons/empty (2).png" width="18" height="18">
    <img src="../icons/empty (2).png" width="18" height="18">
    <img src="../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='1'){
    ?><img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/empty (2).png" width="18" height="18">
    <img src="../icons/empty (2).png" width="18" height="18">
    <img src="../icons/empty (2).png" width="18" height="18">
    <img src="../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'1' && $rate<2){
    ?><img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/half-star.png" width="20" height="20">
    <img src="../icons/empty (2).png" width="18" height="18">
    <img src="../icons/empty (2).png" width="18" height="18">
    <img src="../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='2'){
    ?><img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/empty (2).png" width="18" height="18">
    <img src="../icons/empty (2).png" width="18" height="18">
    <img src="../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'2' && $rate<3){
    ?><img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/half-star.png" width="20" height="20">
    <img src="../icons/empty (2).png" width="18" height="18">
    <img src="../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='3'){
    ?><img src="../icons/full-star.png" width="18" height="18">
    <img src="../icons/full-star.png" width="18" height="18">
    <img src="../icons/full-star.png" width="18" height="18">
    <img src="../icons/empty (2).png" width="18" height="18">
    <img src="../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'3' && $rate<4){
    ?><img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/half-star.png" width="20" height="20">
    <img src="../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate=='4'){
    ?><img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/empty (2).png" width="18" height="18">
    <?php 
}
if($rate>'4' & $rate<5){
    ?><img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/half-star.png" width="20" height="20">
    <?php 
}
if($rate=='5'){
    ?><img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/full-star.png" width="20" height="20">
    <img src="../icons/full-star.png" width="20" height="20">
    <?php 
}



    ?><br>
    <img src="../icons/eye-128.png" width="14" height="14"> <span style="font-size:12px;"><?php echo $view?></span><br>

    <br><br>
     <img src="../images/<?php echo $logo?>" width="150" height="150" style="border-radius:10px;" class="" alt="your logo"><br>
     <?php
     if($lvl=='green'){
         ?>
         <hr>
         <p class="text-center share" style="box-shadow:0px 4px 6px rgba(0,0,0,0.3); padding:10px;">
            <strong style="font-size:20px;">SHARE YOUR PROFILE</strong><br>
            <span style="font-size:13px;">Sharing your business page to social media is an ideal way of promoting your bussiness to a large audience and growing out.</span><br>
            <a href="https://www.facebook.com/sharer.php?u=https://www.yetalle.com/user/profile?id=<?php echo $id?>" class="social">
            <img src="../icons/socials/facebook.png" width="28" height="28"></a>&nbsp;&nbsp;
            <a href="https://twiter.com/share?url=https://www.yetalle.com/user/profile?id=<?php echo $id?> & text=<?php echo $name?> on Yetale" class="social">
                <img src="../icons/socials/twitter.png" width="28" height="28"></a>&nbsp;&nbsp;
            <a href="https://api.whatsapp.com/send?text=<?php echo $name?> on Yetale https://www.yetalle.com/user/profile?id=<?php echo $id?>" class="social">
                <img src="../icons/socials/whatsapp.png" width="30" height="30"></a>&nbsp;&nbsp;
            <a href="https://www.linkedin.com/shareArticle?mini=true&url=https://www.yetalle.com/user/profile?id=<?php echo $id?>" class="social">
                <img src="../icons/socials/linkedin.png" width="28" height="28" style="border-radius:100%;"></a>&nbsp;&nbsp;
            <a href="https://www.pinterest.com/pin/create/button?url=https://www.yetalle.com/user/profile?id=<?php echo $id?>&media=https://www.yetale.com/images/<?php echo $photo?>" class="social">
                <img src="../icons/socials/pinterest.png" width="28" height="28"></a>
         </p>
         
         <?php
     }
     ?>
  <div class="ml-3 mb-2 recentlyopened">UPDATE PROFILE</div>
    <!-- update logo -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>Business Logo</span>  
   <script>
       function one(){
           document.getElementById('one').style.display='block';
           document.getElementById('down1').style.display='none';
           document.getElementById('up1').style.display='block';

       }
       function one1(){
           document.getElementById('one').style.display='none';
           document.getElementById('up1').style.display='none';
           document.getElementById('down1').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down1" onclick="one()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up1" onclick="one1()"><</span> 
   <div id="one" style="display:none; transition:2s;"><input type="file" name="logo" accept="image/*" class="form-control" rquired mutiple>
   <input type="submit" value="Save" name="update-logo" class="btn form-control">
   </div>
   </div>
    </form>
    <?php 
    if(isset($_POST['update-logo'])){
        $logo =str_shuffle('giugsdiu').$_FILES['logo']['name'];
            
             $Temp =  $_FILES['logo']['tmp_name'];
             $MyLocation= "../images/".$logo ;
             move_uploaded_file($Temp,$MyLocation);
             $query = "UPDATE bussiness SET logo='$logo' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('Success.'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of logo form -->
    <!-- update cover -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>Cover Photo</span> 
   <script>
       function two(){
           document.getElementById('two').style.display='block';
           document.getElementById('down2').style.display='none';
           document.getElementById('up2').style.display='block';

       }
       function two1(){
           document.getElementById('two').style.display='none';
           document.getElementById('up2').style.display='none';
           document.getElementById('down2').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down2" onclick="two()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up2" onclick="two1()"><</span> 
      <div id="two" style="display:none;">
   <input type="file" name="cover" accept="image/*" class="form-control" rquired mutiple>
   <input type="submit" value="Save" name="update-cover" class="btn form-control">
    </div>
   </div>
    </form>
    <?php 
    if(isset($_POST['update-cover'])){
        $cover =str_shuffle('giugsdiu').$_FILES['cover']['name'];
            
             $Temp =  $_FILES['cover']['tmp_name'];
             $MyLocation= "../images/".$cover ;
             move_uploaded_file($Temp,$MyLocation);
             $query = "UPDATE bussiness SET photo='$cover' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('Success.'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of cover form -->

     <!-- update name -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container">
   <span>Business Name</span>
   <script>
       function three(){
           document.getElementById('three').style.display='block';
           document.getElementById('down3').style.display='none';
           document.getElementById('up3').style.display='block';
           document.getElementById('bizname').focus();


       }
       function three1(){
           document.getElementById('three').style.display='none';
           document.getElementById('up3').style.display='none';
           document.getElementById('down3').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down3" onclick="three()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up3" onclick="three1()"><</span> 
      <div id="three" style="display:none;">     
   <input type="text" id="bizname" name="name" pattern="^[a-zA-Z\s]*" value="<?php echo $fetch['name']?>" maxlength="25" class="form-control" rquired>
   <input type="submit" value="Save" name="update-name" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-name'])){
             $name= $_POST['name'];
             $new_name = str_replace(' ','_',$name);  
              $query_num = "SELECT count(email) AS num FROM bussiness WHERE name='$name'";
             $sql_num = mysqli_query($conn,$query_num);
             $fetch_num = mysqli_fetch_array($sql_num);
             $num = $fetch_num['num']+1;
             $id = $new_name.'_'.$num; 

            $query = "UPDATE bussiness SET name='$name' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($lvl=='green'){
                 $query = "UPDATE bussiness SET id='$id' WHERE email='$email'";
                $sql = mysqli_query($conn,$query);
             }
             if($sql){
                 echo "<script>alert('Success.'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of update name -->

    <!-- update category -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container">
   <span>Business Category</span> 
   <script>
       function four(){
           document.getElementById('four').style.display='block';
           document.getElementById('down4').style.display='none';
           document.getElementById('up4').style.display='block';

       }
       function four1(){
           document.getElementById('four').style.display='none';
           document.getElementById('up4').style.display='none';
           document.getElementById('down4').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down4" onclick="four()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up4" onclick="four1()"><</span> 
      <div id="four" style="display:none;">    
   <select name="category" class="form-control" style="background-color:white;">
      
        <?php include('cat.htm');?> 
    </select>
   <input type="submit" value="Save" name="update-category" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-category'])){
             $category= $_POST['category'];
             

            $query = "UPDATE bussiness SET category='$category',category_type='primary' WHERE email='$email' AND category_type='primary'";
              $sql = mysqli_query($conn,$query);
              if($sql){
                echo "<script>alert('Success.'); location.href='my-profile';</script>";
              }
    }
    ?>
    <hr>
    <!-- end of update name -->

      <!-- update desc -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>Business Description</span> 
    <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down5" onclick="five()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up5" onclick="five1()"><</span> 
      <div id="five" style="display:none;">  
   <br><span style="font-size:10px;">Describe your business in less than 500 words.<br>Customers will see it on your profile.</span>
   <script>
       function five(){
           document.getElementById('five').style.display='block';
           document.getElementById('down5').style.display='none';
           document.getElementById('up5').style.display='block';
            document.getElementById('bizdesc').focus();

       }
       function five1(){
           document.getElementById('five').style.display='none';
           document.getElementById('up5').style.display='none';
           document.getElementById('down5').style.display='block';
           
       }
   </script> 
    
   <textarea name="desc" maxlength="200" id="bizdesc" class="form-control" rquired></textarea>
   <input type="submit" value="Save" name="update-desc" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-desc'])){
             $desc= mysql_real_escape_string($_POST['desc']);
             
            $query = "UPDATE bussiness SET description ='$desc' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('Success.'); location.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of update address -->

       <!-- update city -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container">
   <span>City</span>
   <script>
       function six(){
           document.getElementById('six').style.display='block';
           document.getElementById('down6').style.display='none';
           document.getElementById('up6').style.display='block';

       }
       function six1(){
           document.getElementById('six').style.display='none';
           document.getElementById('up6').style.display='none';
           document.getElementById('down6').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down6" onclick="six()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up6" onclick="six1()"><</span> 
      <div id="six" style="display:none;">     
   <span>current city: <?php echo $fetch['city']?></span><br>
   <span style="color:black; font-size:10px;">(If your city is not in the list, select your nearest city.)</span>
   <select name="city" class="form-control" style="background-color:white;">
       
          <?php include('cities.htm');?>
          </select>
   <input type="submit" value="Save" name="update-city" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-city'])){
             $city= $_POST['city'];
            

            $query = "UPDATE bussiness SET city='$city' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('Success.'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of update city -->
     <!-- update address -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>Address</span> 
   <script>
       function seven(){
           document.getElementById('seven').style.display='block';
           document.getElementById('down7').style.display='none';
           document.getElementById('up7').style.display='block';
            document.getElementById('bizaddress').focus();

       }
       function seven1(){
           document.getElementById('seven').style.display='none';
           document.getElementById('up7').style.display='none';
           document.getElementById('down7').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down7" onclick="seven()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up7" onclick="seven1()"><</span> 
      <div id="seven" style="display:none;">    
   <input type="text" name="address" id="bizaddress" value="<?php echo $fetch['address']?>"  maxlength="100" class="form-control" rquired>
   <input type="submit" value="Save" name="update-address" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-address'])){
             $address= mysql_real_escape_string($_POST['address']);
             
            $query = "UPDATE bussiness SET address='$address' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('Success.'); location.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of update address -->

       <!-- update phone -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>Mobile</span>
   <script>
       function eight(){
           document.getElementById('eight').style.display='block';
           document.getElementById('down8').style.display='none';
           document.getElementById('up8').style.display='block';
            document.getElementById('bizphone').focus();

       }
       function eight1(){
           document.getElementById('eight').style.display='none';
           document.getElementById('up8').style.display='none';
           document.getElementById('down8').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down8" onclick="eight()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up8" onclick="eight1()"><</span> 
      <div id="eight" style="display:none;">
      
   <input type="text" name="phone" id="bizphone" value="<?php echo $fetch['phone']?>" placeholder="eg: 091143--76" pattern="[0][9][0-9]{8}" maxlength="10" class="form-control" rquired>
   <span style="font-size:10px; color:red" id="error-phone"></span>
   <input type="submit" value="Save" name="update-phone" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-phone'])){
             $phone= $_POST['phone'];
             $query_phone="SELECT count(email) AS num FROM bussiness WHERE phone=$phone";
             $sql_phone = mysqli_query($conn,$query_phone);
             $fetch_phone = mysqli_fetch_array($sql_phone);
             $phones = $fetch_phone['num'];

             
             
             if($phones>0){
                 ?><script>
                 document.getElementById("error-phone").innerHTML="Phone already in use. Try another.";
                 document.getElementById("bizphone").style.border="1px red solid";
                 document.getElementById("bizphone").focus();</script><?php

             }
             if($phones<1){
                 $query = "UPDATE bussiness SET phone='$phone' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('Success.'); locaton.href='my-profile';</script>";
             }
             }

           
    }
    ?>
    <hr>
    <!-- end of update phone -->


        <!-- update office -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>Tel</span>
   <script>
        function thirteen(){
           document.getElementById('thirteen').style.display='block';
           document.getElementById('down13').style.display='none';
           document.getElementById('up13').style.display='block';
            document.getElementById('bizoffice').focus();

       }
       function thirteen1(){
           document.getElementById('thirteen').style.display='none';
           document.getElementById('up13').style.display='none';
           document.getElementById('down13').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down13" onclick="thirteen()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up13" onclick="thirteen1()"><</span> 
      <div id="thirteen" style="display:none;">
        
   <input type="text" name="office" id="bizoffice" value="<?php echo $fetch['office_phone']?>" placeholder="eg: 011523--76" pattern="[0][0-9]{9}" maxlength="10" class="form-control" rquired>
   <span style="font-size:10px; color:red" id="error-office"></span>
   <input type="submit" value="Save" name="update-office" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-office'])){
             $phone= $_POST['office'];
             $query_phone="SELECT count(email) AS num FROM bussiness WHERE office_phone=$phone";
             $sql_phone = mysqli_query($conn,$query_phone);
             $fetch_phone = mysqli_fetch_array($sql_phone);
             $phones = $fetch_phone['num'];

             
             
             if($phones>0){
                 ?><script>
                 document.getElementById("error-office").innerHTML="Phone already in use. Try another.";
                 document.getElementById("bizoffice").style.border="1px red solid";
                 document.getElementById("bizoffice").focus();</script><?php

             }
             if($phones<1){
                 $query = "UPDATE bussiness SET office_phone='$phone' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('Success.'); locaton.href='my-profile';</script>";
             }
             }

           
    }
    ?>
    <hr>
    <!-- end of update phone -->

     <!-- update website -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>Website</span>
   <script>
       function nine(){
           document.getElementById('nine').style.display='block';
           document.getElementById('down9').style.display='none';
           document.getElementById('up9').style.display='block';
            document.getElementById('bizsite').focus();

       }
       function nine1(){
           document.getElementById('nine').style.display='none';
           document.getElementById('up9').style.display='none';
           document.getElementById('down9').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down9" onclick="nine()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up9" onclick="nine1()"><</span> 
    <span style="font-size:11px;">(eg: www.yoursite.com)</span>
      <div id="nine" style="display:none;">     
   <input type="text" name="website" id="bizsite" value="<?php echo $fetch['website']?>" maxlength="10" class="form-control" rquired>
   <input type="submit" value="Save" name="update-website" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-website'])){
             $website= $_POST['website'];
             
            $query = "UPDATE bussiness SET website='$website' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('Success.'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of update website -->

     <!-- update hours -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>Service hours</span> 
   <script>
       function ten(){
           document.getElementById('ten').style.display='block';
           document.getElementById('down10').style.display='none';
           document.getElementById('up10').style.display='block';

       }
       function ten1(){
           document.getElementById('ten').style.display='none';
           document.getElementById('up10').style.display='none';
           document.getElementById('down10').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down10" onclick="ten()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up10" onclick="ten1()"><</span> 
      <div id="ten" style="display:none;">
   <br>    
   <span>From:</span><input type="time" name="from" value="<?php echo $fetch['open_from']?>" class="form-control" rquired>
   <span>To:</span><input type="time" name="to" value="<?php echo $fetch['open_to']?>" class="form-control" rquired>
   <input type="submit" value="Save" name="update-serv" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-serv'])){
             $from= $_POST['from'];
              $to= $_POST['to'];
             
             
            $query = "UPDATE bussiness SET open_from='$from',open_to='$to' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('Success.'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of update hours -->

      <!-- update founded -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>Founded on</span>
   <script>
       function eleven(){
           document.getElementById('eleven').style.display='block';
           document.getElementById('down11').style.display='none';
           document.getElementById('up11').style.display='block';

       }
       function eleven1(){
           document.getElementById('eleven').style.display='none';
           document.getElementById('up11').style.display='none';
           document.getElementById('down11').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down11" onclick="eleven()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up11" onclick="eleven1()"><</span> 
      <div id="eleven" style="display:none;">
   <br>  
   <?php $today = date('20y-m-d');?>  
  <input type="date" name="founded" max="<?php echo $today?>" value="<?php echo $fetch['date_founded']?>" class="form-control" rquired>
   
   <input type="submit" value="Save" name="update-found" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-found'])){
             $found= $_POST['founded'];
             
             
            $query = "UPDATE bussiness SET date_founded='$found' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('Success.'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    <!-- end of update founded -->

     <!-- update emp -->
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <span>Number of Employees</span>
   <script>
       function twelve(){
           document.getElementById('twelve').style.display='block';
           document.getElementById('down12').style.display='none';
           document.getElementById('up12').style.display='block';
            document.getElementById('bizemp').focus();

       }
       function twelve1(){
           document.getElementById('twelve').style.display='none';
           document.getElementById('up12').style.display='none';
           document.getElementById('down12').style.display='block';
           
       }
   </script> 
   <span style="float:right; font-size:13px; transform:rotate(-90deg); cursor:pointer;"
    class="mt-1 mr-3" id="down12" onclick="twelve()"><</span> 
    <span style="float:right; display:none; font-size:13px; transform:rotate(-270deg); cursor:pointer;"
    class="mt-1 mr-3" id="up12" onclick="twelve1()"><</span> 
      <div id="twelve" style="display:none;">
   <br>  
   
  <input type="number" id="bizemp" name="emp" min="1" class="form-control" rquired>
   
   <input type="submit" value="Save" name="update-emp" class="btn form-control">
   </div>
    </div>
    </form>
    <?php 
    if(isset($_POST['update-emp'])){
             $emp= $_POST['emp'];
             
             
            $query = "UPDATE bussiness SET emp_size='$emp' WHERE email='$email'";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('Success.'); locaton.href='my-profile';</script>";
             }
    }
    ?>
    <hr>
    
    <div class="ml-3 recentlyopened"><img src="../icons/plus.png" style="" width="32" height="32"> EXTRA CATEGORIES
</div><div style="margin:0 auto; font-size:12px;" class="container-fluid">Registering with multiple categories of business helps you get discovered by more clients and grow your bussiness.</div>
    <form method="post" class="container-fluid">
        <hr>
        

          <?php if($lvl=='green' || $lvl=='blue'){
            ?>
                <div class="mt-1" style="font-size:14px;">Category 1</div>
        <span>
            <select name="cat1" class="form-control" style="background-color:white; font-size:14px; color:black;"> 
           
            <?php include('cat.htm');?>
        </select></span>
            <?php
        }?>

         <?php if(1){
            ?>
                <div class="mt-1" style="font-size:14px;">Choose an additional category</div>
        <span>
            <select name="cat1" class="form-control" style="background-color:white; font-size:14px; color:black;"> 
           <?php include('cat.htm');?>
        </select></span>
            <?php
        }?>
         
        
       <br>
        <input type="submit" value="Add Category" name="update-ex-cat" class="btn form-control" style="font-size:16px;">
        <br>
        <span style="font-size:14px;">Your business can be registered in 2 extra categories. Want to be registered 
        in more categories? <a href="tel:0918888225" style="font-size:14px;">call us</a></span>
    </form>
    <?php
    if(isset($_POST['update-ex-cat'])){
        $cat1 = $_POST['cat1'];
        
        if ($_POST['cat1']=='select category') $cat1='';
        
        $queryCat = "SELECT count(category_type) AS num FROM bussiness WHERE email='$email'";
        $sqlCat = mysqli_query($conn,$queryCat);
        $fetchCat = mysqli_fetch_array($sqlCat);
        $NumOfCat = $fetchCat['num'];

        if($NumOfCat >=2){
         $query = "INSERT INTO bussiness (email,category,category_type,name,address,id,city,photo,acc_type,avg_rating) VALUES 
        ('$email','$cat1','extra1','$name','$address','$id','$city','$photo','$lvl','$avg_rating')";
        $sql = mysqli_query($conn,$query);

         if($sql){
            echo "<script>alert('Yes'); location.href='my-profile';</script>";
        }
        }
        if($NumOfCat <2){
            echo "<script>alert('Your bussiness is already registered in 2 categories.');</script>";
        }


       

        
        // $query = "DELETE FROM bussiness WHERE category_type!='primary' AND category=''";
        // $sql = mysqli_query($conn,$query);

       

    }

    ?>
     <hr>
     <div class="ml-3 recentlyopened"><img src="../icons/Tg/PicsArt_02-18-11.30.51.png" style="filter:invert(1);" width="32" height="32"> PHOTOS</div>
    

  
     <!-- update add new photo -->
     <?php
     $query = "SELECT photo,id FROM pics WHERE email='$email'";
     $sql = mysqli_query($conn,$query);
     if(mysqli_num_rows($sql)>0){
         ?><div class="" style="display:flex; flex-direction:row; overflow-x:auto;">
             <?php
             while($fetch = mysqli_fetch_array($sql)){
                $id = $fetch['id'];
                $pic = $fetch['photo'];
                ?>
                <div class="mr-3" style="">
                    <img src="../images/<?php echo $pic?>" width="150" height="150" style="border-radius:10px"><br><br>
                    <a href="my-profile?del-pic=<?php echo $id?>" class="btn-sm mt-1 text-white" style="cursor:pointer; font-size:16px;">
                    <img src="../icons/Tg/PicsArt_02-18-11.51.00.png" width="25" height="25"> Delete</a>
                   
                </div>
               
                <?php
             }?>
         </div><hr>
          
         <?php
     }
     if(isset($_GET['del-pic'])){
         $pic = $_GET['del-pic'];
         ?>
            
         <?php
         $ftf = "SELECT photo FROM pics WHERE id=$pic";
         $ftfs = mysqli_query($conn,$ftf);
         $ftff = mysqli_fetch_array($ftfs);
         $photo = $ftff['photo'];
          rename('../images/'.$photo,'../del/'.$photo);
         $query = "DELETE FROM pics WHERE id=$pic";
         $sql = mysqli_query($conn,$query);
          $query = "DELETE FROM feed WHERE image='$photo'";
         $sql = mysqli_query($conn,$query);
        
        
         ?>
        <script>location.href='my-profile';</script>
         <?php
     }
     ?>
    <form method="post" class="container" enctype="multipart/form-data">
   <div class="container"> 
   <img src="../icons/camera.png" width="30" height="30"> Add a new photo to your profile.<br> 
   <label for="add-image" class="form-control" style="background-color:white; border-bottom:2px solid #00bfff;"><img src="../icons/Tg/PicsArt_02-18-11.30.51.png" style="filter:invert(1);" width="22" height="22"> Choose Photo</label>
  <input type="file" id="add-image" name="add-photo" accept="image/*" class="form-control" rquired hidden>
   
   <input type="submit" value="Upload" name="Post" class="btn form-control" style="font-size:16px;">
   </div>
    </form>
    <?php 
    if(isset($_POST['Post'])){
              $photo = str_shuffle('gdigidsg').$_FILES['add-photo']['name'];
             //echo $FileName;
             $Temp =  $_FILES['add-photo']['tmp_name'];
             $MyLocation= "../images/".$photo ;
             move_uploaded_file($Temp,$MyLocation);
             
             $query_num = "SELECT count(id) AS num FROM pics WHERE email='$email'";
             $sql_num = mysqli_query($conn,$query_num);
             $fetch_num = mysqli_fetch_array($sql_num);
             $num = $fetch_num['num'];

              $qi = "SELECT COUNT(id) AS num FROM pics";
            $si = mysqli_query($conn,$qi);
            $fi = mysqli_fetch_array($si);
            $event_id = $fi['num']+1;

             if(1 && $num>=5){
                echo "<script>alert('Delete one of your photos to add more.'); locaton.href='my-profile';</script>";
                
             }

             
             if(1 && $num<5){
                $query = "INSERT INTO pics VALUES ('$email','$photo',$event_id)";
             $sql = mysqli_query($conn,$query);
             if($sql){
                 echo "<script>alert('Success.'); location.href='my-profile'; </script>";
             }
            }
             
    }
    ?>
    <hr>
    <!-- end of add new photo -->


    
    
    

  
 <div class="ml-3 recentlyopened"><img src="../icons/destination.png" width="30" height="30"> LOCATION</div>
    
    <form method="post" class="container" enctype="">
        
     <p class="text-center" style="font-size:13px;">
         If you want to update your location to where you are right now, just click the button below.
         We will automatically detect your current location and update your profile.<br>
         (allow detection request on your device.)
     </p>
   
    <hr>
    <p class="text-center">
        <?php
        if(1){
            ?>
            <button onClick="getLocation()" class="btn form-control" style="font-size:16px;">Update Location</button>
            <?php
        }
        
    ?>
    
        <span style="color:red; font-size:10px;" id="error-loc"></span><br>
        <script>
            function getLocation(){
              if(navigator.geolocation){
                   navigator.geolocation.getCurrentPosition(savePosition);
               }
               else{
                    document.getElementById("error-loc").innerHTML="Failed to get location.";
               }
               function savePosition(position){
                   document.cookie='lat='+position.coords.latitude+'';
                   document.cookie='lng='+position.coords.longitude+'';
                   var email = <?php echo $email?>;
                   document.cookie='email='+email'';
                   

               }
            
              
            }
            if(!getLocation()){
                document.getElementById("error-loc").innerHTML="Failed to get location. Either you denied location request or your device/browser doesn't support geolocation.";
            }
             if(!saveposition()){
                   document.getElementById("error-loc").innerHTML="Failed to get location.";
               }
               if(savePosition()){
                  alert('Location successfully updated.');
               }
        </script>
    </p>
  
</form>
<?php

?>


</div>


</div>
<hr><hr>

<footer class="mt-2">
    <p class="text-center" style="font-size:14px;">
    <span style="color:white; background-color:#00bfff; padding:5px; border-radius:10px;"><strong>Yetalle</strong></span> is a platform to help customers easily explore the goods and 
    services in their surrounding and to help businesses to easily be explored. <br>
   
</p>
<?php include('../includes/contacts2.htm');?>
<?php include('../includes/social.htm');?>
</footer>




</body>
</html>

