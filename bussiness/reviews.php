<?php
session_start();
if(!isset($_SESSION['email'])  || $_SESSION['type']!='bussiness'){
    ?>
    <script>alert('Please login first.'); location.href="../user/login";</script>
    <?php
}
require("../db/connection.php");
if(isset($_COOKIE['email']) && isset($_COOKIE['lat']) && isset($_COOKIE['lng'])){
$email = $_COOKIE['email'];
$lat = $_COOKIE['lat'];
$lng = $_COOKIE['lng'];

	$query = "UPDATE bussiness SET lat='$lat',lng='$lng' WHERE email='$email'";
	$sql = mysqli_query($conn,$query);
} 

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reviews | Yetalle</title>
    <link rel="icon" type="image/png" href="../icons/yet.png" hreflang="en-us">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/mystyle.css?version=50">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <style>
        *{
            font-family: 'Source Sans Pro', sans-serif;
            font-style: normal;
            font-weight:400;
        }
    </style>
</head>
<body>
    <?php
         $email = $_SESSION['email'];
            $type = $_SESSION['type'];

         $query = "SELECT * FROM bussiness WHERE email='$email'";
    $sql = mysqli_query($conn,$query);
    $fetch = mysqli_fetch_array($sql);
    $name = $fetch['name'];
     $photo = $fetch['photo'];
    $lvl = $fetch['acc_type'];
    $view = $fetch['views'];
    $id = $fetch['id'];
    $logo = $fetch['logo'];
    $desc= $fetch['description'];

    ?>
   
    <!-- Profile Image -->
    <div class="topnav" id="myTopnav" style="position:fixed; background-color:#00bfff; width:100%; z-index:100;">
        <a href="../HOME" class="mr-5"><img src="../icons/yet.png" width="60" height="60"></a>
         <?php
        if(isset($_SESSION['email']) && $_SESSION['type']=='bussiness'){
            ?>
                <a href="" class="text-center" style="color:black; font-weight:600; text-decoration:none; margin:0 auto;">
         <img src="../images/<?php echo $photo?>" width="100" height="100" style="border-radius:100%;"><br>  
       <span style="text-transform:uppercase; font-size:27px; color:white;"><?php echo $name?></span>
       
   
     <a href="log-out.php" class="text-center" style="color:white; font-weight:600; text-decoration:none; margin-top:-35px;">
           
        Log Out</a>
    </a>
            <?php
        }
        ?>
        <a href="my-profile" class="" style="font-weight:600; color:black; text-decoration:none; margin-top:-20px;">
        <!-- <img src="icons/destination.png" width="18" height="18">  -->
        My Profile</a>
       
        <a href="my-badge" class="" style="color:black; font-weight:600; text-decoration:none;">
         <!-- <img src="icons/food.png" width="18" height="18">  -->
        Premium</a>
         <a href="" class="" style="font-weight:600; color:white; border-bottom:5px solid white; text-decoration:none;">
        <!-- <img src="icons/blog 1.png" width="18" height="18">  -->
        My Reviews</a>
        <a href="../user/profile?id=<?php echo $id?>" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        View-As</a>
         <a href="my-events" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/blog 1.png" width="18" height="18">  -->
        Post an Event</a>
        <a href="my-vacancy" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        Post a Job</a>
        
        
       <a href="javascript:void(0);" class="icon mr-3 mt-3" onclick="myFunction()"> 
           <img src="../icons/2747254.png" width="40" height="40" style="" class="ham"> 
            </a>
            <br>
             </div>
        
    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
   
    <!--end of nav-->
    <br>
    <br>
    <br>

            <?php
          
         

            $query = "SELECT * FROM rating WHERE bussiness='$id'";
            $sql = mysqli_query($conn,$query);
            $fetch = mysqli_fetch_array($sql);

            
            if(mysqli_num_rows($sql)>0){
                $num = mysqli_num_rows($sql);

                $q = "SELECT COUNT(id) AS num FROM rating WHERE bussiness='$id'";
                $s = mysqli_query($conn,$q);
                $f = mysqli_fetch_array($s);
                $num = $f['num'];
                ?>
                <br>
                         <span class="recentlyopened ml-1"><br>Your Customer Reviews (<?php echo $num?>)</span><hr>
                         <?php
                            if($num<1){
                                ?>
                                    <p class="text-center">You don't have any customer review yet.</p>
                                <?php
                            }
                         ?>

        <div class="container">
            <?php
           
            $query = "SELECT * FROM rating WHERE bussiness='$id'";
            $sql = mysqli_query($conn,$query);
            

                while($fetch=mysqli_fetch_array($sql)){
                    $giver = $fetch['giver'];
                    $star = $fetch['star'];
                   $query_red = "SELECT * FROM normal WHERE id='$giver'";
                   $sql_red = mysqli_query($conn,$query_red);
                   $fetch_red = mysqli_fetch_array($sql_red);
                   $photo = $fetch_red['photo'];
                   $mailto = $fetch_red['email'];
                  
                   ?>
                    <a href="../images/<?php echo $photo?>"><img src="../images/<?php echo $photo?>" width="50" height="50" style="border-radius:100%;"></a>
                    <span><?php echo $fetch_red['full_name']?></span>
                    <?php for($i=0;$i<$star;$i++){
                        ?>
                        <img src="../icons/full-star.png" width="17" height="17">
                        <?php
                    }?>
                        
                    <br>
                                        <br>
                    <span style="font-size:14px; font-style:italic;"><?php echo $fetch['comment']?></span>
                    <br><span style="font-size:13px;"><?php echo $fetch['date']?></span><br>
                    
                    
                    <hr>
                    <?php
                $id = $fetch['id'];
                $q_r = "SELECT * FROM replies WHERE rate_id=$id";
                $s_r = mysqli_query($conn,$q_r);
                $n_r = mysqli_num_rows($s_r);
                if(1){
                    while($f_r = mysqli_fetch_array($s_r)){
                        ?>
                        <div class="ml-4">
                           <a style=""><span style="font-size:15px;"><?php 
                           $name = $f_r['replied_by'];
                           $name = str_replace('_',' ',$name);
                           $name = str_replace('0','',$name);
                           $name = str_replace('1','',$name);
                           $name = str_replace('2','',$name);
                           $name = str_replace('3','',$name);
                           $name = str_replace('4','',$name);
                           $name = str_replace('5','',$name);
                           $name = str_replace('6','',$name);
                           $name = str_replace('7','',$name);
                           $name = str_replace('8','',$name);
                           $name = str_replace('9','',$name);
                           
                           echo $name?></span></a>&nbsp;<img src="../icons/<?php echo $f_r['acc_type']?>" width="14" height="14"><br>
                           <span style="font-size:13px; font-style:italic;"><?php echo $f_r['reply_content']?></span><br> 
                           <span style="font-size:12px;"><?php echo $f_r['date']?></span>
                        </div><hr>
                        
                        <?php
                    }
                    ?>
                    <a href="my-reviews?rep=<?php echo $fetch['id']?>"
                     class="btn text-white" style="font-size:14px;">Reply</a>
                    <a href="my-reviews?del=<?php echo $fetch['id']?>"
                     class="btn text-white" style="font-size:14px;">Report</a><hr>
                    <?php
                }
                 
                   if(isset($_GET['del'])){
                       $id = strval($_GET['del']);
                        $q = "SELECT count(id) AS num FROM reported WHERE id=$id";
                        $s = mysqli_query($conn,$q);
                        $f = mysqli_fetch_array($s);
                        $rep = $f['num'];

                        if($rep==0){
                             $query = "INSERT INTO reported VALUES ($id)";
                       $sql = mysqli_query($conn,$query);

                       if($sql){
                           echo "<script>alert('Successfully reported. Please, rest assured that it will be reviewed and removed if we find it offensive. Thank you.'); location.href='my-reviews'; </script>";
                       }
                        }
                        else{
                            echo "<script>alert('Sorry, this review is already reported.'); location.href='my-reviews';</script>";

                        }

                      
                   }
                }
            }
            ?>
            <?php
                   if(isset($_GET['rep'])){
                       $rate_id = strval($_GET['rep']);
                       $query_giver = "SELECT giver FROM rating WHERE id=$rate_id";
                       $sql_giver = mysqli_query($conn,$query_giver);
                       $fetch_giver = mysqli_fetch_array($sql_giver);
                       $giver = $fetch_giver['giver'];
                       $giver1 = $giver;
                       $giver1 = str_replace('_',' ',$giver1);
                       $giver1 = str_replace('0','',$giver1);
                       $giver1 = str_replace('1','',$giver1);
                       $giver1 = str_replace('2','',$giver1);
                       $giver1 = str_replace('3','',$giver1);
                       $giver1 = str_replace('4','',$giver1);
                       $giver1 = str_replace('5','',$giver1);
                       $giver1 = str_replace('6','',$giver1);
                       $giver1 = str_replace('7','',$giver1);
                       $giver1 = str_replace('8','',$giver1);
                       $giver1 = str_replace('9','',$giver1);
                       
                       ?>
                       <div class="" id="reply" style="position:absolute; top:0; left:0; width:100vw; height:100vh; background-color:white; padding:50px 50px; z-index:100;">
                       <p class="text-center" style="font-size:20px;">
                           Reply to <span style="font-style:italic; font-weight:bolder; font-size:20px;"><?php echo $giver1?>'s</span> review on your business.
                           <br><span style="font-size:14px;">Replying to reviews is a wonderful way of interaction with your customers.</span>
                       </p>
                       <form method="post">

                           <textarea rows="12" maxlength="500" cols="50" class="form-control" id="field" 
                                         placeholder="eg: So glad to hearing from you. Thanks." name="reply" style="font-size:12px;" required></textarea>
                           <input type="submit" name="post_reply"
                            class="btn form-control" value="Reply" style="font-size:17px;">
                           <script>
                               
                               document.getElementById("reply").scrollIntoView();
                               document.getElementById("field").focus();
                           </script>
                       </form>
                       </div>
                       <?php
                       if(isset($_POST['post_reply'])){
                           $reply = mysql_real_escape_string($_POST['reply']);
                           $email = $_SESSION['email'];
                           $q = "SELECT id FROM bussiness WHERE email='$email'";
                           $s = mysqli_query($conn,$q);
                           $f = mysqli_fetch_array($s);
                           $id= $f['id'];
                           $query = "INSERT INTO replies VALUES ($rate_id,'$giver','$id','$lvl.png',NOW(),'$reply','')";
                           $sql = mysqli_query($conn,$query);
                           if($sql){
                               echo "<script>alert('Successfully replied!'); document.getElementById('reply').style.display='none'; location.href='my-reviews';</script>";
                           }
                           
                           ?>
                           <script></script>
                           <?php
                       }
              
            }
            ?>
        </div>
    </div>

    
     </div>
               
    <hr>

<footer class="mt-2">
    <p class="text-center" style="font-size:14px;">
    <span style="color:white; background-color:#00bfff; padding:5px; border-radius:10px;"><strong>Yetalle</strong></span> is a platform to help customers easily explore the goods and 
    services in their surrounding and to help bussinesses to easily be explored. <br>
   
</p>
<?php include('../includes/contacts2.htm');?>
<?php include('../includes/social.htm');?>
</footer>
   

</body>
</html>