<?php
session_start();
if(!isset($_SESSION['email']) || $_SESSION['type']=='user'){
    ?>
    <script>
        alert('You are not logged in.');
        location.href="../user/login.php";
    </script>
    <?php
}

require("../db/connection.php");
if(isset($_COOKIE['email']) && isset($_COOKIE['lat']) && isset($_COOKIE['lng'])){
$email = $_COOKIE['email'];
$lat = $_COOKIE['lat'];
$lng = $_COOKIE['lng'];

	$query = "UPDATE bussiness SET lat='$lat',lng='$lng' WHERE email='$email'";
	$sql = mysqli_query($conn,$query);
} 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Event | Yetalle </title>
    <link rel="icon" type="image/png" href="../icons/yet.png" hreflang="en-us">
     <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/mystyle.css?version=51">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <style>
         input{
            width:250px;
            height:35px;
            border:1px solid #00bfff;
            border-radius:5px;
            margin-bottom:10px;
        }
         
         @media screen and (min-width: 470px){
             .form{
                margin-left:30px;
             }
           
        }
         @media screen and (min-width: 700px){
             .form{
                margin-left:70px;
             }
           
        }
         @media screen and (min-width: 850px){
             .form{
                margin-left:150px;
             }
           
        }
        *{
            font-family: 'Source Sans Pro', sans-serif;
            font-style: normal;
            font-weight:400;
        }
    </style>
</head>
<body>
    <?php 
    
    $email = $_SESSION['email'];
     $query = "SELECT * FROM bussiness WHERE email='$email'";
    $sql = mysqli_query($conn,$query);
    $fetch = mysqli_fetch_array($sql);
    $name = $fetch['name'];
     $photo = $fetch['photo'];
    $lvl = $fetch['acc_type'];
    $view = $fetch['views'];
    $id = $fetch['id'];
    $logo = $fetch['logo'];
    $desc= $fetch['description'];

    ?>
    <div class="topnav" id="myTopnav" style="position:fixed; background-color:#00bfff; width:100%; z-index:100;">
        <a href="../HOME" class="mr-5"><img src="../icons/yet.png" width="60" height="60"></a>
         <?php
        if(isset($_SESSION['email']) && $_SESSION['type']=='bussiness'){
            ?>
                <a href="" class="text-center" style="color:black; font-weight:600; text-decoration:none; margin:0 auto;">
         <img src="../images/<?php echo $photo?>" width="100" height="100" style="border-radius:100%;"><br>  
       <span style="text-transform:uppercase; font-size:27px; color:white;"><?php echo $name?></span>
       
   
     <a href="log-out.php" class="text-center" style="color:white; font-weight:600; text-decoration:none; margin-top:-35px;">
           
        Log Out</a>
    </a>
            <?php
        }
        ?>
        <a href="my-profile" class="" style="font-weight:600; color:black; text-decoration:none; margin-top:-20px;">
        <!-- <img src="icons/destination.png" width="18" height="18">  -->
        My Profile</a>
       <a href="my-badge" class="" style="color:black; font-weight:600; text-decoration:none;">
         <!-- <img src="icons/food.png" width="18" height="18">  -->
        Premium</a>
        <a href="my-reviews" class="" style="color:black; font-weight:600; text-decoration:none;">
         <!-- <img src="icons/food.png" width="18" height="18">  -->
        My Reviews</a>
        <a href="../user/profile?id=<?php echo $id?>" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        View-As</a>
         <a href="" class="" style="font-weight:600; color:white; border-bottom:5px solid white; text-decoration:none;">
        <!-- <img src="icons/blog 1.png" width="18" height="18">  -->
        Post an Event</a>
        
        <a href="my-vacancy" class="" style="color:black; font-weight:600; text-decoration:none;">
        <!-- <img src="icons/about.png" width="23" height="23">  -->
        Post a Job</a>
        
        
       <a href="javascript:void(0);" class="icon mr-3 mt-3" onclick="myFunction()"> 
           <img src="../icons/2747254.png" width="40" height="40" style="" class="ham"> 
            </a>
            <br>
             </div>
        
    <script>
        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        }
    </script>
   
    <!--end of nav-->
    <br><br><br><br><br>
<span class="recentlyopened">Your Events</span>
    

<div class="ml-1 recent">
       
            <?php
                $query = "SELECT * FROM events WHERE bussiness='$id'";
                $sql = mysqli_query($conn,$query);
                if(mysqli_num_rows($sql)<1){
                    ?>
                    <p class="text-center">You don't have an event to happen soon.</p>
                    <?php
                }

                if(mysqli_num_rows($sql)>0){
                   while($fetch = mysqli_fetch_array($sql)){
                       ?>
                         <div class="car ml-1">
                             
                             <span><img src="../images/<?php echo $fetch['photo']?>" width="150" height="150" style="border-radius:10px;"></span><br>
                             <a href="my-events?id=<?php echo $id?> && del=<?php echo $fetch['id']?>" class="btn mt-1 btn-sm text-white"><img src="../icons/Tg/PicsArt_02-18-11.34.45.png" width="16" height="16"> Cancel</a><br>
                             <span style="font-weight:800; font-size:19px;"><?php echo $fetch['caption']?></span><br>
                             <span><?php echo $fetch['place']?></span><br>
                             <div style="font-size:13px;"><?php echo $fetch['starts_on']?>&nbsp;
                             <?php echo $fetch['tym']?></div><br>
                             
                         </div>
                       <?php
                       if(isset($_GET['del'])){
                           
                                
                                         $id = $_GET['del'];
                                         $q = "SELECT photo FROM events WHERE id=$id";
                                         $s = mysqli_query($conn,$q);
                                         $f = mysqli_fetch_array($s);
                                         $photo = $f['photo'];
                                         rename('../images/'.$photo,'../del/'.$photo);
                                            $query_del = "DELETE FROM events WHERE id=$id";
                                            $sql_del = mysqli_query($conn,$query_del);
                                            $query_del = "DELETE FROM interested WHERE id=$id";
                                            $sql_del = mysqli_query($conn,$query_del);
                                             $query_del = "DELETE FROM feed WHERE id=$id";
                                            $sql_del = mysqli_query($conn,$query_del);
                                            ?>
                                            <script>alert('Event successfully cancelled.'); location.href="my-events";</script><?php
                                          

                               
                                                                 
                                           
                                    }
                               
                       }
                   }
               
            ?>
       
</div>
<br>
<span class="recentlyopened"><img src="../icons/plus.png" width="30" height="30"> Add New Event</span>

<div class="container">
<br>    
<form method="post" enctype="multipart/form-data">

<span style="font-size:15px;">Title of Event <sup style="color:red; font-size:14px;" >*</sup></span><br>            
<input type="text" name="title" placeholder="eg: Dub Music Festival" class="mt-2 form-control" maxlength="50" autofocus required>

<span style="font-size:15px;">Description (a maximum of 100 words)</span><br>            
<textarea type="text" name="description" placeholder="" cols="4" class="mt-2 form-control" maxlength="100" 
style="border-bottom:2px solid #00bfff;"></textarea><br>

<label for="image" class="form-control" style="background-color:white; border-bottom:2px solid #00bfff;"><img src="../icons/camera.png" width="22" height="22"> Add Image</label><br>            
<input type="file" id="image" name="photo" accept="image/*" class="form-control" hidden>

 <span style="font-size:15px;">Place of Event <sup style="color:red; font-size:14px;" >*</sup></span><br>            
<input type="text" name="place" placeholder="eg: Millenium Hall" class="mt-2 form-control" required>

<span style="font-size:15px;">Starts on <sup style="color:red; font-size:14px;" >*</sup></span><br>
<?php $today = date('20y-m-d');?>        
<input type="date" name="starts_on" min="<?php echo $today?>" class="mt-2 form-control" style="font-size:12px; background-color:white; border-bottom:2px solid #00bfff;" required>

<span style="font-size:15px;">at (time) <sup style="color:red; font-size:14px;" >*</sup></span><br>    
<input type="time" name="starts_at" class="mt-2 form-control" style="background-color:white; border-bottom:2px solid #00bfff;" required>

<span style="font-size:15px;">Number of Days</span><br> 
<input type="number" name="days" class="mmt-2 form-control" placeholder="eg: 2" min="1" style="background-color:white; border-bottom:2px solid #00bfff;" required>


<span style="font-size:15px;">Tickets available at</span><br> 
<span style="font-size:13px;">Leave it blank if no ticket is required for the event.</span>           
<input type="text" name="ticket" placeholder="eg: At all CBE branches or via Amole" class="mt-2 form-control" maxlength="50">
<?php $email = $_SESSION['email'];
      $query = "SELECT acc_type,expiry FROM bussiness WHERE email='$email'";
      $sql = mysqli_query($conn,$query);
      $fetch = mysqli_fetch_array($sql);
      $badge = $fetch['acc_type'];
      $expiry = $fetch['expiry'];
      $today = date('20y-m-d');

      if(1){
          ?>
        <p class="text-center"><input type="submit" style="font-size:14px;" class="mt-3 btn text-white" name="add" value="Post"></p>
          <?php

      }
      ?>
    </form>
</div>
<?php
    if(isset($_POST['add'])){
      
       
        $title = mysql_real_escape_string($_POST['title']);
        $title = str_replace('\n','<br>',$title);
        $desc = mysql_real_escape_string($_POST['description']);
        $desc = str_replace('\n','<br>',$desc);
        $place = mysql_real_escape_string($_POST['place']);
        $ticket = mysql_real_escape_string($_POST['ticket']);
        $place = str_replace('\n','<br>',$place);
        $start = $_POST['starts_on'];
         $days = $_POST['days'];
       $end = date('Y-m-d', strtotime($start. ' + '.$days.' days'));
      
        $tym = $_POST['starts_at'];
       
        $datetime= date('Y-m-d').' '.date('h:i:sa');

         $photo =str_shuffle('fuyfyfufu').$_FILES['photo']['name'];
             //echo $FileName;
             $Temp =  $_FILES['photo']['tmp_name'];
             $MyLocation= "../images/".$photo ;
             move_uploaded_file($Temp,$MyLocation);
        $qi = "SELECT COUNT(id) AS num FROM events";
        $si = mysqli_query($conn,$qi);
        $fi = mysqli_fetch_array($si);
        $event_id = $fi['num']+1;
        $query = "INSERT INTO events VALUES
                 ($event_id,'$id','$title','$photo','$place','$start','$end','$tym',$days,'$datetime','$ticket','$desc')";
        $sql = mysqli_query($conn,$query);
        if($sql){
            $email = $_SESSION['email'];
            $q = "SELECT name,id FROM bussiness WHERE email='$email'";
            $s = mysqli_query($conn,$q);
            $f = mysqli_fetch_array($s);
            $name = $f['name'];
            $username = $f['id'];
            $query = "INSERT INTO feed (date_updated,feed_type,name,username,image,id) VALUES (NOW(),'event','$name','$id','$photo',$event_id)";
            $sql = mysqli_query($conn,$query);
            echo "<script>alert('Success.');
            location.href='my-events';
            </script>";
        }
    }
?>
    <hr>
    <hr>

<footer class="mt-2">
    <p class="text-center" style="font-size:14px;">
    <span style="color:white; background-color:#00bfff; padding:5px; border-radius:10px;"><strong>Yetalle</strong></span> is a platform to help customers easily explore the goods and 
    services in their surrounding and to help bussinesses to easily be explored. <br>
   
</p>
<?php include('../includes/contacts2.htm');?>
<?php include('../includes/social.htm');?>
</footer>


</body>
</html>

